//
//  AppDelegate.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import GoogleMaps
import GooglePlaces
import SQLite
import CRNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let dbSetup = DatabaseHelper()
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.all
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.setupPushNotification(application: application)
        
        print("Device = \(UIDevice.modelName)")

        /*
        let center = UNUserNotificationCenter.current()
        center.delegate = self //DID NOT WORK WHEN self WAS MyOtherDelegateClass()
        center.requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            // Enable or disable features based on authorization.
            if granted {
                // update application settings
            }
        }
        
        // iOS 12 support
        if #available(iOS 13, *) {
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        
        // iOS 10 support
        if #available(iOS 10, *) {
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        */
        
        /*
        SharedManager.xyz
        SharedManager.shared.sharedUser
        SharedManager.shared.filtersIds
        SharedManager.shared.sharedUser
        SharedManager
        SharedManager.shared.sharedUser.abc
        SharedManager.shared.sharedUser.xyz
        SharedManager.shared.abc
        SharedManager.shared.sharedProduct
        SharedManager.shared.sharedUser.allowEmail
        SharedManager.xyz
        SharedManager.abc
        SharedManager.Tt
        SharedManager.xyz*/
        
        
        return true
    }
    
    // Setup appdelegate for push notifications
    func setupPushNotification(application: UIApplication) -> () {
        
        //create the notificationCenter
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge,  .providesAppNotificationSettings]) { (granted, error) in
            // Enable or disable features based on authorization
            if granted{
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            } else {
                print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
            }
        }
        application.registerForRemoteNotifications()
        
        /*
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge])
        {(granted,error) in
            if granted{
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            } else {
                print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
            }
        }*/
    }
    
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
 
        print("APNs device token: \(deviceTokenString)")
        setDefaultValue(keyValue: kdeviceToken, valueIs: deviceTokenString)
    }
    
    //Called if unable to register for APNS.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
  }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("Recived: \(userInfo)")
        //Parsing userinfo:
        //self.extracDataFromNotification(userInfo: userInfo, isFromBackground: false)
        /*
        var temp : NSDictionary = userInfo as NSDictionary
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
        {
            var alertMsg = info["alert"] as! String
            var alert: UIAlertView!
            alert = UIAlertView(title: "", message: alertMsg, delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }*/
        
        
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func testDummyData() {
        let pdt = Product()
        pdt.userId = 1245
        pdt.restaurantId = 34
        pdt.restauntName = "Burger Rafael"
        pdt.menuId = 44
        pdt.menuName = "Jiasd"
        pdt.orderQuantity = 2
        pdt.itemPrice = 7.8
        pdt.menuOptionId = "976"
        pdt.sendToServer = "no"
        pdt.optionJson = "[{MenuOptionAdd : [{UpdatedBy: asd}]"
        pdt.orderType = "neworder"
        pdt.orderId = 456
        
        //dbSetup.insertProduct(product: pdt)
        //dbSetup.insertProduct(product: pdt)
        
        let arr = dbSetup.getAllProducts(isTopupOrders: false, resturantId: "34")
        
        //dbSetup.updateProduct(product: arr[0])
        //dbSetup.deleteProduct(product: arr[1])
        print("\(arr.count)")
        
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
        print("\(notification.request.content.userInfo)")
        
        let userInfo = notification.request.content.userInfo
        self.extracDataFromNotification(userInfo: userInfo, isFromBackground: false)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        let userInfo = response.notification.request.content.userInfo
        self.extracDataFromNotification(userInfo: userInfo, isFromBackground: true)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    /*
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        let userInfo = response.notification.request.content.userInfo
        self.extracDataFromNotification(userInfo: userInfo, isFromBackground: true)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
        print("\(notification.request.content.userInfo)")
        
        let userInfo = notification.request.content.userInfo
        self.extracDataFromNotification(userInfo: userInfo, isFromBackground: false)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }*/
    
    func extracDataFromNotification(userInfo: [AnyHashable: Any], isFromBackground: Bool) {
        //let info : NSDictionary = userInfo as NSDictionary
         
        var alertMsg = String()
        var TabName = String()
        
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject> {
            print("User-Info: \(info)")
            
            TabName = userInfo[AnyHashable("TabName")] as? String ?? ""
            print("User-Info-TabName: \(TabName)")
            if let msg = info["alert"] as? String {
                alertMsg = msg
                print("Alert Msg = \(alertMsg)")
            }
        }
        
        if isFromBackground {
            if alertMsg.contains("Cancelled") {
                gotoOrderScreen()
            }
            
        }else{
            //Forground notification handling
            NotificationCenter.default.post(name: CompleteNotification.CompleteOrderNotification, object: nil)
            NotificationCenter.default.post(name: ReloadHomeServiceNotification.ReloadNotification, object: nil)
        
            if alertMsg != "" {
                
                if TabName != "TopUp-Completed" {
                    CRNotifications.showNotification(type: CRNotifications.success, title: "Notification", message: alertMsg, dismissDelay: 3) {
                        //TODO: add logic for specific scenario where it is fired.
                    }
                }
            }
        }
    }
    
    func gotoOrderScreen(){
        
        let storyBoard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kOrderHistoryControllerID) as! OrderVC
        vc.isRecentOrders = false
        
        vc.title = "Orders".uppercased()
        let navC = self.getNavController()
        navC.pushViewController(vc, animated: true)
    }
    
    func getNavController() -> UINavigationController{
        let mainController = UIApplication.shared.windows.first!.rootViewController as! MainViewController
        let navC = mainController.rootViewController as! UINavigationController
        return navC
    }
    
}

