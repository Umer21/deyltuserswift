//
//  APIClient.swift
//
//  Created by umar on 9/14/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import CoreLocation
import CoreTelephony
import SystemConfiguration
import PKHUD
import AlamofireMapper
import JGProgressHUD

class APIClient: NSObject {
    
    static let shared = APIClient()
    var manager: SessionManager {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 5
        return manager
    }
    
    static func isConnectedToNetwork() -> Bool {
        if Reachability.isConnectedToNetwork() == true {
            return true
        }
        else {
            return false
        }
    }
    
    
    /*!
     * Login User Request
     */
    
    static func LoginRequest(email: String, password: String, type: String, block: ((_ response: UserObj?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kApiLogin)"
        
        let token = getValueForKey(keyValue: kdeviceToken)
        
        var param = Parameters()
        param = [
            "CustomerLoginId" : email,
            "CustomerPassword" : password,
            "CustomerSignupType" : type,
            "AuthCode" : authCode,
            "DeviceToken" : token,
            "DeviceData": "IOS-\(UIDevice.modelName)",
            "DeviceType" : "IOS",
            "DeviceVersion" : getAppVersion()
        ]
        /*
         if type == kFacebookUser {
         param = [
         "UserLoginId" : email,
         "UserPassword" : password,
         "UserSignupType" : type,
         "AuthCode" : authCode,
         "DeviceToken" : token,
         "Phone" : "+923322235956"
         ]
         }else{
         
         }*/
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var userObj: UserObj?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        
                        if baseResponce?.message == "Please Verify Your Phone Number" {
                            userObj = baseResponce?.response
                        }else{
                            message = baseResponce?.message!
                        }
                        
                    }else{
                        userObj = baseResponce?.response
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    /*!
     * Get Redeem Request
     */
    
    static func GetRedeemPointsRequest(cardAmount: String, block: ((_ response: RedeemObj?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetRedeemPoints)"
        
        var userID = ""
        if let user = getUser() {
            userID = "\(String(describing: user.userId!))"
        }
        
        var param = Parameters()
        param = [
            "AuthCode" : authCode,
            "CustomerId" : userID
        ]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<RedeemResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var userObj: RedeemObj?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        
                        message = baseResponce?.message!
                        
                    }else{
                        userObj = baseResponce?.result
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    /*!
     * Post Redeem Request
     */
    
    static func PostRedeemRequest(cardAmount: String, redeemAmount: String, pointWorth: String, block: ((_ response: RedeemResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kPostRedeemPoints)"
        
        var userID = ""
        if let user = getUser() {
            userID = "\(String(describing: user.userId!))"
        }
        
        var param = Parameters()
        param = [
            "AuthCode" : authCode,
            "CustomerId" : userID,
            "CardAmount": cardAmount,
            "RedeemPoint" : redeemAmount,
            "PointWorth" : pointWorth
        ]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<RedeemResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var userObj: RedeemResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        
                        userObj = baseResponce
                        
                    }else{
                        userObj = baseResponce
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    /*!
     * SignUp Request
     */
    
    
    static func SignUpRequest2(userObject: userObject, cardNo: String, expiry: String, cvv: String, userType: String, isPic: Bool, UserImage: UIImage?, withCard: Bool, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kApiSignup)"
        
        //let headers: HTTPHeaders = ["Content-Type" : "application/form-data"]//application/x-www-form-urlencoded
        //if (allowEmail = true) ? "Pass" : "Fail"
        
        var param = Parameters()
        
        var allowEmail = ""
        if userObject.allowEmail {
            allowEmail = "1"
        }else{
            allowEmail = "0"
        }
        
        
        if withCard {
            param = [
                "AuthCode" : authCode,
                "CustomerFirstName" : userObject.fname,
                "CustomerLastName": userObject.lname,
                "CustomerEmail": userObject.email,
                "CustomerLoginId" : userObject.email,
                "CustomerPassword" : userObject.password,
                "CustomerPhoneNumber" : userObject.phone .replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: ""),
                "CustomerZipCode" : userObject.zipCode,
                "CustomerSignupType" : userType,
                "IsAllowEmailReceipt" : allowEmail,
                "CreditCardNo" : cardNo,
                "CardExpire" : expiry,
                "CVV" : cvv,
                "IsDefaultCard" : "1",
                "DeviceData": "IOS-\(UIDevice.modelName)",
                "HomeDeliveryAddress" : "",
                "OfficeDeliveryAddress" : "",
                "OtherDeliveryAddress" : ""
                
                
            ]
        }else{
            //without card
            param = [
                "AuthCode" : authCode,
                "CustomerFirstName" : userObject.fname,
                "CustomerLastName": userObject.lname,
                "CustomerEmail": userObject.email,
                "CustomerLoginId" : userObject.email,
                "CustomerPassword" : userObject.password,
                "CustomerPhoneNumber" : userObject.phone .replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: ""),
                "CustomerZipCode" : userObject.zipCode,
                "CustomerSignupType" : userType,
                "IsAllowEmailReceipt" : allowEmail,
                "PaymentMethodNonce" : "",
                "DeviceData": "IOS-\(UIDevice.modelName)"
                
            ]
        }
        
        var error: Error?
        var message: String?
        var userObj: UserResponse?
        
        print(param)
        print(URL)
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            if isPic {
                if let data = UserImage!.jpegData(compressionQuality: 0.4) {
                    multipartFormData.append(data, withName: "CustomerPic", fileName: "image.jpeg",mimeType: "image/jpeg")
                }
            }
            
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                //multipartFormData.append((value as AnyObject).data(using: .utf8)!, withName: key)
            }
            
            
        },
             usingThreshold:UInt64.init(),
             to:URL,
             method:.post,
             headers:nil,
             encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    //Showing progress of uploading
                    upload.uploadProgress(closure: {(progress) in
                        //let tenPercent = 10 / 100 * progress.fractionCompleted
                        //let newProgress = progress.fractionCompleted - tenPercent
                        //SVProgressHUD.showProgress(Float(newProgress), status: "File Uploading Progress")
                    })
                    
                    
                    upload.responseObject(completionHandler: { (response: DataResponse<UserResponse>) in
                        //PKHUD.sharedHUD.hide()
                        hud.dismiss(afterDelay: 0.0)
                        
                        if let code = response.response?.statusCode {
                            let baseResponce = response.result.value
                            if code == 200 {
                                userObj = baseResponce
                            }else{
                                message = baseResponce?.message!
                            }
                        }else{
                            error = response.result.error
                            message = error?.localizedDescription
                        }
                        
                        if block != nil {
                            block!(userObj, error, message)
                        }
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    //PKHUD.sharedHUD.hide()
                    hud.dismiss(afterDelay: 0.0)
                    if block != nil {
                        block!(userObj, error, message)
                    }
                }
        })
        
    }
    
    /*!
     * Logout User Request
     */
    
    static func LogoutRequest(userId: String, block: ((_ response: UserObj?, _ error:Error?, _ message:String?) -> Void)?) {
        
        /*
         let window = UIApplication.shared.windows.first
         let hud = JGProgressHUD(style: .dark)
         if UIDevice.current.userInterfaceIdiom == .pad {
         hud.transform = hud.transform.scaledBy(x: 2, y: 2)
         }
         hud.show(in: window!)*/
        
        let URL = baseUrl+"\(kApiLogout)"
        
        let token = getValueForKey(keyValue: kdeviceToken)
        
        var param = Parameters()
        param = [
            "CustomerId" : userId,
            "AuthCode" : authCode,
            "DeviceToken" :token
            
        ]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            //hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var userObj: UserObj?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        
                        if baseResponce?.message == "Please Verify Your Phone Number" {
                            userObj = baseResponce?.response
                        }else{
                            message = baseResponce?.message!
                        }
                        
                    }else{
                        message = baseResponce?.message
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    static func SignUpRequest(userObject: userObject, cardId: String, userType: String, isPic: Bool, UserImage: UIImage?, withCard: Bool, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kApiSignup)"
        
        //let headers: HTTPHeaders = ["Content-Type" : "application/form-data"]//application/x-www-form-urlencoded
        //if (allowEmail = true) ? "Pass" : "Fail"
        
        var param = Parameters()
        
        var allowEmail = ""
        if userObject.allowEmail {
            allowEmail = "1"
        }else{
            allowEmail = "0"
        }
        
        if withCard {
            param = [
                "AuthCode" : authCode,
                "CustomerFirstName" : userObject.fname,
                "CustomerLastName": userObject.lname,
                "CustomerEmail": userObject.email,
                "CustomerLoginId" : userObject.email,
                "CustomerPassword" : userObject.password,
                "CustomerPhoneNumber" : userObject.phone .replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: ""),
                "CustomerZipCode" : userObject.zipCode,
                "CustomerSignupType" : userType,
                "IsAllowEmailReceipt" : allowEmail,
                "WePayCreditCardId" : cardId,
                "IsWePayDefaultCard" : "1",
                "DeviceData": "IOS-\(UIDevice.modelName)"
                
            ]
        }else{
            //without card
            param = [
                "AuthCode" : authCode,
                "CustomerFirstName" : userObject.fname,
                "CustomerLastName": userObject.lname,
                "CustomerEmail": userObject.email,
                "CustomerLoginId" : userObject.email,
                "CustomerPassword" : userObject.password,
                "CustomerPhoneNumber" : userObject.phone .replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: ""),
                "CustomerZipCode" : userObject.zipCode,
                "CustomerSignupType" : userType,
                "IsAllowEmailReceipt" : allowEmail,
                "PaymentMethodNonce" : "",
                "DeviceData": "IOS-\(UIDevice.modelName)"
                
            ]
        }
        
        
        var error: Error?
        var message: String?
        var userObj: UserResponse?
        
        print(param)
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            if isPic {
                if let data = UserImage!.jpegData(compressionQuality: 0.4) {
                    multipartFormData.append(data, withName: "CustomerPic", fileName: "image.jpeg",mimeType: "image/jpeg")
                }
            }
            
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                //multipartFormData.append((value as AnyObject).data(using: .utf8)!, withName: key)
            }
            
            
        },
                         usingThreshold:UInt64.init(),
                         to:URL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            
                            
                            switch encodingResult {
                                
                            case .success(let upload, _, _):
                                //Showing progress of uploading
                                upload.uploadProgress(closure: {(progress) in
                                    //let tenPercent = 10 / 100 * progress.fractionCompleted
                                    //let newProgress = progress.fractionCompleted - tenPercent
                                    //SVProgressHUD.showProgress(Float(newProgress), status: "File Uploading Progress")
                                })
                                
                                
                                upload.responseObject(completionHandler: { (response: DataResponse<UserResponse>) in
                                    //PKHUD.sharedHUD.hide()
                                    hud.dismiss(afterDelay: 0.0)
                                    
                                    if let code = response.response?.statusCode {
                                        let baseResponce = response.result.value
                                        if code == 200 {
                                            userObj = baseResponce
                                        }else{
                                            message = baseResponce?.message!
                                        }
                                    }else{
                                        error = response.result.error
                                        message = error?.localizedDescription
                                    }
                                    
                                    if block != nil {
                                        block!(userObj, error, message)
                                    }
                                })
                                
                            case .failure(let encodingError):
                                print(encodingError)
                                //PKHUD.sharedHUD.hide()
                                hud.dismiss(afterDelay: 0.0)
                                if block != nil {
                                    block!(userObj, error, message)
                                }
                            }
        })
    }
    
    /*!
     *  Add card converge Request
     */
    
    static func AddCardConvergeRequest(customerId: String, isDefaultCard: String, cardNo: String, expiryDate: String, cvv: String, firstName: String, lastName: String, zipcode: String, block: ((_ response: String?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kAddCardCoverage)"
        
        var param = Parameters()
        param = [
            "AuthCode" : authCode,
            "CustomerId" : customerId,
            "CardNumber" : cardNo,
            "IsDefaultCard" : isDefaultCard,
            "ExpiryDate" : expiryDate,
            "CVV" : cvv,
            "FirstName" : firstName,
            "LastName" : lastName,
            "ZipCode" : zipcode,
            
            "DeviceData" : "IOS-\(UIDevice.modelName)"]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var status: String?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        message = baseResponce?.message!
                    }else{
                        status = baseResponce?.status!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(status, error, message)
            }
        }
    }
    
    /*!
     *  Get card id Request
     */
    
    
    static func GetCardIDRequest(clientId: String, riskToken: String, ip: String, expMon: String, expYear: String, cvv: String, cardNo: String, email: String, userName: String, postalCode: String, block: ((_ response: String?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let url = URL(string: getCardIdRequest)!
        
        do {
            
            let address = ["country" : "US",
                           "postal_code": postalCode] as [String : Any]
            let unixtimeInterval = NSDate().timeIntervalSince1970
            
            let dicts = [
                "client_id" : clientId,
                "user_name" : userName,
                "email" : email,
                "cc_number" : cardNo,
                "cvv" : cvv,
                "expiration_month" : expMon,
                "expiration_year" : expYear,
                "original_ip" : ip,
                "original_device" : "apple",
                "reference_id" : "23_\(unixtimeInterval)", //"23_timestamp
                "card_on_file" : true,
                "recurring" : false,
                "address" : address
                ] as [String : Any]
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dicts, options: .prettyPrinted)
                // here "jsonData" is the dictionary encoded in JSON data
                //JSONText = String(data: jsonData, encoding: .utf8)!
                
                var request = URLRequest(url: url)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                request.setValue(riskToken, forHTTPHeaderField: "WePay-Risk-Token")
                request.setValue(ip, forHTTPHeaderField: "Client-IP")
                request.httpBody = jsonData
                
                Alamofire.request(request).responseJSON {
                    (response) in
                    
                    hud.dismiss(afterDelay: 0.0)
                    
                    var error: Error?
                    var message: String?
                    var cardID: String?
                    
                    //print(response)
                    //print(response.description)
                    
                    //to get status code
                    if let status = response.response?.statusCode {
                        switch(status){
                        case 200:
                            print("success")
                            //to get JSON return value
                            if let result = response.result.value {
                                let JSON = result as! NSDictionary
                                print(JSON)
                                let cardId = JSON.object(forKey: "credit_card_id") as! Int
                                print("\(cardId)")
                                cardID = "\(cardId)"
                            }
                        default:
                            print("error with response status: \(status)")
                            if let result = response.result.value {
                                let JSON = result as! NSDictionary
                                print(JSON)
                                let msg = JSON.object(forKey: "error_description") as! String
                                message = msg
                            }
                        }
                    }
                    
                    
                    
                    if block != nil {
                        block!(cardID, error, message)
                    }
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
        }
    }
    
    /*!
     *  Add card Request Elavon
     */
    
    
    static func AddCardElavonRequest(customerId: String, firstName: String, lastName: String, Email: String, zipCode:String, cardNumber: String, expiry: String, cvv: String, isDefaultCard:String,  block: ((_ response: String?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kAddCardElavon)"
        
        var param = Parameters()
        param = [
            "CustomerId" : customerId,
            "CardNumber" : cardNumber,
            "ExpiryDate" : expiry,
            "CVV" : cvv,
            "IsDefaultCard" : isDefaultCard,
            "FirstName": firstName,
            "LastName": lastName,
            "Email": Email,
            "Zipcode": zipCode,
            "AuthCode" : authCode//"DeviceData" : "IOS-\(UIDevice.modelName)"
        ]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var status: String?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        message = baseResponce?.message!
                    }else{
                        status = baseResponce?.status!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(status, error, message)
            }
        }
    }
    
    
    
    /*!
     *  Add card Request
     */
    
    
    static func AddCardRequest(customerId: String, cardId: String, isDefaultCard: String, block: ((_ response: String?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kAddCard)"
        
        var param = Parameters()
        param = [
            "CustomerId" : customerId,
            "WePayCreditCardId" : cardId,
            "IsDefaultCard" : isDefaultCard,
            "AuthCode" : authCode,
            "DeviceData" : "IOS-\(UIDevice.modelName)"]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var status: String?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        message = baseResponce?.message!
                    }else{
                        status = baseResponce?.status!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(status, error, message)
            }
        }
    }
    
    /*!
     * Get Wepay Details Request
     */
    
    
    static func GetWePayDetailsRequest( block: ((_ response: WepayDetailsResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Getting Wepay Details...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetWepayDetails)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<WepayDetailsResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: WepayDetailsResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    
    
    /*!
     * Get Verification Code Request
     */
    
    static func GetVerificationRequest(id: String, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Getting Code...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetCode)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : id
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: UserResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    /*!
     * Verify Verification Code Request
     */
    
    static func VerifyCodeRequest(id: String, code: String, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kVerifyCode)"
        
        let token = getValueForKey(keyValue: kdeviceToken)
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : id,
            "Code" : code,
            "DeviceType" : "IOS",
            "DeviceVersion" : getAppVersion(),
            "DeviceToken" : token
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var userObj: UserResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        userObj = baseResponce
                        //message = baseResponce?.message!
                    }else{
                        userObj = baseResponce
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    /*!
     * Cancel Order Request
     */
    
    static func CancelOrderRequest(orderId: String, block: ((_ response: CancelOrderResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Cancelling Order...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kCancelOrder)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "OrderId" : orderId,
            "CancelReason" : "",
            "TopUpCode" : ""
            
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<CancelOrderResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: CancelOrderResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                        //message = baseResponce?.message!
                    }else{
                        //message = baseResponce?.message!
                        dashboardObj = baseResponce
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    
    /*!
     * Cancel Topup Order Request
     */
    
    static func CancelTopupOrderRequest(orderId: String, topupCode: String, block: ((_ response: CancelOrderResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Cancelling Order...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kCancelOrder)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "OrderId" : orderId,
            "CancelReason" : "",
            "TopUpCode" : topupCode
            
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<CancelOrderResponse>) in
            // PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: CancelOrderResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                        //message = baseResponce?.message!
                    }else{
                        //message = baseResponce?.message!
                        dashboardObj = baseResponce
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    /*!
     * Forgot Password Request
     */
    
    
    static func ResetPasswordRequest(email: String, block: ((_ response: ForgotPassResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Reseting...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kForgotPass)"
        
        let parameters: Parameters = [
            "AuthCode": authCode,
            "CustomerLoginId" : email
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<ForgotPassResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: ForgotPassResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status! == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message
                    }
                    
                }else{
                    message = baseResponce?.error?.messages![0]
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Resturants by location Request
     */
    
    static func GetResturantsByLocation(id: String, tags: String, Lat:String, Long: String, isPullToRef: Bool, block: ((_ response: ResturantsResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        /*PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
         PKHUD.sharedHUD.show(onView: window)*/
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetResturantsbyLocation)"
        
        var parameters = Parameters()
        
        if isPullToRef {
            parameters = [
                "AuthCode" : authCode,
                "CustomerId" : id,
                "TagIds" : tags.dropLast(),
                "Lat": Lat,
                "Long": Long
            ]
        }else{
            parameters = [
                "AuthCode" : authCode,
                "CustomerId" : id,
                "TagIds" : tags.dropLast(),
                "Lat": Lat,
                "Long": Long,
                "isPullToResfresh": "0"
            ]
        }
        
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<ResturantsResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: ResturantsResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                        if let range = baseResponce?.AllowedBusinessRange {
                            setDefaultValue(keyValue: "Range", valueIs: range)
                        }
                    }
                    
                    if (baseResponce!.isPendingReviews == true) {
                        setDefaultValue(keyValue: "isPendingReview", valueIs: "true")
                    }else{
                        setDefaultValue(keyValue: "isPendingReview", valueIs: "false")
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Payment Methods list Request
     */
    
    static func GetPaymentMethods( block: ((_ response: PaymentMethodResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Cards...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kPaymentMethods)"
        
        var userId = ""
        if let user = getUser() {
            userId = "\(user.userId!)"
        }
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : userId
            
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<PaymentMethodResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: PaymentMethodResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Set Card Default Request
     */
    
    static func SetCardToDefault(cardId: Int, block: ((_ response: PaymentMethodResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Cards...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kUpdateCard)"
        
        var userId = ""
        if let user = getUser() {
            userId = "\(user.userId!)"
        }
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : userId,
            "CreditCardId" : "\(cardId)",
            "IsDefaultCard" : "1"
            
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<PaymentMethodResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: PaymentMethodResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get User Settings
     */
    
    static func GetUserSettingsInformation(block: ((_ response: Faq_ContactUs?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Information...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kUserSettingsInformation)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<Faq_ContactUs>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: Faq_ContactUs?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    responseObj = baseResponce
                    
                }else{
                    message = "No Data Found"
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Notification List
     */
    
    static func GetNotificationList(id: String, block: ((_ response: NotificationResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //           PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Notifications...")
        //           PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kNotificationsList)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : id
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<NotificationResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: NotificationResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Order Receipt
     */
    
    static func GetOrderReceipt(id: String, block: ((_ response: ReceiptResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kOrderReceipt)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "OrderId" : id
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<ReceiptResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: ReceiptResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                        
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Refund Request
     */
    
    static func RefundRequest(id: String, reason: String, block: ((_ response: ReceiptResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kRefundRequest)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "OrderId" : id,
            "RefundReason" : reason
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<ReceiptResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: ReceiptResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        //responseObj = baseResponce
                        message = baseResponce?.message!
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Business Reviews
     */
    
    static func GetBusinessReviews(id: String, block: ((_ response: BusinessReviews?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Reviews...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetBusinessReviews)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "BusinessId" : id
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<BusinessReviews>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: BusinessReviews?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Resturants Details
     */
    
    static func GetResturantDetials(id: String, block: ((_ response: RestDetailsResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Menu...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetResturantDetail)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "BusinessId" : id
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<RestDetailsResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: RestDetailsResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    
    /*!
     * Get Filters Request
     */
    
    static func GetFilters( block: ((_ response: FiltersResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kGetFilters)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            
        ]
        
        print(parameters)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<FiltersResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: FiltersResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Get Order History Request
     */
    
    static func GetOrderHistroy(isRecentOrder: Bool, page: Int, block: ((_ response: OrderHistoryResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading Order History...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        if page == 1{
            hud.show(in: window!)
        }
        
        
        let URL = baseUrl+"\(kGetOrderHistory)"
        var userId = ""
        if let user = getUser() {
            userId = "\(user.userId!)"
        }else{
            userId = "0"
        }
        
        var params = Parameters()
        
        if isRecentOrder {
            params = [
                "AuthCode" : authCode,
                "CustomerId" : userId,
                "FilterHours" : "24",
                "PageNumber" : "\(page)"
                
            ]
            
        }else{
            params = [
                "AuthCode" : authCode,
                "CustomerId" : userId,
                "PageNumber" : page
                
            ]
        }
        
        
        print(params)
        //URLEncoding.default, JSONEncoding.default
        Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<OrderHistoryResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var responseObj: OrderHistoryResponse?
            
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        responseObj = baseResponce
                        print(response.result.value!.toJSONString(prettyPrint: true))
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(responseObj, error, message)
            }
        }
    }
    
    /*!
     * Change Password
     */
    
    
    static func ChangeCustomerPassword(email: String, oldPassword: String, newPassword: String, block: ((_ response: PasswordResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Changing Password...")
        //        PKHUD.sharedHUD.show()
        let window = UIApplication.shared.windows.first
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kCustomerChangePassword)"
        
        let parameters: Parameters = [
            "AuthCode": authCode,
            "CustomerLoginId" : email,
            "OldPassword": oldPassword,
            "NewPassword": newPassword
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<PasswordResponse>) in
            
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var passObj: PasswordResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    
                    if baseResponce?.status == "True" {
                        passObj = baseResponce
                    }else{
                        passObj = baseResponce
                        //message = baseResponce?.message!
                    }
                }else{
                    message = baseResponce?.message
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(passObj, error, message)
            }
        }
    }
    
    static func ChangeRange(rangeFromUser: String, block: ((_ response: PasswordResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Changing Password...")
        //        PKHUD.sharedHUD.show()
        let window = UIApplication.shared.windows.first
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kRangeByCustomer)"
        var userId = ""
        if let user = getUser() {
            userId = "\(user.userId!)"
        }else{
            userId = "0"
        }
        
        let parameters: Parameters = [
            "AuthCode": authCode,
            "CustomerId": userId,
            "Range": rangeFromUser
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<PasswordResponse>) in
            
            // PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var passObj: PasswordResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    
                    if baseResponce?.status == "True" {
                        passObj = baseResponce
                    }else{
                        passObj = baseResponce
                        //message = baseResponce?.message!
                    }
                }else{
                    message = baseResponce?.message
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(passObj, error, message)
            }
        }
    }
    
    /*!
     * Change Password-Pincode Request
     */
    
    
    static func ChangePasswordWithCodeRequest(email: String, code: String, newPassword: String, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Reseting Password...")
        //        PKHUD.sharedHUD.show()
        let window = UIApplication.shared.windows.first
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kChangePassWithCode)"
        
        let parameters: Parameters = [
            "AuthCode": authCode,
            "CustomerLoginId" : email,
            "CustomerPasswordResetCode": code,
            "NewPassword": newPassword
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var userObj: UserResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    
                    if baseResponce?.status == "True" {
                        userObj = baseResponce
                    }else{
                        userObj = baseResponce
                    }
                }else{
                    message = baseResponce?.message
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    /*!
     * Place Order Request
     */
    
    static func PlaceOrderRequest(jsonData: String, businessId: String, orderType: String, deliveryAdress: String, Instructions: String,DiscountAmount: String, DiscountPoint: String, pointId : String, isAvailDiscount: String, block: ((_ response: OrderPlaceResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Placing order...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kPlaceOrder)"
        
        var userId = ""
        if let user = getUser() {
            userId = "\(user.userId!)"
        }else{
            userId = "0"
        }
        
        var parameters = Parameters()
        
        if orderType == "3" {
            parameters = [
            "AuthCode" : authCode,
            "CustomerId" : userId,
            "BusinessId" : businessId,
            "JsonData" : jsonData,
            "OrderType" : orderType,
            //"DeliveryAddress": deliveryAdress,
            "Instructions" : Instructions,
            "DeviceVersion": getAppVersion(),
            "DeviceType": "IOS",
            "DiscountWorth": DiscountAmount,
            "DiscountPoint" : DiscountPoint,
            "PointId" : pointId,
            "IsAvailDiscount" : isAvailDiscount,
            
            "DeliveryAddress" : getValueForKey(keyValue: "dAddressName"),
            "DestinationLat" : getValueForKey(keyValue: "deliveryLatitude"),
            "DestinationLong" : getValueForKey(keyValue: "deliveryLongitude"),
            "DeliverName" : getValueForKey(keyValue: "dName"),
            "DeliverPhone" : getValueForKey(keyValue: "dPhone"),
            "AdditionalDeliveryInstructions" : getValueForKey(keyValue: "dNotes")
            
                
         ]
        }else{
            
            parameters = [
            "AuthCode" : authCode,
            "CustomerId" : userId,
            "BusinessId" : businessId,
            "JsonData" : jsonData,
            "OrderType" : orderType,
            "DeliveryAddress": deliveryAdress,
            "Instructions" : Instructions,
            "DeviceVersion": getAppVersion(),
            "DeviceType": "IOS",
            "AdditionalDeliveryInstructions" : "",
            "DiscountWorth": DiscountAmount,
            "DiscountPoint" : DiscountPoint,
            "PointId" : pointId,
            "IsAvailDiscount" : isAvailDiscount
        ]
    }
        
            
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<OrderPlaceResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var orderObj: OrderPlaceResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        //dashboardObj = baseResponce
                        //message = baseResponce?.message!
                        orderObj = baseResponce
                    }else{
                        orderObj = baseResponce
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(orderObj, error, message)
            }
        }
    }
    
    
    /*!
     * Update Order Request
     */
    
    static func UpdateOrderRequest(jsonData: String, businessId: String, orderId: String, Instructions: String,DiscountAmount: String, DiscountPoint: String, pointId : String, isAvailDiscount: String, block: ((_ response: OrderPlaceResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Updating order...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kUpdateOrder)"
        
        var userId = ""
        if let user = getUser() {
            userId = "\(user.userId!)"
        }else{
            userId = "0"
        }
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : userId,
            "BusinessId" : businessId,
            "JsonData" : jsonData,
            "OrderId" : orderId,
            "Instructions" : Instructions,
            "DiscountWorth":DiscountAmount,
            "DiscountPoint" : DiscountPoint,
            "PointId" : pointId,
            "IsAvailDiscount" : isAvailDiscount
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<OrderPlaceResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var placeOrderObj: OrderPlaceResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        placeOrderObj = baseResponce
                        //message = baseResponce?.message!
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(placeOrderObj, error, message)
            }
        }
    }
    
    /*!
     * Dashboard List Request
     */
    
    /*
     static func DashboardListRequest(id: String, filterId:String, block: ((_ response: DashboardResponse?, _ error:Error?, _ message:String?) -> Void)?) {
     
     let URL = baseUrl+"\(kDashBoardList)"
     
     let parameters: Parameters = [
     "AuthCode" : authCode,
     "UserId" : id,
     "FilterId" : filterId
     ]
     
     print(parameters)
     
     Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<DashboardResponse>) in
     
     var error: Error?
     var message: String?
     var dashboardObj: DashboardResponse?
     
     if let code = response.response?.statusCode {
     let baseResponce = response.result.value
     if code == 200 {
     if baseResponce?.status == "True" {
     dashboardObj = baseResponce
     }else{
     message = baseResponce?.message!
     }
     
     }else{
     message = baseResponce?.message!
     }
     }else{
     error = response.result.error
     message = error?.localizedDescription
     }
     
     if block != nil {
     block!(dashboardObj, error, message)
     }
     }
     }*/
    
    
    
    /*!
     * Notification List Request
     */
    
    /*
     static func NotificationListRequest(id: String, block: ((_ response: NotificationResponse?, _ error:Error?, _ message:String?) -> Void)?) {
     
     let URL = baseUrl+"\(kNotificationList)"
     
     let parameters: Parameters = [
     "AuthCode" : authCode,
     "UserId" : id
     ]
     
     print(parameters)
     
     Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<NotificationResponse>) in
     
     var error: Error?
     var message: String?
     var dashboardObj: NotificationResponse?
     
     if let code = response.response?.statusCode {
     let baseResponce = response.result.value
     if code == 200 {
     if baseResponce?.status == "True" {
     dashboardObj = baseResponce
     }else{
     message = baseResponce?.message!
     }
     
     }else{
     message = baseResponce?.message!
     }
     }else{
     error = response.result.error
     message = error?.localizedDescription
     }
     
     if block != nil {
     block!(dashboardObj, error, message)
     }
     }
     }*/
    
    
    
    
    /*!
     * Update Password Request
     */
    
    /*
     static func UpdatePassword(oldPassword: String, newPassword: String, block: ((_ response: UserExistResponse?, _ error:Error?, _ message:String?) -> Void)?) {
     
     let window = UIApplication.shared.windows.first
     PKHUD.sharedHUD.contentView = PKHUDTextView(text: "Updating Password...")
     PKHUD.sharedHUD.show(onView: window)
     
     let URL = baseUrl+"\(kUpdatePassword)"
     
     var userID = ""
     if let user = getUser() {
     userID = "\(String(describing: user.email!))"
     }
     
     let parameters: Parameters = [
     "AuthCode" : authCode,
     "UserLoginId" : userID,
     "OldPassword" : oldPassword,
     "NewPassword" : newPassword
     ]
     
     Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserExistResponse>) in
     
     PKHUD.sharedHUD.hide()
     
     var error: Error?
     var message: String?
     var userObj: UserExistResponse?
     
     if let code = response.response?.statusCode {
     let baseResponce = response.result.value
     if code == 200 {
     if baseResponce?.status == "True" {
     userObj = baseResponce
     }else{
     message = baseResponce?.message!
     }
     }else{
     message = baseResponce?.message!
     }
     }else{
     error = response.result.error
     message = error?.localizedDescription
     }
     
     if block != nil {
     block!(userObj, error, message)
     }
     }
     }*/
    
    /*!
     * Update Profile Request
     */
    
    
    static func UpdateProfileRequest(firstName: String, lastName: String, zipCode: String, emailReceip: String, isPic: Bool, UserImage: UIImage?, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Updating Profile...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kUpdateProfile)"
        
        var userID = ""
        if let user = getUser() {
            userID = "\(String(describing: user.userId!))"
        }
        
        var param = Parameters()
        
        param = [
            "AuthCode" : authCode,
            "CustomerId" : userID,
            "CustomerFirstName" : firstName,
            "CustomerLastName": lastName,
            "CustomerZipCode" : zipCode,
            "IsAllowEmailReceipt" : emailReceip
        ]
        
        print(param)
        
        var error: Error?
        var message: String?
        var userObj: UserResponse?
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            if isPic {
                if let data = UserImage?.jpegData(compressionQuality: 0.1) {
                    multipartFormData.append(data, withName: "PicUrl", fileName: "image.jpeg",mimeType: "image/jpeg")
                }
            }
            
            for (key, value) in param {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         usingThreshold:UInt64.init(),
                         to:URL,
                         method:.post,
                         headers:nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                //Showing progress of uploading
                                upload.uploadProgress(closure: {(progress) in
                                    //let tenPercent = 10 / 100 * progress.fractionCompleted
                                    //let newProgress = progress.fractionCompleted - tenPercent
                                    //SVProgressHUD.showProgress(Float(newProgress), status: "File Uploading Progress")
                                })
                                
                                upload.responseObject(completionHandler: { (response: DataResponse<UserResponse>) in
                                    //PKHUD.sharedHUD.hide()
                                    hud.dismiss(afterDelay: 0.0)
                                    
                                    if let code = response.response?.statusCode {
                                        let baseResponce = response.result.value
                                        if code == 200 {
                                            if baseResponce?.status == "True" {
                                                userObj = baseResponce
                                            }else{
                                                message = baseResponce?.message!
                                            }
                                        }else{
                                            message = baseResponce?.message
                                        }
                                    }else{
                                        error = response.result.error
                                        message = error?.localizedDescription
                                    }
                                    
                                    if block != nil {
                                        block!(userObj, error, message)
                                    }
                                })
                                
                            case .failure(let encodingError):
                                print(encodingError)
                                if block != nil {
                                    block!(userObj, error, message)
                                }
                            }
        })
    }
    
    
    /*!
     * Change Phone Number Request
     */
    
    static func ChangePhoneNumberRequest(phone: String, userID:String, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Changing Phone...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kChangePhone)"
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "CustomerId" : userID,
            "CustomerNewPhoneNumber" : phone.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: " ", with: "")
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: UserResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    /*!
     * Update Tip Request
     */
    
    static func PostTipRequest(orderId: String, tipAmount: String, tipPercentage: String, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kUpdateTip)"
        
        var userID = ""
        if let user = getUser() {
            userID = "\(String(describing: user.userId!))"
        }
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "OrderId" : orderId,
            "CustomerId" : userID,
            "OrderTip" : tipAmount,
            "IsTipPercentage" : tipPercentage
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: UserResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    /*!
     * Post Review  Request
     */
    
    static func PostReviewRequest(orderId: String, OrderRating: Int, reviewText: String, block: ((_ response: UserResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kUpdateTip)"
        
        var userID = ""
        if let user = getUser() {
            userID = "\(String(describing: user.userId!))"
        }
        
        let parameters: Parameters = [
            "AuthCode" : authCode,
            "OrderId" : orderId,
            "CustomerId" : userID,
            "OrderRating" : OrderRating,
            "IsTipPercentage" : "0",
            "ReviewText" : reviewText
        ]
        
        print(parameters)
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<UserResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: UserResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    /*!
     * Next Order to be Rated Request
     */
    
    static func NextOrderToRatedRequest(isPullToRefresh: Bool, block: ((_ response: ResponseNextRateOrder?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        //        PKHUD.sharedHUD.contentView = PKHUDProgressView.init()//PKHUDTextView(text: "Loading...")
        //        PKHUD.sharedHUD.show(onView: window)
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kNextOrderToRate)"
        
        var userID = ""
        if let user = getUser() {
            userID = "\(String(describing: user.userId!))"
        }
        
        var param = Parameters()
        
        if isPullToRefresh {
            param = [
                "AuthCode" : authCode,
                "CustomerId" : userID,
                "isPullToResfresh" : "0"
            ]
        }else{
            param = [
                "AuthCode" : authCode,
                "CustomerId" : userID,
                
            ]
        }
        
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<ResponseNextRateOrder>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var dashboardObj: ResponseNextRateOrder?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "True" {
                        dashboardObj = baseResponce
                    }else{
                        message = baseResponce?.message!
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(dashboardObj, error, message)
            }
        }
    }
    
    
    /*!
     *  Check Receipt status card Request
     */
    
    static func CheckReceiptRequest(orderId: String, block: ((_ response: ReceiptResponse?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let window = UIApplication.shared.windows.first
        
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)
        
        let URL = baseUrl+"\(kCheckReceiptStatus)"
        
        var param = Parameters()
        param = [
            "AuthCode" : authCode,
            "OrderId" : orderId]
        
        print(param)
        
        Alamofire.request(URL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseObject { (response: DataResponse<ReceiptResponse>) in
            //PKHUD.sharedHUD.hide()
            hud.dismiss(afterDelay: 0.0)
            
            var error: Error?
            var message: String?
            var status: ReceiptResponse?
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    if baseResponce?.status == "False" {
                        message = baseResponce?.message!
                    }else{
                        status = baseResponce.self
                    }
                    
                }else{
                    message = baseResponce?.message!
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(status, error, message)
            }
        }
    }
    
    
}

// MARK: - Reachablity
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}
