//
//  SharedManager.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/3/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class SharedManager {
    static let shared = SharedManager()
    
    var filtersIds = [Int]()
    var sharedUser = userObject()
    var sharedProduct = Product()
    
    var locationGranted: Bool?    //Initializer access level change now
    private init(){}
    
    
    func requestForUser() -> userObject{
        //Code Process
        return self.sharedUser
    }
    
    func requestForFilters() -> [Int] {
        return self.filtersIds
    }
    
    func requestForSharedProduct() -> Product {
        return self.sharedProduct
    }
}

class userObject: NSObject {
    var fname = ""
    var lname = ""
    var email = ""
    var password = ""
    var confirmPass = ""
    var phone = ""
    var zipCode = ""
    var allowEmail = false
    var isImage = false
    var profileImage: UIImage?
}

