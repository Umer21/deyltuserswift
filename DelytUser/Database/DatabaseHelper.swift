//
//  DatabaseHelper.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umar Farooq on 4/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import Foundation
import SQLite


class DatabaseHelper {
    //DB Info
    var db: Connection?
    
    //Table
    let order = Table("ordersCart")
    
    //Columns
    let pkId = Expression<Int64>("pkId")
    let userId = Expression<Int64>("userId")
    let restaurantId = Expression<Int64>("restaurantId")
    let restaurantName = Expression<String>("restaurantName")
    let menuId = Expression<Int64>("menuId")
    let menuName = Expression<String>("menuName")
    let orderQuantity = Expression<Int64>("orderQuantity")
    let itemPrice = Expression<Double>("itemPrice")
    let menuOptionId = Expression<String>("menuOptionId")
    let sendToServer = Expression<String>("sendToServer")
    let optionJson = Expression<String>("optionJson")
    let orderType = Expression<String>("orderType")
    let orderId = Expression<Int64>("orderId")
    let basePrice = Expression<Double>("basePrice")
    
    init () {
        dbSetup()
    }
    
    func dbSetup() {
        let databaseFileName = "db.sqlite3"
        let databaseFilePath = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
        print("database path => \(databaseFilePath)")
        self.db = try! Connection(databaseFilePath)
        
        try! db!.run(order.create(ifNotExists: true) { t in
            //t.column(pkId)
            t.column(pkId, primaryKey: .autoincrement)
            t.column(userId)
            t.column(restaurantId)
            t.column(restaurantName)
            t.column(menuId)
            t.column(menuName)
            t.column(orderQuantity)
            t.column(itemPrice)
            t.column(menuOptionId)
            t.column(sendToServer)
            t.column(optionJson)
            t.column(orderType)
            t.column(orderId)
            t.column(basePrice)
        })
        
    }
    
    func insertProduct(product: Product) {
        do {
            let rowid = try db!.run(order.insert(or: .replace, userId <- product.userId!,
                                    restaurantId <- product.restaurantId!,
                                    restaurantName <- product.restauntName!,
                                    menuId <- product.menuId!,
                                    menuName <- product.menuName!,
                                    orderQuantity <- product.orderQuantity!,
                                    itemPrice <- product.itemPrice!,
                                    menuOptionId <- product.menuOptionId!,
                                    sendToServer <- product.sendToServer!,
                                    optionJson <- product.optionJson!,
                                    orderType <- product.orderType!,
                                    orderId <- product.orderId!,
                                    basePrice <- product.basePrice!
                                    ))
        print("Row inserted successfully id: \(rowid)")
        }
        catch {
            print("insertion failed: \(error)")
        }
    }
    
    
    func getAllProducts(isTopupOrders: Bool, resturantId: String) -> [Product] {
        var arrProducts = [Product]()
        
        do {
            let databaseFileName = "db.sqlite3"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
           let db = try! Connection(path)
            
            var query = ""
            
            if isTopupOrders {
                query = "SELECT pkId, userId, restaurantId, restaurantName, menuId, menuName, orderQuantity, itemPrice, menuOptionId, sendToServer, optionJson, orderType, orderId, basePrice FROM ordersCart WHERE orderId > 0 AND restaurantId = \(resturantId)"
            }else{
                query = "SELECT pkId, userId, restaurantId, restaurantName, menuId, menuName, orderQuantity, itemPrice, menuOptionId, sendToServer, optionJson, orderType, orderId, basePrice FROM ordersCart WHERE orderId = 0 AND restaurantId = \(resturantId)"
            }
            
            for row in try db.prepare(query) {
                let obj = Product()
                
                obj.pkId = row[0] as? Int64
                obj.userId = row[1] as? Int64
                obj.restaurantId = row[2] as? Int64
                obj.restauntName = row[3] as? String
                obj.menuId = row[4] as? Int64
                obj.menuName = row[5] as? String
                obj.orderQuantity = row[6] as? Int64
                obj.itemPrice = row[7] as? Double
                obj.menuOptionId = row[8] as? String
                obj.sendToServer = row[9] as? String
                obj.optionJson = row[10] as? String
                obj.orderType = row[11] as? String
                obj.orderId = row[12] as? Int64
                obj.basePrice = row[13] as? Double
                
                
                arrProducts.append(obj)
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
        return arrProducts
    }
    
    func getProdcutWithOrderId(orderId: Int64, resturantId: String) -> [Product] {
        var arrProducts = [Product]()
               
        do {
            let databaseFileName = "db.sqlite3"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            let db = try! Connection(path)
            
            for row in try db.prepare("SELECT * FROM ordersCart WHERE orderId = \(orderId) AND restaurantId = \(resturantId)" ) {
                let obj = Product()
                
                obj.pkId = row[0] as? Int64
                obj.userId = row[1] as? Int64
                obj.restaurantId = row[2] as? Int64
                obj.restauntName = row[3] as? String
                obj.menuId = row[4] as? Int64
                obj.menuName = row[5] as? String
                obj.orderQuantity = row[6] as? Int64
                obj.itemPrice = row[7] as? Double
                obj.menuOptionId = row[8] as? String
                obj.sendToServer = row[9] as? String
                obj.optionJson = row[10] as? String
                obj.orderType = row[11] as? String
                obj.orderId = row[12] as? Int64
                obj.basePrice = row[13] as? Double
                
                arrProducts.append(obj)
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
        return arrProducts
    }
    
    func getProdcutWithCombination(optionsIds: String, product: Product) -> [Product] {
        var arrProducts = [Product]()
        
        let databaseFileName = "db.sqlite3"
        let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
        let db = try! Connection(path)
        do {
            var query = ""
            if product.orderId == 0 {
                query = "SELECT * FROM ordersCart WHERE menuOptionId = '\(optionsIds)' AND orderId = 0"
            }else{
                query = "SELECT * FROM ordersCart WHERE menuOptionId = '\(optionsIds)' AND orderId = '\(String(describing: product.orderId!))'"
            }
          
            print(query)
            for row in try db.prepare(query) {
                let obj = Product()
                
                obj.pkId = row[0] as? Int64
                obj.userId = row[1] as? Int64
                obj.restaurantId = row[2] as? Int64
                obj.restauntName = row[3] as? String
                obj.menuId = row[4] as? Int64
                obj.menuName = row[5] as? String
                obj.orderQuantity = row[6] as? Int64
                obj.itemPrice = row[7] as? Double
                obj.menuOptionId = row[8] as? String
                obj.sendToServer = row[9] as? String
                obj.optionJson = row[10] as? String
                obj.orderType = row[11] as? String
                obj.orderId = row[12] as? Int64
                obj.basePrice = row[13] as? Double
                
                arrProducts.append(obj)
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
        
        return arrProducts
    }
    
    
    func updateProduct(product: Product) {
        let databaseFileName = "db.sqlite3"
        let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
        let db = try! Connection(path)
        
        do {
            let pdt = order.filter(pkId == product.pkId!)
            let quantity = product.orderQuantity!
            let newOptionIds = product.menuOptionId!
            let newPrice = product.itemPrice!
            
            if try db.run(pdt.update(orderQuantity <- quantity, menuOptionId <- newOptionIds, itemPrice <- newPrice)) > 0 {
                print("product update successfully")
            }else{
                print("product not found")
            }
            
        }catch {
            print("update failed: \(error)")
        }
    }
    
    func deleteProduct(product: Product) {
        let databaseFileName = "db.sqlite3"
        let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
        let db = try! Connection(path)
        
        do {
            let pdt = order.filter(pkId == product.pkId!)
            
            if try db.run(pdt.delete()) > 0 {
                print("product deleted successfully")
            }else{
                print("product not found")
            }
            
        }catch {
            print("update failed: \(error)")
        }
    }
    
    func deleteAllProducts(topUpOrder: Bool) {
        var arrProducts = [Product]()
               
        do {
            let databaseFileName = "db.sqlite3"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            let db = try! Connection(path)
            
            let orderId = 0 // all cart items
            var query = ""
            
            if topUpOrder {
                query = "SELECT * FROM ordersCart WHERE orderId > \(orderId)"
            }else{
                query = "SELECT * FROM ordersCart WHERE orderId = \(orderId)"
            }
            
            for row in try db.prepare(query) {
                let obj = Product()
                
                obj.pkId = row[0] as? Int64
                obj.userId = row[1] as? Int64
                obj.restaurantId = row[2] as? Int64
                obj.restauntName = row[3] as? String
                obj.menuId = row[4] as? Int64
                obj.menuName = row[5] as? String
                obj.orderQuantity = row[6] as? Int64
                obj.itemPrice = row[7] as? Double
                obj.menuOptionId = row[8] as? String
                obj.sendToServer = row[9] as? String
                obj.optionJson = row[10] as? String
                obj.orderType = row[11] as? String
                obj.orderId = row[12] as? Int64
                obj.basePrice = row[13] as? Double
                
                
                arrProducts.append(obj)
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
        
        for pdt in arrProducts {
            deleteProduct(product: pdt)
        }
        
    }
    
    
   
}
