//
//  Product.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class Product {
    var pkId : Int64?
    var userId : Int64?
    var restaurantId : Int64?
    var restauntName: String?
    var menuId: Int64?
    var menuName: String?
    var orderQuantity: Int64?
    var itemPrice: Double?
    var menuOptionId: String?
    var sendToServer: String?
    var optionJson: String?
    var orderType: String?
    var orderId: Int64?
    var basePrice: Double?
    //
    var desc: String?
    var orderStatus: String?
    var isEditable: Bool?
    var orderDate: String?
    var topupCode: String?
    var topupInstructions: String?
    
    //location variables
    var sourceLat: String?
    var sourceLong: String?
    var desLat: String?
    var desLong: String?
    var sourceAddr: String?
    var destinationAdrr: String?
    var time: String?
    var distance: String?
}
