//
//  UserResponse.swift
//  FlashCard
//
//  Created by umar on 11/12/18.
//  Copyright © 2018 APP. All rights reserved.
//

import UIKit
import ObjectMapper

class UserResponse: Mappable {
    var response: UserObj?
    var status: String?
    var message: String?
    var rewardPoints: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        response <- map["Result"]
        message <- map["Message"]
        status <- map["Status"]
        rewardPoints <- map["RewardPoints"]
    }
}

class UserObj: NSObject, Mappable, NSCoding{
    
    var userId: Int?
    var firstName: String?
    var lastName: String?
    var username: String?
    var phoneNumber: String?
    var email: String?
    var userPicUrl: String?
    var isUserPhoneNumberVerified: Bool?
    var userSignupTypeId: Int?
    var zipCode: String?
    var IsAllowEmailReceipt: Bool?
    var rewardPoints: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        userId <- map["CustomerId"]
        firstName <- map["CustomerFirstName"]
        lastName <- map["CustomerLastName"]
        phoneNumber <- map["CustomerPhoneNumber"]
        email <- map["CustomerEmail"]
        userPicUrl <- map["CustomerPicUrl"]
        isUserPhoneNumberVerified <- map["IsCustomerPhoneNumberVerified"]
        userSignupTypeId <- map["CustomerSignupTypeId"]
        zipCode <- map["CustomerZipCode"]
        IsAllowEmailReceipt <- map["IsAllowEmailReceipt"]
        rewardPoints <- map["RewardPoints"]
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let userID = aDecoder.decodeObject(forKey: "id") as? Int
        let firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        let lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let phone = aDecoder.decodeObject(forKey: "phone") as? String
        let imgUrl = aDecoder.decodeObject(forKey: "imgUrl") as? String
        let phoneVerified = aDecoder.decodeObject(forKey: "isPhoneVerified") as? Bool
        let userSignupTypeId = aDecoder.decodeObject(forKey: "userSignupTypeId") as? Int
        let zipCode = aDecoder.decodeObject(forKey: "zipcode") as? String
        let allowEmail = aDecoder.decodeObject(forKey: "isAllowEmail") as? Bool
        let rewardPoints = aDecoder.decodeObject(forKey: "rewardPoints") as? Int
        
        self.init(id: userID, fname: firstName, lname: lastName, email: email, phone: phone, url: imgUrl, isPhoneVerified: phoneVerified, userType: userSignupTypeId, zipCode: zipCode, isAllowEmail:allowEmail, rewardPoints: rewardPoints)
    }
    
    init(id: Int?, fname: String?, lname: String?, email: String?, phone: String?, url: String?, isPhoneVerified: Bool?, userType:Int?, zipCode: String?, isAllowEmail: Bool?, rewardPoints: Int?) {
        
        self.userId = id
        self.firstName = fname
        self.lastName = lname
        self.email = email
        self.phoneNumber = phone
        self.userPicUrl = url
        self.isUserPhoneNumberVerified = isPhoneVerified
        self.userSignupTypeId = userType
        self.zipCode = zipCode
        self.IsAllowEmailReceipt = isAllowEmail
        self.rewardPoints = rewardPoints
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userId, forKey: "id")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phoneNumber, forKey: "phone")
        aCoder.encode(userPicUrl, forKey: "imgUrl")
        aCoder.encode(isUserPhoneNumberVerified, forKey: "isPhoneVerified")
        aCoder.encode(userSignupTypeId, forKey: "userSignupTypeId")
        aCoder.encode(zipCode, forKey: "zipcode")
        aCoder.encode(IsAllowEmailReceipt, forKey: "isAllowEmail")
        aCoder.encode(rewardPoints, forKey: "rewardPoints")
       
    }
}


class ErrorCase: Mappable {
    var code: Int?
    var messages: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        messages <- map["messages"]
    }
}

class UserImageClass: NSObject, Mappable, NSCoding {
    
    var x1: String?
    var x2: String?
    var x3: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        x1 <- map["1x"]
        x2 <- map["2x"]
        x3 <- map["3x"]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(x1, forKey: "1x")
        aCoder.encode(x2, forKey: "2x")
        aCoder.encode(x3, forKey: "3x")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let x1 = aDecoder.decodeObject(forKey: "1x") as? String
        let x2 = aDecoder.decodeObject(forKey: "2x") as? String
        let x3 = aDecoder.decodeObject(forKey: "3x") as? String
        
        self.init(x1: x1, x2: x2, x3: x3)
    }
    
    init(x1: String?, x2: String?, x3: String?) {
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
    }
    
}

