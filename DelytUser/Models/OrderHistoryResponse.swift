//
//  OrderHistoryResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/1/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderHistoryResponse: Mappable  {
     var status: String?
     var message: String?
     var totalCount: String?
     var result: [HistoryObj]?
    
     required init?(map: Map) {
          
     }
          
     func mapping(map: Map) {
         message <- map["Message"]
         status <- map["Status"]
         result <- map["Result"]
         totalCount <- map["TotalCount"]
        
    }

}

class HistoryObj: Mappable {
    
    var orderId : Int?
    var customerId : Int?
    var businessId : Int?
    var businessLogoUrl : String?
    var businessImageUrl : String?
    var businessName : String?
    var customerName : String?
    var customerEmail : String?
    var customerPhone : String?
    var orderRating : Int?
    var orderTip : Double?
    var isTipPercentage : String?
    var orderedOn : String?
    var completedOn : String?
    var orderStatus : Int?
    var orderStatusDetail : String?
    var orderTotalAmount : Double?
    var orderType : Int?
    var deliveryAddress : String?
    var orderTypeDescription : String?
    var orderLine : [OrderLine]?
    var orderLineCancelled : [OrderLine]?
    var orderLineCanCancel : [OrderLine]?
    var businessReview : BusinessReview?
    var IsRefundRequest: Int?
    var RefundReason: String?
    var RefundRejectReason: String?
    var IsAllowDineIn: Bool?
    var IsAllowTakeOut: Bool?
    var IsAllowDelivery: Bool?
    var subtotal : Double?
    var discount : Double?
    var totalAmount : Double?
    
    required init?(map: Map) {
        
    }
    init() {}
    
    func mapping(map: Map) {
        orderId <- map["OrderId"]
        customerId <- map["CustomerId"]
        businessId <- map["BusinessId"]
        businessLogoUrl <- map["BusinessLogoUrl"]
        businessImageUrl <- map["BusinessImageUrl"]
        businessName <- map["BusinessName"]
        customerName <- map["CustomerName"]
        customerEmail <- map["CustomerEmail"]
        customerPhone <- map["CustomerPhone"]
        orderRating <- map["OrderRating"]
        orderTip <- map["OrderTip"]
        isTipPercentage <- map["IsTipPercentage"]
        orderedOn <- map["OrderedOn"]
        completedOn <- map["CompletedOn"]
        orderStatus <- map["OrderStatus"]
        orderStatusDetail <- map["OrderStatusDetail"]
        orderTotalAmount <- map["OrderTotalAmount"]
        orderType <- map["OrderType"]
        deliveryAddress <- map["DeliveryAddress"]
        orderTypeDescription <- map["OrderTypeDescription"]
        orderLine <- map["OrderLine"]
        orderLineCancelled <- map["OrderLineCancelled"]
        orderLineCanCancel <- map["OrderLineCanCancel"]
        businessReview <- map["BusinessReview"]
        IsRefundRequest <- map["IsRefundRequest"]
        RefundReason <- map["RefundReason"]
        RefundRejectReason <- map["RefundRejectReason"]
        IsAllowDineIn <- map["IsAllowDineIn"]
        IsAllowTakeOut <- map["IsAllowTakeOut"]
        IsAllowDelivery <- map["IsAllowDelivery"]
        subtotal <- map["Subtotal"]
        discount <- map["Discount"]
        totalAmount <- map["TotalAmount"]
        
    }
}

struct OrderLine : Mappable {
    var orderLineId : Int?
    var orderId : Int?
    var menuId : Int?
    var menuName : String?
    var quantity : Int?
    var unitPrice : Double?
    var topUpCode : String?
    var createdBy : Int?
    var createdOn : String?
    var updatedBy : String?
    var updatedOn : String?
    var orderLineStatus : Int?
    var orderLineStatusDescription : String?
    var instructions : String?
    var cancelledBy : String?
    var cancelledOn : String?
    var canCancel : Bool?
    var cancelReason : String?
    var orderLineMenuOptionViewModel : [OrderLineMenuOptionViewModel]?
    var OptionList: [OptionList]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        orderLineId <- map["OrderLineId"]
        orderId <- map["OrderId"]
        menuId <- map["MenuId"]
        menuName <- map["MenuName"]
        quantity <- map["Quantity"]
        unitPrice <- map["UnitPrice"]
        topUpCode <- map["TopUpCode"]
        createdBy <- map["CreatedBy"]
        createdOn <- map["CreatedOn"]
        updatedBy <- map["UpdatedBy"]
        updatedOn <- map["UpdatedOn"]
        orderLineStatus <- map["OrderLineStatus"]
        orderLineStatusDescription <- map["OrderLineStatusDescription"]
        instructions <- map["Instructions"]
        cancelledBy <- map["CancelledBy"]
        cancelledOn <- map["CancelledOn"]
        canCancel <- map["CanCancel"]
        cancelReason <- map["CancelReason"]
        orderLineMenuOptionViewModel <- map["OrderLineMenuOptionViewModel"]
        OptionList <- map["OptionList"]
    }

}

struct OrderLineMenuOptionViewModel : Mappable {
    var orderLineMenuOptionId : Int?
    var orderLineId : Int?
    var menuOptionId : Int?
    var optionName : String?
    var menuId : String?
    var menuOptionKey : String?
    var menuOptionValue : String?
    var createdBy : Int?
    var createdOn : String?
    var updatedBy : String?
    var updatedOn : String?
    var orderLineMenuOptionStatus : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        orderLineMenuOptionId <- map["OrderLineMenuOptionId"]
        orderLineId <- map["OrderLineId"]
        menuOptionId <- map["MenuOptionId"]
        optionName <- map["OptionName"]
        menuId <- map["MenuId"]
        menuOptionKey <- map["MenuOptionKey"]
        menuOptionValue <- map["MenuOptionValue"]
        createdBy <- map["CreatedBy"]
        createdOn <- map["CreatedOn"]
        updatedBy <- map["UpdatedBy"]
        updatedOn <- map["UpdatedOn"]
        orderLineMenuOptionStatus <- map["OrderLineMenuOptionStatus"]
    }

}

struct BusinessReview : Mappable {
    var customerName : String?
    var reviewDate : String?
    var reviewText : String?
    var orderRating : String?
    var masterOrderReviewReasonDetail : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        customerName <- map["CustomerName"]
        reviewDate <- map["ReviewDate"]
        reviewText <- map["ReviewText"]
        orderRating <- map["OrderRating"]
        masterOrderReviewReasonDetail <- map["MasterOrderReviewReasonDetail"]
    }

}
