//
//  RestDetailsResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/21/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

#if canImport(ObjectMapper)
import ObjectMapper
#endif


class RestDetailsResponse: Mappable {
     var status: String?
     var message: String?
     var result: ResturantsDetailObj?
    
     required init?(map: Map) {
          
     }
          
     func mapping(map: Map) {
         message <- map["Message"]
         status <- map["Status"]
         result <- map["Result"]
        
    }
    
}

class ResturantsDetailObj: Mappable {
    var businessInfo: BusinessInfo?
    var categoryListObj: [CategoryListObj]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        businessInfo <- map["BusinessInfo"]
        categoryListObj <- map["CategoryList"]
    }
}

class BusinessInfo: Mappable {
    var businessObj: BusinessObj?
    var businessImages: [BusinessImages]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        businessObj <- map["Business"]
        businessImages <- map["BusinessImages"]
    }
}

class BusinessImages: Mappable {
    var BusinessImageId: Int?
    var BusinessImageUrl: String?
    
    required init?(map: Map) {
           
    }
       
    func mapping(map: Map) {
        BusinessImageId <- map["BusinessImageId"]
        BusinessImageUrl <- map["BusinessImageUrl"]
    }
}

class BusinessObj: Mappable {
    var BusinessId: Int?
    var BusinessName: String?
    var BusinessAddress: String?
    var BusinessLat: String?
    var BusinessLong:String?
    var BusinessLogoUrl: String?
    var BusinessPhone: String?
    var BusinessRating: Int?
    var Heading1: String?
    var Heading2: String?
    var Detail2: String?
    var Heading3: String?
    var Detail3: String?
    
    
    var TagIds: String?
    var Detail1: String?
    var WePayAccountId: Double?
    var DistanceMiles: Double?
    var Reviews: Int?
    
    var IsAllowDineIn: Bool?
    var IsAllowTakeOut: Bool?
    var IsAllowDelivery: Bool?
    
    init() { }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        BusinessId <- map["BusinessId"]
        BusinessName <- map["BusinessName"]
        BusinessAddress <- map["BusinessAddress"]
        BusinessLat <- map["BusinessLat"]
        BusinessLong <- map["BusinessLong"]
        BusinessLogoUrl <- map["BusinessLogoUrl"]
        BusinessPhone <- map["BusinessPhone"]
        BusinessRating <- map["BusinessRating"]
        TagIds <- map["TagIds"]
        Detail1 <- map["Detail1"]
        Heading1 <- map["Heading1"]
        Heading2 <- map["Heading2"]
        Detail2 <- map["Detail2"]
        Heading3 <- map["Heading3"]
        Detail3 <- map["Detail3"]
        WePayAccountId <- map["WePayAccountId"]
        DistanceMiles <- map["DistanceMiles"]
        Reviews <- map["Reviews"]
        IsAllowDineIn <- map["IsAllowDineIn"]
        IsAllowTakeOut <- map["IsAllowTakeOut"]
        IsAllowDelivery <- map["IsAllowDelivery"]
        
    }
}

class CategoryListObj: Mappable {
    var CategoryId: Double?
    var CategoryName: String?
    var CategoryIconUrl: String?
    var ParentCategoryId: Double?
    var BusinessId: Double?
    
    var MenuList: [MenuObj]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        CategoryId <- map["CategoryId"]
        CategoryName <- map["CategoryName"]
        CategoryIconUrl <- map["CategoryIconUrl"]
        ParentCategoryId <- map["ParentCategoryId"]
        BusinessId <- map["BusinessId"]
        
        MenuList <- map["MenuList"]
        
    }
}

class MenuObj: Mappable {
    var MenuId: Double?
    var MenuName: String?
    var MenuShortDesc: String?
    var MenuLongDesc: String?
    var MenuPrice: Double?
    var CategoryId: Double?
    
    var MenuImages: [MenuImages]?
    var OptionList: [OptionList]?
    
    init() {}
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        MenuId <- map["MenuId"]
        MenuName <- map["MenuName"]
        MenuShortDesc <- map["MenuShortDesc"]
        MenuLongDesc <- map["MenuLongDesc"]
        MenuPrice <- map["MenuPrice"]
        CategoryId <- map["CategoryId"]
        MenuImages <- map["MenuImages"]
        OptionList <- map["OptionList"]
    }
}

class MenuImages: Mappable {
    var MenuImageId: Double?
    var MenuImageUrl: String?
    var MenuId: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        MenuImageId <- map["MenuImageId"]
        MenuImageUrl <- map["MenuImageUrl"]
        MenuId <- map["MenuId"]
    }
}

class OptionList: Mappable {
    var OptionAdd: OptionAdd?
    var MenuOptionAdd: [MenuOptionAdd]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        OptionAdd <- map["OptionAdd"]
        MenuOptionAdd <- map["MenuOptionAdd"]
    }
   
}

class OptionAdd: Mappable {
    var OptionId: Double?
    var OptionName: String?
    var MenuId: String?
    var IsKeyValue: Bool?
    var IsAddAmount: Bool?
    var IsSelectable: Bool?
    var IsMultiSelect: Bool?
    var IsActiveOption: Int?
    var IsSelectionMandatory: Bool?
    var IsAmountComulative: Bool?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        OptionId <- map["OptionId"]
        OptionName <- map["OptionName"]
        MenuId <- map["MenuId"]
        IsKeyValue <- map["IsKeyValue"]
        IsAddAmount <- map["IsAddAmount"]
        IsSelectable <- map["IsSelectable"]
        IsMultiSelect <- map["IsMultiSelect"]
        IsActiveOption <- map["IsActiveOption"]
        IsSelectionMandatory <- map["IsSelectionMandatory"]
        IsAmountComulative <- map["IsAmountComulative"]
        
    }
}

class MenuOptionAdd: Mappable {
    var MenuOptionId: Int?
    var OptionId: Int?
    var MenuOptionKey: String?
    var MenuOptionValue: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        MenuOptionId <- map["MenuOptionId"]
        MenuOptionId <- map["MenuOptionId"]
        MenuOptionKey <- map["MenuOptionKey"]
        MenuOptionValue <- map["MenuOptionValue"]
    }
}
