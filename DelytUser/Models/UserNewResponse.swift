//
//  UserNewResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/11/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
//import AlamofireMapper

class UserNewResponse: Decodable {

    var Result: User?
    var Status: String?
    var Message: String?
}

class User: Decodable {
    var CustomerId: Double!
    var CustomerFirstName: String!
    var CustomerPhoneNumber: String!
    var CustomerEmail: String!
    var CustomerLoginId:String!
    var CustomerSignupTypeId: Double!
    var IsAllowEmailReceipt: String!
    var CustomerPicUrl: String!
}
