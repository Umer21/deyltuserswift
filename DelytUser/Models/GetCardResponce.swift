//
//  GetCardResponce.swift
//  DelytUser
//
//  Created by UmarFarooq on 23/11/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class GetCardResponce: Mappable {
    
    var credit_card_id: Int?
    var state: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        credit_card_id <- map["credit_card_id"]
        state <- map["state"]
        
    }

}
