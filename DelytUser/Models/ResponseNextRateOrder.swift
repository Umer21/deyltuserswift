//
//  ResponseNextRateOrder.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/31/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class ResponseNextRateOrder: Mappable {
    var status: String?
    var message: String?
    var nextOrder: NextOrderToBeRated?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["Message"]
        status <- map["Status"]
        nextOrder <- map["NextOrderToBeRated"]
    }

}

class NextOrderToBeRated: Mappable {
    var OrderId: Int?
    var BusinessId: Int?
    var BusinessName: String?
    var BusinessImageUrl: String?
    var OrderedOn: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        OrderId <- map["OrderId"]
        BusinessId <- map["BusinessId"]
        BusinessName <- map["BusinessName"]
        BusinessImageUrl <- map["BusinessImageUrl"]
        OrderedOn <- map["OrderedOn"]
    }
}
