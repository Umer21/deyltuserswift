//
//  ForgotPassResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/3/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class ForgotPassResponse: Mappable {
    
       var error: ErrorCase?
       var status: String?
       var message: String?
       
       required init?(map: Map) {
           
       }
       
       func mapping(map: Map) {
           error <- map["error"]
           message <- map["Message"]
           status <- map["Status"]
       }

}
