//
//  NotificationResponse.swift
//  DelytUser
//
//  Created by Inabia1 on 06/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import ObjectMapper

struct NotificationResponse : Mappable {
    var status : String?
    var message : String?
    var notificationList : [NotificationList]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["Status"]
        message <- map["Message"]
        notificationList <- map["NotificationList"]
    }

}

struct NotificationList : Mappable {
    var notifiationId : Int?
    var notificationTitle : String?
    var notificationDetail : String?
    var notificationSentTo : String?
    var notificationSentToId : Int?
    var notificationDate : String?
    var notificationStatus : Int?
    var businessName : String?
    var cancelReason : String?
    var IsCancelled : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        notifiationId <- map["NotifiationId"]
        notificationTitle <- map["NotificationTitle"]
        notificationDetail <- map["NotificationDetail"]
        notificationSentTo <- map["NotificationSentTo"]
        notificationSentToId <- map["NotificationSentToId"]
        notificationDate <- map["NotificationDate"]
        notificationStatus <- map["NotificationStatus"]
        businessName <- map["BusinessName"]
        cancelReason <- map["CancelReason"]
        IsCancelled <- map["IsCancelled"]
    }

}
