//
//  ReceiptResponse.swift
//  DelytUser
//
//  Created by Inabia1 on 10/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import ObjectMapper

struct ReceiptResponse : Mappable {
    var status : String?
    var customerOrderReceipt : CustomerOrderReceipt?
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["Status"]
        customerOrderReceipt <- map["CustomerOrderReceipt"]
        message <- map["Message"]
    }

}

struct CustomerOrderReceipt : Mappable {
    var businessName : String?
    var businessLogoUrl : String?
    var orderId : String?
    var orderDate : String?
    var orderTotalAmount : String?
    var customerId : String?
    var customerName : String?
    var businessAddress : String?
    var fromDate : String?
    var toDate : String?
    var menuName : String?
    var menuOptions : String?
    var quantity : String?
    var unitPrice : String?
    var amount : String?
    var receiptOrderLine : [ReceiptOrderLine]?
    var subTotal : String?
    var discount : String?
    var tipType : String?
    var tip : String?
    var taxPercent : String?
    var taxAmount : String?
    var total : String?
    var IsRefunded: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        businessName <- map["BusinessName"]
        businessLogoUrl <- map["BusinessLogoUrl"]
        orderId <- map["OrderId"]
        orderDate <- map["OrderDate"]
        orderTotalAmount <- map["OrderTotalAmount"]
        customerId <- map["CustomerId"]
        customerName <- map["CustomerName"]
        businessAddress <- map["BusinessAddress"]
        fromDate <- map["FromDate"]
        toDate <- map["ToDate"]
        menuName <- map["MenuName"]
        menuOptions <- map["MenuOptions"]
        quantity <- map["Quantity"]
        unitPrice <- map["UnitPrice"]
        amount <- map["Amount"]
        receiptOrderLine <- map["ReceiptOrderLine"]
        subTotal <- map["SubTotal"]
        discount <- map["Discount"]
        tipType <- map["TipType"]
        tip <- map["Tip"]
        taxPercent <- map["TaxPercent"]
        taxAmount <- map["TaxAmount"]
        total <- map["Total"]
        IsRefunded <- map["IsRefunded"]
    }

}

struct ReceiptOrderLine : Mappable {
    var menuName : String?
    var menuOptions : String?
    var quantity : String?
    var unitPrice : String?
    var amount : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        menuName <- map["MenuName"]
        menuOptions <- map["MenuOptions"]
        quantity <- map["Quantity"]
        unitPrice <- map["UnitPrice"]
        amount <- map["Amount"]
    }

}
