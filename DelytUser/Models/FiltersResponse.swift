//
//  FiltersResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/16/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class FiltersResponse: Mappable {
     var status: String?
     var message: String?
     var result: [FiltersObj]?
    
     required init?(map: Map) {
          
     }
          
     func mapping(map: Map) {
         message <- map["Message"]
         status <- map["Status"]
         result <- map["Result"]
        
    }

}

class FiltersObj: Mappable {
     var tagId: Int?
     var TagName: String?
     var TagIconUrl: String?
     var TagHoverIconUrl: String?
    
     required init?(map: Map) {
          
     }
          
     func mapping(map: Map) {
         tagId <- map["TagId"]
         TagName <- map["TagName"]
         TagIconUrl <- map["TagIconUrl"]
         TagHoverIconUrl <- map["TagHoverIconUrl"]
    }
}
