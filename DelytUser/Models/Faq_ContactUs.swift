//
//  Faq_ContactUs.swift
//  DelytUser
//
//  Created by Inabia1 on 06/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import ObjectMapper

struct Faq_ContactUs : Mappable {
    var userAppSetting : UserAppSetting?
    var userFaqSetting : [UserFaqSetting]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        userAppSetting <- map["UserAppSetting"]
        userFaqSetting <- map["UserFaqSetting"]
    }

}

struct UserAppSetting : Mappable {
    var userAppSettingId : Int?
    var contactUsText : String?
    var contactUsPhone : String?
    var contactUsEmail : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        userAppSettingId <- map["UserAppSettingId"]
        contactUsText <- map["ContactUsText"]
        contactUsPhone <- map["ContactUsPhone"]
        contactUsEmail <- map["ContactUsEmail"]
    }

}

struct UserFaqSetting : Mappable {
    var userFaqId : Int?
    var userQuestion : String?
    var userAnswer : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        userFaqId <- map["UserFaqId"]
        userQuestion <- map["UserQuestion"]
        userAnswer <- map["UserAnswer"]
    }

}
