//
//  BusinessReviews.swift
//  DelytUser
//
//  Created by Inabia1 on 03/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import ObjectMapper

struct BusinessReviews : Mappable {
    var status : String?
    var message : String?
    var businessReviewsListModel : [BusinessReviewsListModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["Status"]
        message <- map["Message"]
        businessReviewsListModel <- map["BusinessReviewsListModel"]
    }

}

struct BusinessReviewsListModel : Mappable {
    var customerName : String?
    var reviewDate : String?
    var reviewText : String?
    var orderRating : String?
    var masterOrderReviewReasonDetail : String?

    init?(map: Map) {

    }
    
    init(){}

    mutating func mapping(map: Map) {

        customerName <- map["CustomerName"]
        reviewDate <- map["ReviewDate"]
        reviewText <- map["ReviewText"]
        orderRating <- map["OrderRating"]
        masterOrderReviewReasonDetail <- map["MasterOrderReviewReasonDetail"]
    }

}
