//
//  ResturantsResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/15/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class ResturantsResponse: Mappable {
    var status: String?
    var message: String?
    var isPendingReviews: Bool?
    var isDefaultPaymentMethodAdded : Bool?
    var AllowedBusinessRange : String?
    var RewardPoints : String?
    
    var result: [ResturantsObj]?
   
    required init?(map: Map) {
         
    }
         
    func mapping(map: Map) {
        message <- map["Message"]
        status <- map["Status"]
        isPendingReviews <- map["IsPendingReviews"]
        isDefaultPaymentMethodAdded <- map["IsDefaultPaymentMethodAdded"]
        AllowedBusinessRange <- map["AllowedBusinessRange"]
        RewardPoints <- map["RewardPoints"]
        result <- map["Result"]
       
   }
}

class ResturantsObj: Mappable {
    var BusinessId: Int?
    var BusinessName: String?
    var BusinessAddress: String?
    var BusinessLat: String?
    var BusinessLong:String?
    var BusinessLogoUrl: String?
    var BusinessPhone: String?
    var BusinessRating: Int?
    
    var TagIds: String?
    var Detail1: String?
    var WePayAccountId: Double?
    var DistanceMiles: Double?
    var Reviews: Double?
    
    var IsAllowDineIn: Bool?
    var IsAllowTakeOut: Bool?
    var IsAllowDelivery: Bool?
    
    required init?(map: Map) {
        
    }
    
    init() { }
    
    func mapping(map: Map) {
        BusinessId <- map["BusinessId"]
        BusinessName <- map["BusinessName"]
        BusinessAddress <- map["BusinessAddress"]
        BusinessLat <- map["BusinessLat"]
        BusinessLong <- map["BusinessLong"]
        BusinessLogoUrl <- map["BusinessLogoUrl"]
        BusinessPhone <- map["BusinessPhone"]
        BusinessRating <- map["BusinessRating"]
        TagIds <- map["TagIds"]
        Detail1 <- map["Detail1"]
        WePayAccountId <- map["WePayAccountId"]
        DistanceMiles <- map["DistanceMiles"]
        
        Reviews <- map["Reviews"]
        IsAllowDineIn <- map["IsAllowDineIn"]
        IsAllowTakeOut <- map["IsAllowTakeOut"]
        IsAllowDelivery <- map["IsAllowDelivery"]
        
    }
}


/*
class ResturantsResponse: Decodable {
    
    
    var Status: String?
    var Message: String?
    var IsPendingReviews: Double?
    var IsDefaultPaymentMethodAdded: Double?
    var Result: Array<ResturantsObj>?

}

class ResturantsObj: Decodable {
    var BusinessId: Double!
    var BusinessName: String!
    var BusinessAddress: String!
    var BusinessLat: String!
    var BusinessLong:String!
    var BusinessLogoUrl: Double!
    var BusinessPhone: String!
    var BusinessRating: String!
    
    var TagIds: String!
    var Detail1: String!
    var WePayAccountId: Double!
    var DistanceMiles: Double!
    var Reviews: Double!
    
    var IsAllowDineIn: Bool!
    var IsAllowTakeOut: Bool!
    var IsAllowDelivery: Bool!
}
*/
