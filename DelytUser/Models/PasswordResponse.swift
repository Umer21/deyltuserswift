//
//  PasswordResponse.swift
//  DelytUser
//
//  Created by Inabia1 on 14/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import ObjectMapper

class PasswordResponse: Mappable {
    
    var status: String?
    var message: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        message <- map["Message"]
        status <- map["Status"]
    }
}

struct CancelOrderResponse : Mappable {
    var status : String?
    var message : String?
    var pth : String?
    var rewardPoints : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["Status"]
        message <- map["Message"]
        pth <- map["pth"]
        rewardPoints <- map["RewardPoints"]
    }

}
