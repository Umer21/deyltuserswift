//
//  RedeemResponse.swift
//  DelytUser
//
//  Created by Inabia1 on 01/09/2021.
//  Copyright © 2021 Umar Farooq. All rights reserved.
//

import Foundation
#if canImport(ObjectMapper)
import ObjectMapper
#endif



struct RedeemResponse : Mappable {
    var status : String?
    var message : String?
    var result : RedeemObj?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["Status"]
        message <- map["Message"]
        result <- map["Result"]
    }

}

struct RedeemObj : Mappable {
    var customer : Int?
    var pointWorth : Double?
    var totalpoint : Int?
    var pointcalculate : Double?
    var discount : Int?
    var cardAmount : Double?
    var pointId : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        customer <- map["Customer"]
        pointWorth <- map["PointWorth"]
        totalpoint <- map["Totalpoint"]
        pointcalculate <- map["Pointcalculate"]
        discount <- map["Discount"]
        cardAmount <- map["CardAmount"]
        pointId <- map["PointId"]
    }

}
