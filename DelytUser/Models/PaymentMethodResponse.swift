//
//  PaymentMethodResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentMethodResponse: Mappable {

    var status: String?
    var message: String?
    var cardList: [WePayCreditCardList]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        message <- map["Message"]
        status <- map["Status"]
        cardList <- map["WePayCreditCardList"]
        
    }
}

class WePayCreditCardList: Mappable {
    var CardId: Int?
    var WePayCardId: Int?
    var CustomerId: Int?
    var CardCreatedOn: String?
    var IsDefaultCard: Bool?
    var LastFour: String?
    var ExpirationMonth : String?
    var ExpirationYear : String?
    var ImageUrl: String?
    var CardName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        CardId <- map["CardId"]
        WePayCardId <- map["WePayCardId"]
        CustomerId <- map["CustomerId"]
        CardCreatedOn <- map["CardCreatedOn"]
        IsDefaultCard <- map["CustomerCardDefault"]
        LastFour <- map["CardNumber"]
        ExpirationMonth <- map["ExpirationMonth"]
        ExpirationYear <- map["ExpirationYear"]
        ImageUrl <- map["CardImage"]
        CardName <- map["CardNumber"]
    }
    
}
