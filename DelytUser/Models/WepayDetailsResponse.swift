//
//  WepayDetailsResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/7/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class WepayDetailsResponse: Mappable {
    //var error: ErrorCase?
    
    var status: String?
    var message: String?
    var wepayData: WePayAccessDetailsData?
    
    required init?(map: Map) {
          
    }
          
    func mapping(map: Map) {
        //error <- map["error"]
        message <- map["Message"]
        status <- map["Status"]
        wepayData <- map["WePayAccessDetailsData"]
        
    }
}

class WePayAccessDetailsData: Mappable {
    
    var ClientId: Int?
    var ClientSecret: String?
    var AccessToken: String?
    var AccountID: String?
    var DefaultCustomerEmail: String?
          
    required init?(map: Map) {
          
    }
          
    func mapping(map: Map) {
        ClientId <- map["ClientID"]
        ClientSecret <- map["ClientSecret"]
        AccessToken <- map["AccessToken"]
        AccountID <- map["AccountID"]
        DefaultCustomerEmail <- map["DefaultCustomerEmail"]
    }
}
