//
//  OrderPlaceResponse.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/6/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderPlaceResponse: Mappable {

    var orderId: String?
    var status: String?
    var message: String?
    var RewardPoints: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["OrderId"]
        message <- map["Message"]
        status <- map["Status"]
        RewardPoints <- map["RewardPoints"]
    }
}
