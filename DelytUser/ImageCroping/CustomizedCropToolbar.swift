//
//  CustomizedCropToolBar.swift
//  MantisExample
//
//  Created by Echo on 4/26/20.
//  Copyright © 2020 Echo. All rights reserved.
//

import UIKit
import Mantis

class CustomizedCropToolbar: UIView, CropToolbarProtocol {
    var heightForVerticalOrientationConstraint: NSLayoutConstraint?
    var widthForHorizonOrientationConstraint: NSLayoutConstraint?
    var cropToolbarDelegate: CropToolbarDelegate?
    
    private var rotateButton: UIButton?
    private var cropButton: UIButton?
    private var cancelButton: UIButton?
    private var stackView: UIStackView?
    private var config: CropToolbarConfig!
    
    var custom: ((Double) -> Void)?
    
    func createToolbarUI(config: CropToolbarConfig) {
        self.config = config
        
        //custom color
        backgroundColor = kThemeMerunColor
        
        let imgSave = UIImage(named: "icon_save")
        let imgCancel = UIImage(named: "icon_cancel")//icon_cancel
        let imgRotate = UIImage(named: "icon_rotate")
        
        cropButton = createOptionButton(withTitle: " Save", andAction: #selector(crop), image: imgSave!)
        cancelButton = createOptionButton(withTitle: " Cancel", andAction: #selector(cancel), image: imgCancel!)
        rotateButton = createOptionButton(withTitle: " Rotate", andAction: #selector(showRatioList), image: imgRotate!)
        
        stackView = UIStackView()
        addSubview(stackView!)
        
        stackView?.translatesAutoresizingMaskIntoConstraints = false
        stackView?.alignment = .center
        stackView?.distribution = .fillEqually
        
        stackView?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        stackView?.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        stackView?.addArrangedSubview(cancelButton!)
        stackView?.addArrangedSubview(rotateButton!)
        stackView?.addArrangedSubview(cropButton!)
    }
    

    public func handleFixedRatioSetted(ratio: Double) {
        rotateButton?.setTitleColor(.blue, for: .normal)
        rotateButton?.setTitle("Unlock", for: .normal)
    }
    
    public func handleFixedRatioUnSetted() {
        rotateButton?.setTitleColor(.white, for: .normal)
        rotateButton?.setTitle("Ratio", for: .normal)
    }
    
    func adjustUIWhenOrientationChange() {
        if UIApplication.shared.statusBarOrientation.isPortrait {
            stackView?.axis = .horizontal
        } else {
            stackView?.axis = .vertical
        }
    }
    
    func getRatioListPresentSourceView() -> UIView? {
        return rotateButton
    }
            
    @objc private func crop() {
        cropToolbarDelegate?.didSelectCrop()
    }
    
    @objc private func cancel() {
        cropToolbarDelegate?.didSelectCancel()
    }
    
    @objc private func showRatioList() {
        cropToolbarDelegate?.didSelectClockwiseRotate()
    }
    
    private func createOptionButton(withTitle title: String?, andAction action: Selector, image: UIImage) -> UIButton {
        let buttonColor = UIColor.white
        let buttonFontSize: CGFloat = (UIDevice.current.userInterfaceIdiom == .pad) ?
            config.optionButtonFontSizeForPad :
            config.optionButtonFontSize
        
        let buttonFont = UIFont.systemFont(ofSize: buttonFontSize)
        
        let button = UIButton(type: .custom)//.system
        button.setImage(image, for: .normal)
        
        button.titleLabel?.font = buttonFont
        
        if let title = title {
            button.setTitle(title, for: .normal)
            button.setTitleColor(buttonColor, for: .normal)
        }
        
        button.addTarget(self, action: action, for: .touchUpInside)
        button.contentEdgeInsets = UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 10)
        
        
        return button
    }
}
