//
//  ProfileEditVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/14/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import Mantis

class ProfileEditVC: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtZipCode: UITextField!
    @IBOutlet weak var imgReceipt: UIImageView!
    @IBOutlet weak var btnChangePass: UIButton!
    
    
    var selectedImage : UIImage?
    var croppedImage: UIImage?
    
    var isImageChanged = false
    var isReciept = false
    
    @IBAction func saveTapped(_ sender: Any) {
        if isValidate()
        {
            apiEditProfile()
        }
    }
   
    
    @IBAction func btn_addPhoto(_ sender: Any) {
        
        if let user = getUser() {
            if user.userSignupTypeId == 1 {
                showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "Facebook user can't change the profile picture.")
            }else{
                takeImageOptions()
            }
        }
    }
    
    
    @IBAction func receptTapped(_ sender: Any) {
        
        if !isReciept {
            imgReceipt.image = UIImage(named: "icon_checkbox_tick")
        }else{
            imgReceipt.image = UIImage(named: "icon_checkbox")
        }
        isReciept = !isReciept
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Edit Profile"
        populateData()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        
        if let user = getUser() {
            
            if isImageChanged {
                self.profileImage.image = croppedImage
            }else{
                if (user.userPicUrl == "") {
                    profileImage.image = UIImage(named: "icon_account_camera")
                }else{
                    profileImage.af_setImage(withURL: URL(string: user.userPicUrl!)!, placeholderImage: UIImage(named: "icon_account_camera")!)
                }
                
            }
            
            profileImage.layer.cornerRadius = profileImage.frame.size.height/2
            profileImage.layer.masksToBounds = true
        }
        
        
    }
    
    func isValidate()-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        
        if (self.txtFirstName.text?.isEmpty)! {
            message = "Please enter first name."
            returnValue = false
        }
        
        if (self.txtLastName.text?.isEmpty)! {
            message = "Please enter last name."
            returnValue = false
        }
        
        if (self.txtZipCode.text?.isEmpty)! {
            message = "Please enter zip code."
            returnValue = false
        }
        
        if(self.txtZipCode.text!.count < 5)
        {
            message = "Zip code length should be 5 digits or more"
            returnValue = false
        }
       
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    
    func populateData() {
        
        if let user = getUser() {
            txtFirstName.text = user.firstName!
            txtLastName.text = user.lastName!
            //replaces country code
            let phone = user.phoneNumber!.replacingOccurrences(of: "+92", with: "").replacingOccurrences(of: "+1", with: "")
            
            txtPhone.text = formattedNumber(number: phone)
            txtZipCode.text = user.zipCode!
            /*
            if user.userPicUrl != "" {
                profileImage.af_setImage(withURL: URL(string: user.userPicUrl!)!, placeholderImage: UIImage(named: "icon_account_camera")!)
                profileImage.layer.cornerRadius = profileImage.frame.size.height/2
                profileImage.layer.masksToBounds = true
            }*/
            
            if user.IsAllowEmailReceipt == true {
                imgReceipt.image = UIImage(named: "icon_checkbox_tick")
                isReciept = true
            }else{
                imgReceipt.image = UIImage(named: "icon_checkbox")
                isReciept = false
            }
            
            if user.userSignupTypeId == 1 {
              //facebook user
                txtFirstName.isEnabled = false
                txtLastName.isEnabled = false
                txtFirstName.textColor = .lightGray
                txtLastName.textColor = .lightGray
                //btnChangePass.isEnabled = false
            }else{
                
            }
        }
    }
    
    
    func apiEditProfile() {
        
        var isAllowEmail = "0"
        if isReciept {
            isAllowEmail = "1"
        }
        
        APIClient.UpdateProfileRequest(firstName: txtFirstName.text!, lastName: txtLastName.text!, zipCode: txtZipCode.text!, emailReceip: isAllowEmail, isPic: isImageChanged, UserImage: croppedImage) { (response, error, message) in
            
            
            if response != nil {
                //retain old pic url
                if !self.isImageChanged {
                    let newUser = response?.response
                    if let user = getUser() {
                        newUser?.userPicUrl = user.userPicUrl!
                    }
                    setUser(userObj: newUser)
                }else{
                    setUser(userObj: response?.response)
                }
                
                self.popDoalog()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
            
        }
    }
    
    func popDoalog() {
        let optionMenu = UIAlertController(title: "", message: "Profile Updated Successfully", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.popBack(3)
            
        })
        
       
        optionMenu.addAction(okAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func takeImageOptions(){
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let isCameraAvalible = UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            if(isCameraAvalible) {
                self.takePhoto()
            }
        })
        
        let saveAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.PhotoLibray()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func takePhoto() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func PhotoLibray() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }
    

}

extension ProfileEditVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let originalImage = info[.originalImage] as? UIImage {
            self.isImageChanged = true
            selectedImage = originalImage
            //self.profileImage.image = nil
            //self.profileImage.image = selectedImage
        }
        self.dismiss(animated:true, completion: nil)
        
        gotoCropingScene()
    }
    
    func gotoCropingScene() {
        guard let image = selectedImage else {
            return
        }
        
        var config = Mantis.Config()
        config.showRotationDial = false
        
        let cropToolbar = CustomizedCropToolbar(frame: .zero)
        let cropViewController = Mantis.cropViewController(image: image, config: config, cropToolbar: cropToolbar)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        present(cropViewController, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated:true, completion: nil)
    }
}

//Mantis Delegate Methods
extension ProfileEditVC: CropViewControllerDelegate{
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        //print(transformation);
        self.croppedImage = cropped
        self.profileImage.image = nil
        self.profileImage.image = cropped
        self.dismiss(animated: true)
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: true)
    }
}
