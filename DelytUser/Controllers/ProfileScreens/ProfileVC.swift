//
//  ProfileVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/8/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

//@available(iOS 13.0, *)
class ProfileVC: UIViewController {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var imgRecept: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var topContraints: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavBar()
        populateData()
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            topContraints.constant = 250
        }else{
            topContraints.constant = 180
        }
        
        if let user = getUser() {
            if user.userPicUrl != "" {
                   profileImage.af_setImage(withURL: URL(string: user.userPicUrl!)!, placeholderImage: UIImage(named: "icon_account_camera")!)
                   profileImage.layer.cornerRadius = profileImage.frame.size.height/2
                   profileImage.layer.masksToBounds = true
            }
        }
        
    }
   
    func setNavBar() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let editButton = UIBarButtonItem(image: UIImage(named: "icon_nav_edit"), style: .plain, target: self, action: #selector(editTapped))
        self.navigationItem.rightBarButtonItems = [editButton]
        
    }
    
    //@available(iOS 13.0, *)
    @objc func editTapped() {
       let storyBoard = UIStoryboard(name: kProfileStoryboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kMyEditProfileControllerID) as! ProfileEditVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func populateData() {
        if let user = getUser() {
            txtFirstName.text = user.firstName!
            txtLastName.text = user.lastName!
            
            //replaces country code
            let phone = user.phoneNumber!.replacingOccurrences(of: "+92", with: "").replacingOccurrences(of: "+1", with: "")
            
            txtPhone.text = formattedNumber(number: phone)
            txtZip.text = user.zipCode!
            
            
            if user.IsAllowEmailReceipt == true {
                imgRecept.image = UIImage(named: "icon_checkbox_tick")
            }else{
                imgRecept.image = UIImage(named: "icon_checkbox")
            }
            
            if user.userSignupTypeId == 1 {
              //facebook user
                
                txtFirstName.textColor = .lightGray
                txtLastName.textColor = .lightGray
                //btnChangePass.isEnabled = false
            }
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
