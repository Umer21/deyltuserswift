//
//  NotificationCell.swift
//  DelytUser
//
//  Created by Inabia1 on 07/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lblNotificationDetail: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblBusinessTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
