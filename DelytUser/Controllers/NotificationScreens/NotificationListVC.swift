//
//  NotificationListVC.swift
//  DelytUser
//
//  Created by Inabia1 on 07/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class NotificationListVC: UIViewController {

    @IBOutlet weak var tableViewNotifications: UITableView!
    
    var items = [NotificationList]()
    
    var dataModel = [NotificationList]()
    var rowsPerSection = [[NotificationList]]()
    var sections = [MonthSectionNotification]()
    var customerID: String?
    
    var selectedModel : NotificationList?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setNavBar()
        self.tableViewNotifications.isHidden = true
        apiGetBusinessNotification()
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    

    func setNavBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("iPad")
            
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20),NSAttributedString.Key.foregroundColor: UIColor.white]
            
        }else{
            print("not iPad")
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),NSAttributedString.Key.foregroundColor: UIColor.white]
            
        }
        
    }
    
    func apiGetBusinessNotification() {
        
        let userObj = getUser()
        
        if let custID = userObj?.userId
        {
            customerID = String(custID)
        }
        
        
        APIClient.GetNotificationList(id: customerID!) { (response, error, message) in
            if response != nil {
                
                self.items = (response?.notificationList)!
                
                self.sections = MonthSectionNotification.group(headlines: self.items)//.reversed()
                self.sections.sort { lhs, rhs in lhs.month > rhs.month }
                
                if self.items.count > 0 {
                    //self.lblNodata.isHidden = true
                    self.tableViewNotifications.isHidden = false
                }else{
                    //self.lblNodata.isHidden = false
                    self.tableViewNotifications.isHidden = true
                }
                
                self.tableViewNotifications.reloadData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: "No Notifications available", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                optionMenu.addAction(okAction)
                
                self.present(optionMenu, animated: true, completion: nil)
            }
            else if message == nil && (response?.notificationList!.isEmpty)!
            {
                let optionMenu = UIAlertController(title: "", message: "No Notifications available", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                optionMenu.addAction(okAction)
                
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
    }

}

struct MonthSectionNotification {

    var month : Date
    var headlines : [NotificationList]

    static func group(headlines : [NotificationList]) -> [MonthSectionNotification] {
        let groups = Dictionary(grouping: headlines) { headline in
            
            firstDayOfMonthNotification(date: parseDateNotification(headline.notificationDate!))
        }
        return groups.map(MonthSectionNotification.init(month:headlines:))
    }

}

private func firstDayOfMonthNotification(date: Date) -> Date {
    let calendar = Calendar.current
    let components = calendar.dateComponents([.year, .month, .day], from: date)
    return calendar.date(from: components)!
}

private func parseDateNotification(_ str : String) -> Date {
    let dateFormat = DateFormatter()
    let splittedDate = str.split(separator: "T")
    dateFormat.dateFormat = "yyyy-MM-dd"
    return dateFormat.date(from: String(splittedDate[0])) ?? Date()
}

extension NotificationListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = self.sections[section]
        return section.headlines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! NotificationCell
        
        let section = self.sections[indexPath.section]
        let headline = section.headlines[indexPath.row]
        
        cell.lblBusinessTitle.text = headline.businessName
        let splittedtitle = headline.notificationDetail?.split(separator: ".")
        cell.lblNotificationDetail.text = String(splittedtitle![0])//headline.notificationTitle
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm aa"
        let convertedtime = dateFormatter.string(from: parseDateToTime(headline.notificationDate!))
        
        
        cell.lblDateTime.text = convertedtime//headline.notificationDate
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        let section = self.sections[indexPath.section]
        let headline = section.headlines[indexPath.row]
        
        selectedModel = headline
        
        gotoNotificationDetailScreen()
        
    }
    
    func gotoNotificationDetailScreen() {
        
        let storyBoard = UIStoryboard(name: kNotificationsStoryboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kNotificationDetailControllerID) as! NotificationDetailVC
        vc.notificationDetailModel = selectedModel
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("iPad")
            
           return 45
            
        }else{
            print("not iPad")
           
            return 40
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        
        view.tintColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1.0)
        
        
        let header = view as! UITableViewHeaderFooterView
        
        if let textlabel = header.textLabel {
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                print("iPad")
                
                textlabel.font = textlabel.font.withSize(20)
                
            }else{
                print("not iPad")
                textlabel.font = textlabel.font.withSize(15)
                
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        print("Header")
        let section = self.sections[section]

        let date = section.month
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM dd"//"MMM dd yyyy"
        return dateFormatter.string(from: date)
    }
    
    func parseDateToTime(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        let splittedDate = str.split(separator: "T")
        let splittedTime = splittedDate[1].split(separator: ".")
        dateFormat.dateFormat = "HH:mm:ss"
        return dateFormat.date(from: String(splittedTime[0])) ?? Date()
    }
}
