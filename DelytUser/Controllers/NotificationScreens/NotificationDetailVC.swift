//
//  NotificationDetailVC.swift
//  DelytUser
//
//  Created by Inabia1 on 07/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class NotificationDetailVC: UIViewController {
    
    var notificationDetailModel: NotificationList?

    @IBOutlet weak var lblNotificationDate: UILabel!
    @IBOutlet weak var lblBusinessTitle: UILabel!
    @IBOutlet weak var lblNotificationTime: UILabel!
    @IBOutlet weak var lblNotificationDetail: UILabel!
    @IBOutlet weak var lblCancelReason: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.title = "Notification Detail"
        populatedata()
    }
    
    
    

    func populatedata() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let convertedDate = dateFormatter.string(from: parseDateToDateFormat(notificationDetailModel!.notificationDate!))
        lblNotificationDate.text = convertedDate
        
        lblBusinessTitle.text = notificationDetailModel?.businessName
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "hh:mm aa"
        let convertedtime = dateFormatter1.string(from: parseDateToTime(notificationDetailModel!.notificationDate!))
        lblNotificationTime.text = convertedtime
        
        lblNotificationDetail.text = notificationDetailModel?.notificationDetail
        
        if notificationDetailModel?.IsCancelled == "1" {
            lblCancelReason.isHidden = false
            lblCancelReason.text = "Cancel Reason: \(notificationDetailModel!.cancelReason!)"
        }else{
            lblCancelReason.isHidden = true
        }
    }
    
    func parseDateToTime(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        let splittedDate = str.split(separator: "T")
        let splittedTime = splittedDate[1].split(separator: ".")
        dateFormat.dateFormat = "HH:mm:ss"
        return dateFormat.date(from: String(splittedTime[0])) ?? Date()
    }
    
    func parseDateToDateFormat(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        let splittedDate = str.split(separator: "T")
        dateFormat.dateFormat = "yyyy-MM-dd"
        return dateFormat.date(from: String(splittedDate[0])) ?? Date()
    }

}
