//
//  ReviewsVC.swift
//  DelytUser
//
//  Created by Inabia1 on 03/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class ReviewsVC: UIViewController {

    @IBOutlet weak var tableViewReviews: UITableView!
    var resturantId = ""
    var items = [BusinessReviewsListModel]()
    var items_temp = [BusinessReviewsListModel]()
    var isFromSideMenu = false
    var reviewObj = BusinessReviewsListModel()
    var restaurantName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Review"
        self.tableViewReviews.separatorStyle = .none
        self.tableViewReviews.separatorColor = .clear
        
        if isFromSideMenu {
            self.items.append(reviewObj)
            tableViewReviews.reloadData()
        }else{
            apiGetBusinessReviews()
        }
        
    }
    
    func apiGetBusinessReviews() {
        APIClient.GetBusinessReviews(id: resturantId) { (response, error, message) in
            if response != nil {
                if response?.businessReviewsListModel != nil {
                    self.items_temp = (response?.businessReviewsListModel)!
                    self.items = self.items_temp.reversed()
                }
                
                self.tableViewReviews.reloadData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: "No Review(s) available.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                optionMenu.addAction(okAction)
                
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
    }

}

extension ReviewsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! ReviewCell
        
        let model = self.items[indexPath.section]
        if isFromSideMenu {
            cell.lblUserName.text = restaurantName
        }else{
            cell.lblUserName.text = model.customerName ?? ""
        }
        
        //self.title = model.BusinessName!
        cell.lblReviewDate.text = model.reviewDate
        
        if model.reviewText == "" {
            cell.lblReview.text = "No comment available"
        }
        else{
            cell.lblReview.text = model.reviewText
        }
        
        let rate: Int = Int (model.orderRating!) ?? 0
        cell.businessReviews.value = CGFloat(rate)
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 200
        }else{
            return 145
        }
    }
}
