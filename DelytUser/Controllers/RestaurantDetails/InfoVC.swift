//
//  InfoVC.swift
//  DelytUser
//
//  Created by Inabia1 on 03/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import MapKit


class InfoVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    var infoObj: BusinessObj?
    var socialInfo: String?
    var urlFB: String?
    var urlTwitter: String?
    var urlInstagram: String?
    
    
    @IBOutlet weak var btn_tw: UIButton!
    
    @IBOutlet weak var btn_ins: UIButton!
    @IBOutlet weak var btn_fb: UIButton!
    @IBOutlet weak var descHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headingThreeConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailTwoConstraint: NSLayoutConstraint!
    @IBOutlet weak var headingTwoConstraint: NSLayoutConstraint!
    //@IBOutlet weak var detailOneConstraint: NSLayoutConstraint!
    @IBOutlet weak var headingOneConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblBusinessTitle: UILabel!
    @IBOutlet weak var imgBusiness: UIImageView!
    @IBOutlet weak var lblBusinessAddress: UILabel!
    /*
    @IBOutlet weak var lblHeadingOne: UILabel!
    @IBOutlet weak var tvDetailOne: UITextView!
    @IBOutlet weak var lblHeadingTwo: UILabel!
    @IBOutlet weak var tvDetailTwo: UITextView!
    @IBOutlet weak var lblHeadingThree: UILabel!
    */
    
    @IBAction func btn_facebook(_ sender: Any) {
        //print(urlFB)
        
        if let url = urlFB {
            let url1 = URL(string: url)
            if UIApplication.shared.canOpenURL(url1!) {
                UIApplication.shared.open(url1!, options: [:])
            }
            
        }
        else{
            showToast(viewControl: self, titleMsg: "", msgTitle: "No url found")
        }
    }
    
    @IBAction func btn_twitter(_ sender: Any) {
        //print(urlTwitter)
        
        if let url = urlTwitter {
            let url1 = URL(string: url)//URL(string: urlTwitter!) {
            if UIApplication.shared.canOpenURL(url1!) {
                UIApplication.shared.open(url1!, options: [:])
            }
        }
        else{
            showToast(viewControl: self, titleMsg: "", msgTitle: "No url found")
        }
    }
    
    @IBAction func btn_instagram(_ sender: Any) {
        //print(urlInstagram)
        
        if let url = urlInstagram {
            let url1 = URL(string: url)
            if UIApplication.shared.canOpenURL(url1!) {
                UIApplication.shared.open(url1!, options: [:])
            }
        }
        else{
            showToast(viewControl: self, titleMsg: "", msgTitle: "No url found")
        }
    }
    
    
    @IBAction func btn_Call(_ sender: Any) {
        
        callNumber(phoneNumber: String(infoObj?.BusinessPhone ?? "123456789"))
    }
    
    @IBAction func btn_Route(_ sender: Any) {
        
        let latitude = Double(getValueForKey(keyValue: "Latitude"))
        //print("Source-Latitude: \(latitude)")
        let longitude =  Double(getValueForKey(keyValue: "Longitude"))
        //print("Source-Longitude: \(longitude)")
        //print("Destination-Latitude: \(infoObj!.BusinessLat!)")
        //print("Destination-Longitude: \(infoObj!.BusinessLong!)")
        openMapForPlace(source_lat: String (latitude!), source_lng: String (longitude!), destination_Lat: (infoObj!.BusinessLat!), destination_lng: (infoObj!.BusinessLong!))
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //print("\(infoObj)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        showInformation()
    }

    func showInformation()
    {
        lblBusinessTitle.text = infoObj?.BusinessName
        lblBusinessAddress.text = infoObj?.BusinessAddress
        
        if infoObj?.Detail1 != "" && infoObj?.Detail1 != nil
        {
            headingOneConstraint.constant = 22
            //detailOneConstraint.constant = 80
            //lblHeadingOne.text = infoObj?.Heading1
            //tvDetailOne.text = infoObj?.Detail1
            
            
        }
        else
        {
            headingOneConstraint.constant = 0
            //detailOneConstraint.constant = 0
        }
        
        //print(infoObj?.Detail2)
        if infoObj?.Detail2 != "" && infoObj?.Detail2 != nil
        {
            headingTwoConstraint.constant = 22
            
            if UIDevice.current.userInterfaceIdiom == .pad {
               detailTwoConstraint.constant = 70
            }else{
               detailTwoConstraint.constant = 50
            }
            
           
            
            /*
            lblHeadingTwo.text = infoObj?.Heading2
            print("\(String(describing: infoObj?.Detail2))")
            tvDetailTwo.text = infoObj?.Detail2?.replacingOccurrences(of: ",", with: " \n")
            */
            
        }
        else
        {
            headingTwoConstraint.constant = 0
            detailTwoConstraint.constant = 0
        }
        
        //tvDetailOne.isEditable = false
        
      
        
        
        //tvDetailTwo.isEditable = false
        
        if let imgUrl = infoObj?.BusinessLogoUrl {
            imgBusiness.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
        }
        
        if let socialInfo = infoObj?.Detail3
        {
            headingThreeConstraint.constant = 22
            //lblHeadingThree.text = infoObj?.Heading3
            let splittedSocialInfo = socialInfo.split(separator: ",")
            
            let splittedFB = splittedSocialInfo[0].split(separator: ":")
            let splittedInstagram = splittedSocialInfo[1].split(separator: ":")
            let splittedTwitter = splittedSocialInfo[2].split(separator: ":")
            
            if(splittedTwitter.count > 1)
            {
                btn_tw.isHidden = false
                urlTwitter = String (splittedTwitter[1] + ":" + splittedTwitter[2])
            }
            else
            {
                btn_tw.isHidden = true
            }
            
            if(splittedFB.count > 1)
            {
                btn_fb.isHidden = false
                urlFB = String (splittedFB[1] + ":" + splittedFB[2])
            }
            else
            {
                btn_fb.isHidden = true
            }
            
            if(splittedInstagram.count > 1)
            {
                btn_ins.isHidden = false
                urlInstagram = String (splittedInstagram[1] + ":" + splittedInstagram[2])
            }
            else
            {
                btn_ins.isHidden = true
            }
            
            if (socialInfo == "fb:,insta:,twitter:")
            {
                headingThreeConstraint.constant = 0
            }
            else
            {
                headingThreeConstraint.constant = 22
            }
        }
        else
        {
            headingThreeConstraint.constant = 0
            btn_ins.isHidden = true
            btn_fb.isHidden = true
            btn_tw.isHidden = true
        }
        
    }
    
    func callNumber(phoneNumber:String) {

        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }
    
    func openMapForPlace(source_lat: String, source_lng: String, destination_Lat: String, destination_lng: String) {

        let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(source_lat)!, longitude: Double(source_lng)!)))
        source.name = "Source"

        let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(destination_Lat)!, longitude: Double(destination_lng)!)))
        destination.name = "Destination"

        MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }

}

extension InfoVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StroyCell", for: indexPath as IndexPath) as! StoryCell
            
            cell.lblStrory.text = infoObj?.Detail1
            cell.lblTitle.text = infoObj?.Heading1
            
            cell.selectionStyle = .none
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HourCell", for: indexPath as IndexPath) as! HourCell
            
            if let hour = infoObj?.Detail2 {
                
                if hour == "" {
                    cell.lblHour.text = ""
                }else{
                    cell.lblTitle.text = infoObj?.Heading2
                    cell.lblHour.text = hour.replacingOccurrences(of: ",", with: " \n")
                }
                
            }else{
                cell.lblHour.text = ""
            }
            
            cell.selectionStyle = .none
            return cell
        }
        
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SocialCell", for: indexPath as IndexPath) as! SocialCell
            
            if let socialInfo = infoObj?.Detail3 {
                
                let splittedSocialInfo = socialInfo.split(separator: ",")
                
                let splittedFB = splittedSocialInfo[0].split(separator: ":")
                let splittedInstagram = splittedSocialInfo[1].split(separator: ":")
                let splittedTwitter = splittedSocialInfo[2].split(separator: ":")
                
                if(splittedTwitter.count > 1) {
                    cell.btnTwitter.isHidden = false
                    urlTwitter = String (splittedTwitter[1] + ":" + splittedTwitter[2])
                }
                else {
                    cell.btnTwitter.isHidden = true
                }
                
                if(splittedFB.count > 1) {
                    cell.btnFB.isHidden = false
                    urlFB = String (splittedFB[1] + ":" + splittedFB[2])
                }
                else {
                    cell.btnFB.isHidden = true
                }
                
                if(splittedInstagram.count > 1) {
                    
                    cell.btnInsta.isHidden = false
                    urlInstagram = String (splittedInstagram[1] + ":" + splittedInstagram[2])
                }
                else {
                    cell.btnInsta.isHidden = true
                }
            }
            
            cell.selectionStyle = .none
            return cell
        }
        
        return cell1!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.row == 0 {
            if let desc = infoObj?.Detail1 {
                
                if desc == "" {
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }
        }else if indexPath.row == 1 {
            
            if let hour = infoObj?.Detail2 {
                if hour == "" {
                    return 0
                }else{
                    return UITableView.automaticDimension
                }
            }
            
        }else if indexPath.row == 2 {
            
            if let socialInfo = infoObj?.Detail3 {
                
                let splittedSocialInfo = socialInfo.split(separator: ",")
                
                let splittedFB = splittedSocialInfo[0].split(separator: ":")
                let splittedInstagram = splittedSocialInfo[1].split(separator: ":")
                let splittedTwitter = splittedSocialInfo[2].split(separator: ":")
                
                if(splittedTwitter.count > 2 || splittedFB.count > 2 || splittedInstagram.count > 2) {
                    
                    if UIDevice.current.userInterfaceIdiom == .pad {return 120 }else {return 88}
                    
                }
                else {
                    return 0
                }
                
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        
    }
   
}
