//
//  ReviewCell.swift
//  DelytUser
//
//  Created by Inabia1 on 03/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ReviewCell: UITableViewCell {

    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var lblReviewDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var businessReviews: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
