//
//  VerifyPhoneVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import KKPinCodeTextField

class VerifyPhoneVC: UIViewController {

    var phone = ""
    var userId = ""
    var email = ""
    
    var isRegistered = false
    
    var startTime = 60//900
    var secounds = 60//900//900 //15 minutes calculation
    //var timer = Timer()
    var timer : Timer? = nil {
        willSet {
            timer?.invalidate()
        }
    }
    @IBOutlet weak var txtCode: KKPinCodeTextField!
    @IBOutlet weak var btnResendCode: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBAction func doneTapped(_ sender: Any) {
        
        if (txtCode.text!.isEmpty || txtCode.text == "")
        {
            let optionMenu = UIAlertController(title: "", message: "Enter correct code", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
                
            })
            
            optionMenu.addAction(okAction)
            
            self.present(optionMenu, animated: true, completion: nil)
        }
        else{
            apiVerifyCode()
        }
        
    }
    @IBAction func changePhoneTapped(_ sender: Any) {
        gotoChangePhoneNumberScreen()
    }
    
    @IBAction func resedTapped(_ sender: Any) {
        timer = nil
        startTimer(sec: startTime)
        apiResendCode()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavBar()
        
        startTimer(sec: startTime)
        lblNumber.text = phone
        
        if isRegistered {
            apiResendCode()
        }
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
        
        //self.navigationController?.popToRootViewController(animated: true)
        
        /*
       let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
       let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
       let navCon = UINavigationController(rootViewController: welcomeVC)
       let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
       window?.rootViewController = navCon
       UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)*/
               
    }
    

    func setNavBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        
    }
    
    func gotoChangePhoneNumberScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kPhoneNumberControllerID) as! ChangePhoneVC
        //vc.modalPresentationStyle = .fullScreen
        vc.email = email
        vc.userID = userId
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func apiResendCode() {
        
        APIClient.GetVerificationRequest(id: "\(userId)") { (response, error, message) in
            
            if response != nil {
                showToast(viewControl: self, titleMsg: "", msgTitle: "\(String(describing: response!.message!))")
                self.btnResendCode.isEnabled = false
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func showDialog()
    {
        let optionMenu = UIAlertController(title: "", message: "Invalid code", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        optionMenu.addAction(okAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func apiVerifyCode(){
        
        APIClient.VerifyCodeRequest(id: "\(userId)", code: txtCode.text!) { (response, error, message) in
            
            if response != nil {
                
                print("Response--return--\(response)")
                if response?.status == "True" {
                    showToast(viewControl: self, titleMsg: "", msgTitle: "\(String(describing: response!.message!))")
                    self.gotoHomeScreen()
                    
                    let userO = getUser()
                    userO?.phoneNumber = self.lblNumber.text!
                   // print(userO?.phoneNumber)
                    setUser(userObj: userO)
                }
                else{
                    self.showDialog()
                }
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                //self.dismiss(animated: true, completion: nil)
                //self.popBack(4)
            })
        }
        
    }
    
    func gotoHomeScreen(){
        
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: kNavControllerID) as! UINavigationController
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: kHomeControllerID)], animated: false)
        mainViewController.rootViewController = navigationController
        mainViewController.modalPresentationStyle = .fullScreen
        
        //self.present(mainViewController, animated: true, completion: nil)
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = mainViewController
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func startTimer(sec: Int) {
        secounds = sec
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerCode), userInfo: nil, repeats: true)
    }
    
    @objc func timerCode() {
        let hours = self.secounds / 3600
        let mins = self.secounds / 60 % 60
        let secs = self.secounds % 60
        let restTime = ((hours<10) ? "0" : "") + String(hours) + ":" + ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
        
        lblTimer.text = restTime
        
        self.secounds = self.secounds - 1
        
        if self.secounds == -1 {
            timer = nil
            
            btnResendCode.isEnabled = true
            //dismiss Controller
            //self.dismiss(animated: true, completion: nil)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension VerifyPhoneVC: sendPhoneNumberDelegate {
    func sendPhone(phone: String) {
        lblNumber.text = phone
        //apiResendCode()
    }
    
    
}
