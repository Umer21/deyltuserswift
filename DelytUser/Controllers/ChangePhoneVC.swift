//
//  ChangePhoneVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import SwiftPhoneNumberFormatter

protocol sendPhoneNumberDelegate {
    func sendPhone(phone: String)
}
class ChangePhoneVC: UIViewController {

    var delegate: sendPhoneNumberDelegate?
    
    var email = ""
    var userID = ""
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: PhoneFormattedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //US Format
        txtPhone.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "+1 (###) ###-####")
        
        //PK Format
        //txtPhone.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "+92 (###) ###-####")
        
        txtEmail.text = email
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        if isValidate() {
            apiChangePhoneNumber()
        }
    }
    
    func apiChangePhoneNumber() {
        APIClient.ChangePhoneNumberRequest(phone: txtPhone.text!, userID: userID) { (response, error, message) in
            
            if response != nil {
                //print(response)
                if response?.status == "True"
                {
                    /*showToast(viewControl: self, titleMsg: "", msgTitle: "\(String(describing: response!.message!))")
                    self.delegate?.sendPhone(phone: (response?.response!.phoneNumber!)!)
                    self.navigationController?.popViewController(animated: true)*/
                    self.showDialog(phone: (response?.response!.phoneNumber)!)
                }
                else
                {
                    showToast(viewControl: self, titleMsg: "", msgTitle: "Phone number already exist")
                }
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
            
        }
    }
    
    func showDialog(phone: String)
    {
        let optionMenu = UIAlertController(title: "", message: "Phone Number Updated Successfully", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.delegate?.sendPhone(phone: phone)
            self.navigationController?.popViewController(animated: true)
            
        })
        
        optionMenu.addAction(okAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func isValidate()-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        if (self.txtPhone.text?.isEmpty)! {
            message = "Please enter phone number."
            returnValue = false
        }
        
        if (self.txtEmail.text?.isEmpty)! {
            message = "Please enter email."
            returnValue = false
        }
        
        
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
