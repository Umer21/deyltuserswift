//
//  SettingsVC.swift
//  DelytUser
//
//  Created by Inabia1 on 14/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    var range: String?
    
    @IBOutlet weak var rangIB: UISlider!
    
    @IBAction func rangeSlider(_ sender: UISlider) {
        
        lblRange.text = String (Int(sender.value))
    }
    
    
    @IBOutlet weak var lblRange: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        range = getValueForKey(keyValue: "Range")
        lblRange.text = range
        
        rangIB.value = Float (range!) ?? 00
        
    }
    
    func setNavBar() {
        
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTapped))
        //self.navigationItem.leftBarButtonItems = [crossButton]
        self.navigationItem.rightBarButtonItems = [saveButton]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("iPad")
            
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20),NSAttributedString.Key.foregroundColor: UIColor.white]
            
            let font = UIFont.systemFont(ofSize: 20);
            saveButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for:UIControl.State.normal)
            
        }else{
            print("not iPad")
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),NSAttributedString.Key.foregroundColor: UIColor.white]
            
            let font = UIFont.systemFont(ofSize: 16);
            saveButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for:UIControl.State.normal)
        }
        
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func saveButtonTapped() {
        apiChangeRange()
    }
    
    func apiChangeRange() {
            setDefaultValue(keyValue: "Range", valueIs: lblRange.text!)
        
            APIClient.ChangeRange(rangeFromUser: lblRange.text!) { (response, error, message) in
                
                if response != nil {
                    
                    //print(response)
                    if(response?.status == "True")
                    {
                        showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        })
                        
                    }
                    else if(response?.status == "False"){
                        showToast(viewControl: self, titleMsg: "Error", msgTitle: response!.message!)
                    }
                }
                
                if error != nil {
                    print(error.debugDescription)
                }
            }
        
    }
    

}
