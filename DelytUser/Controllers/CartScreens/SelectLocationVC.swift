//
//  SelectLocationVC.swift
//  SafeTruck-Passenger
//
//  Created by UmarFarooq on 16/07/2020.
//  Copyright © 2020 UmarFarooq. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import SwiftPhoneNumberFormatter

class SelectLocationVC: UIViewController {
    var locationManager = CLLocationManager()
    var isFromDelivery = false
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewTop: UIView!
    
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtUnit: UITextField!
    @IBOutlet weak var txtNotes: UITextField!
    @IBOutlet weak var txtPhone: PhoneFormattedTextField!
    
    @IBAction func pickupTapped(_ sender: Any) {
        self.gotoLocationScreen(isSource: true)
    }
    
    @IBAction func destinationTapped(_ sender: Any) {
        self.gotoLocationScreen(isSource: false)
    }
    @IBAction func CurrentLocationTapped(_ sender: Any) {
        setCurrentLocation()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        
        if isValidate() {
            
            setDefaultValue(keyValue: "dName", valueIs: self.txtName.text!)
            setDefaultValue(keyValue: "dPhone", valueIs: self.txtPhone.text!)
            setDefaultValue(keyValue: "dStreet", valueIs: self.txtStreet.text!)
            setDefaultValue(keyValue: "dUnit", valueIs: self.txtUnit.text!)
            setDefaultValue(keyValue: "dNotes", valueIs: self.txtNotes.text!)
            
            gotoNext()
        }
    }
    
    func isValidate()-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        if (self.txtUnit.text?.isEmpty)! {
            message = "Please enter floor/Unit number."
            returnValue = false
        }
        
        if (self.txtStreet.text?.isEmpty)! {
            message = "Please enter street."
            returnValue = false
        }
        
        
        if (self.txtPhone.text?.isEmpty)! {
            message = "Please enter phone number."
            returnValue = false
        }
        
        
        if (self.txtName.text?.isEmpty)! {
            message = "Please enter name."
            returnValue = false
        }
        
        
       
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    
    func phoneNumberFormat() {
        
        
        //US Format
        txtPhone.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "+1 (###) ###-####")
        
        //PK Format
        //txtPhone.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "+## (###) ###-####")
        
    }
    
    func gotoFoodFlow(){
        
        //goto package create screen
        /*
        let storyBoard = UIStoryboard(name: kFoodStroyboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kFoodControllerID) as! FoodVC
        vc.title = "Food Detail"
        
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    
    func gotoNext(){
        
        self.navigationController?.popViewController(animated: true)
        
        /*
        let storyboard = UIStoryboard(name: kAddItemStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kAddItemViewController) as! AddItemVC
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Locations"
        
        //for fresh locations
        //resetLocations()
        //getTotalDistance()
        
        self.tabBarController?.tabBar.isHidden = true
        
        viewTop.layer.borderWidth = 0.5
        viewTop.layer.cornerRadius = 5
        viewTop.layer.borderColor = UIColorFromRGB(rgbValue: 0xacacac).cgColor
        
        phoneNumberFormat()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.populateData()
        
    }
    
    func setCurrentLocation(){
        
        //Location Manager code to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    
    
    func addDestinationMarker(){
        mapView.clear()
        
        let markerSource = GMSMarker()
        
        markerSource.position = CLLocationCoordinate2D(latitude: Double(SharedManager.shared.sharedProduct.sourceLat!) ?? 00 , longitude: Double (SharedManager.shared.sharedProduct.sourceLong!) ?? 00 )
        markerSource.icon = UIImage(named: "icon_map_delivery")
        markerSource.title = SharedManager.shared.sharedProduct.sourceAddr
        //markerSource.snippet = "Desti"
        
        markerSource.map = mapView
        
        //Zoom Work
        let currentLatitude = Double (getValueForKey(keyValue: "deliveryLatitude"))
        let currentLongitude = Double (getValueForKey(keyValue: "deliveryLongitude"))
        let cord2D = CLLocationCoordinate2D(latitude: (currentLatitude ?? 0.000), longitude: (currentLongitude ?? 0.000))
        
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 12)
    }
    
    /*
    func drawPath(from polyStr: String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = mapView // Google MapView
    }*/
    
    
    
    func populateData() {
        
        self.lblDeliveryAddress.text = getValueForKey(keyValue: "dAddressName")
        
        if getValueForKey(keyValue: "dAddressName") == "" {
            self.lblDeliveryAddress.text = "Select Location"
        }else{
            SharedManager.shared.sharedProduct.sourceLat = getValueForKey(keyValue: "deliveryLatitude")
            SharedManager.shared.sharedProduct.sourceLong = getValueForKey(keyValue: "deliveryLongitude")
            
            self.addDestinationMarker()
        }
        
    }
    
    func gotoLocationScreen(isSource: Bool) {
        let storyBoard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kLocationControllerID) as! LocationVC
        vc.isFromDelivery = true
        //vc.isSourseAddr = isSource
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - CLLocationManagerDelegate

extension SelectLocationVC: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    
    guard status == .authorizedWhenInUse else {
      return
    }

    print("AAA-didChangeAuthorization")
    
    locationManager.startUpdatingLocation()
      
    //mapView.isMyLocationEnabled = true
    //mapView.settings.myLocationButton = true
  }
  
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        //let locationss = locations.last
        
        //let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 17.0)
        
        //self.mapView?.animate(to: camera)
        
        print("AAA-didUpdateLocations")
        
        reverseGeocodeCoordinate(location.coordinate)
        
        locationManager.stopUpdatingLocation()
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        let geocoder = GMSGeocoder()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(),
                let lines = address.lines else {
                    return
            }
            
            self.lblDeliveryAddress.text = lines.joined(separator: "\n")
            
            
            SharedManager.shared.sharedProduct.sourceAddr = self.lblDeliveryAddress.text!
            SharedManager.shared.sharedProduct.sourceLat = "\(coordinate.latitude)"
            SharedManager.shared.sharedProduct.sourceLong = "\(coordinate.longitude)"
            
            setDefaultValue(keyValue: "sAddressName", valueIs: self.lblDeliveryAddress.text!)
            setDefaultValue(keyValue: "sLatitude", valueIs: String(coordinate.latitude))
            setDefaultValue(keyValue: "sLongitude", valueIs: String(coordinate.longitude))
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                self.locationManager.stopUpdatingLocation()
            }
        }
    }
}
