//
//  OrderListCellTwo.swift
//  DelytUser
//
//  Created by Inabia1 on 27/08/2021.
//  Copyright © 2021 Umar Farooq. All rights reserved.
//

import UIKit

class OrderListCellTwo: UITableViewCell {

    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var btnRedeemPoints: UIButton!
    @IBOutlet weak var viewParent: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
