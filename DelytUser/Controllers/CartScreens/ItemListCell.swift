//
//  ItemListCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/20/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class ItemListCell: UITableViewCell {

    @IBOutlet weak var lblKey: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var btnTick: UIButton!
    @IBAction func btnTapped(_ sender: Any) {
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
