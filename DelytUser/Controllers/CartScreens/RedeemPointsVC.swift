//
//  RedeemPointsVC.swift
//  DelytUser
//
//  Created by Inabia1 on 27/08/2021.
//  Copyright © 2021 Umar Farooq. All rights reserved.
//

import UIKit

class RedeemPointsVC: UIViewController {

    @IBOutlet weak var tfWorth: UITextField!
    @IBOutlet weak var tfPoints: UITextField!
    @IBOutlet weak var viewSecond: UIView!
    @IBOutlet weak var lblPointWorth: UILabel!
    @IBOutlet weak var lblRedeemPoint: UILabel!
    @IBOutlet weak var viewPointWorth: UIView!
    @IBOutlet weak var viewRedeemPoint: UIView!
    
    var discount = 0.0
    var totalCount = ""
    var pointID = "0"
    var items: RedeemObj?
    var fromScreen: String?
    
    @IBOutlet weak var btnRedeemIB: UIButton!
    
    @IBAction func btnRedeem(_ sender: Any) {
        if tfPoints.text == "" || tfWorth.text == ""{
            //showToast(viewControl: self, titleMsg: "Error", msgTitle: "Enter Points")
            alertDialog(msg: "Enter Points")
        }
        else {
            postRedeemPoints()
        }
    }
    
    
    @IBAction func tfPointsEditingChanged(_ sender: Any) {
        print("Text Changed Event")
        
        if (self.items?.pointcalculate)! > 0 && self.tfPoints.text != ""{
            
            self.discount = Double ((Double (self.tfPoints.text!)! / Double((self.items?.pointcalculate)!)))
            self.tfWorth.text = String (self.discount)
        }
        else {
            self.tfWorth.text = "0"
        }
       /* DispatchQueue.main.asyncAfter(deadline: .now() +  1.0, execute: {
            print("Text Changed Event")
            
            if (self.items?.pointcalculate)! > 0 && self.tfPoints.text != ""{
                
                self.discount = Double ((Double (self.tfPoints.text!)! / Double((self.items?.pointcalculate)!)))
                self.tfWorth.text = String (self.discount)
            }
            else {
                self.tfWorth.text = "0"
            }
        })*/
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Redeem Points"
        setNavBar()
        
        viewRedeemPoint.layer.cornerRadius = 5
        viewPointWorth.layer.cornerRadius = 5
        viewSecond.layer.cornerRadius = 5
        
        btnRedeemIB.layer.cornerRadius = 5
        
        tfWorth.isEnabled = false
        
        getRedeemPointsDetail()
    }
    
    func setNavBar() {
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        self.navigationItem.leftBarButtonItems = [crossButton]
        
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x5E2447)
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("iPad")
            
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 22),NSAttributedString.Key.foregroundColor: UIColor.white]
            
        }else{
            print("not iPad")
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),NSAttributedString.Key.foregroundColor: UIColor.white]
            
        }
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRedeemPointsDetail() {
        
        APIClient.GetRedeemPointsRequest(cardAmount: totalCount) { (response, error, message) in
            
            if response != nil {
                //print(response)
                self.items = response
                self.setRedeemData(redeemObj: self.items!)
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func setRedeemData(redeemObj: RedeemObj) {
        lblPointWorth.text = String (redeemObj.pointWorth!)
        lblRedeemPoint.text = String (redeemObj.totalpoint!)
        pointID = String (redeemObj.pointId!)
    }
    
    func postRedeemPoints() {
        
        APIClient.PostRedeemRequest(cardAmount: totalCount, redeemAmount: tfPoints.text!, pointWorth: tfWorth.text!) { (response, error, message) in
            
            if response != nil {
                //print(response)
                if response?.status == "True" {
                    print("True")
                    
                    if let user = getUser() {
                        print("RewardPoints: \(response!.result?.totalpoint)")
                        user.rewardPoints = Int((response!.result?.totalpoint)!)
                        setUser(userObj: user)
                    }
                    
                    let object: [String: Any] = ["DiscountAmount": self.tfWorth.text!, "RedeemPoints": self.tfPoints.text!, "PointId" : self.pointID]
                    
                    if self.fromScreen == "AddOrder" {
                        print("Notification Center Add Order")
                        NotificationCenter.default.post(name: ReDeemDiscountBoolNotification.RedeemGiveNotification, object: object)
                    }
                    else {
                        print("Notification Center Update Order")
                        NotificationCenter.default.post(name: ReDeemDiscountBoolNotificationForUpdateOrder.RedeemGiveNotificationUpdateOrder, object: object)
                    }
                    
                    self.alertDialogForSuccess(msg: (response?.message)!)
                    
                }
                else {
                    //showToast(viewControl: self, titleMsg: "", msgTitle: (response?.message)!)
                    self.alertDialog(msg: (response?.message)!)
                    print("False")
                }
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                self.alertDialog(msg: message!)
            }
        }
    }
    
    func alertDialog(msg: String) {
        let optionMenu = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        optionMenu.addAction(okAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func alertDialogForSuccess(msg: String) {
        let optionMenu = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.navigationController?.popViewController(animated: true)
            
        })
        
        optionMenu.addAction(okAction)
        self.present(optionMenu, animated: true, completion: nil)
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
