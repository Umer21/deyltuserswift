//
//  OrderCompleteVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/21/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class OrderCompleteVC: UIViewController {

    var orderNo: String?
    
    @IBAction func btn_OK(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        if let user = getUser()
        {
            lblTitle.text = "Thank You " + user.firstName!  + " for your order"

        }
        
        lblOrderNumber.text = orderNo
        
        self.navigationController?.navigationBar.isHidden = true
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //It will show the status bar again after dismiss
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
