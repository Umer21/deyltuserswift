//
//  OrderListCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/20/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol OrderListDelegate {
    func quantityUpdate(tag: Int, isPlus: Bool)
    func deleteTapped(tag: Int)
}

class OrderListCell: UITableViewCell {
    var delegate: OrderListDelegate?
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblResturantName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblPerItemPrice: UILabel!
    
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    
    @IBAction func btnMinus(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.quantityUpdate(tag: button.tag, isPlus:false)
    }
    
    @IBAction func btnPlus(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.quantityUpdate(tag: button.tag, isPlus:true)
    }
    
    @IBAction func btnCross(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.deleteTapped(tag: button.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()

       //Your separatorLineHeight with scalefactor
       let separatorLineHeight: CGFloat = 15/UIScreen.main.scale

       let separator = UIView()

       separator.frame = CGRect(x: self.frame.origin.x,
                                y: self.frame.size.height - separatorLineHeight,
                            width: self.frame.size.width,
                           height: separatorLineHeight)

       separator.backgroundColor = UIColorFromRGB(rgbValue: 0xf9f9f9)

       self.addSubview(separator)
    }

}
