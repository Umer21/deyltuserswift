//
//  OrderCartVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/20/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import Foundation
import IQKeyboardManagerSwift

class OrderCartVC: UIViewController {

    let reuseIdentifierTable = "OrderListCell"
    let reuseIdentifierTableTwo = "OrderListCellTwo"
    var items = [Product]()
    var orderTypeView : OrderTypeView?
    let dbManager = DatabaseHelper()
    var sharedProduct = SharedManager.shared.requestForSharedProduct()
    var total = 0.0
    var totalItems = 0
    var subTotal = "0.0"
    var redeemDiscount = "0.0"
    var redeemPoints = "0"
    var pointID = "0"
    var selectedPdt = Product()
    var resturantObj: BusinessObj?
    var orderType = "1"
    var orderInstructions = ""
    var orderDeliveryaddress = ""
    var isavaildiscount = "0"
    var isOrderPlaceable = true
    
    private let kSeparatorId = 123
    private let kSeparatorHeight: CGFloat = 1.5
    
    @IBOutlet weak var orderNowIB: UIButton!
    //Instructions View Properties
    @IBOutlet var segment: UISegmentedControl!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var instructionsView: UIView!
    @IBOutlet weak var txtInstructions: UITextView!
    @IBOutlet weak var txtDeliveryAddress: UITextField!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblAlergies: UILabel!
    @IBAction func doneTapped(_ sender: Any) {
        
        if orderType == "1" || orderType == "2"{
            removeViewWithAnimation()
        }
        
        if isValid() {
            self.doneTapped(deliveryAddress: txtDeliveryAddress.text!, Instructions: txtInstructions.text!, orderType: orderType)
        }
        
    }
    
    @IBAction func segmentControl(_ sender: Any) {
        let title = segment.titleForSegment(at: self.segment.selectedSegmentIndex)
        if title == "DineIn" {
            orderType = "1"
            txtDeliveryAddress.text = ""
            txtDeliveryAddress.isEnabled = false
        }else if title == "TakeOut" {
            orderType = "2"
            txtDeliveryAddress.text = ""
            txtDeliveryAddress.isEnabled = false
        }else if title == "Delivery" {
            orderType = "3"
            txtDeliveryAddress.isEnabled = true
            
            //TODO: new-work
            self.gotoLocationScreen()
        }
    }
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBAction func orderNowPressed(_ sender: Any) {
        txtDeliveryAddress.text = ""
        
        addNibView()
        /* local handling when dinein, takeout, delivery is off no api call then
        if isOrderPlaceable {
            
        }else{
            showToast(viewControl: self, titleMsg: "", msgTitle: "Order can not be placed.")
        }*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Order"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(OrderCartVC.self)
        instructionsView.isHidden = true
        
        let titleTextAttributesN = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let titleTextAttributesS = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segment.setTitleTextAttributes(titleTextAttributesN, for: .normal)
        segment.setTitleTextAttributes(titleTextAttributesS, for: .selected)
        
        orderNowIB.layer.cornerRadius = 5
        tblView.separatorStyle = .none
        tblView.backgroundColor = UIColorFromRGB(rgbValue: 0xececec)
        
        
        //Segment Dynaimic for dine in, take out options
        if let resturant = self.resturantObj {
            
            self.segment.removeAllSegments()
            
            var items = [String]()
            
            if resturant.IsAllowDineIn == true {
                items.append("DineIn")
            }
            if resturant.IsAllowTakeOut == true {
                items.append("TakeOut")
            }
            if resturant.IsAllowDelivery == true {
                items.append("Delivery")
            }
            
            if items.count == 0 {
                isOrderPlaceable = false
            }
            
            for (index,item) in items.enumerated() {
                self.segment.insertSegment(withTitle: item, at: index, animated: false)
            }
            
            self.segment.selectedSegmentIndex = 0;
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.reloadTable()
        registerNotifications()
        //apiPlaceOrder()
        
        
        if getValueForKey(keyValue: "dAddressName") != "" {
            txtDeliveryAddress.text = getValueForKey(keyValue: "dAddressName")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //unregisterNotifications()
    }
    
    func gotoLocationScreen(){
        
        let storyboard = UIStoryboard(name: kCartStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kLocationUpdateControllerID) as! SelectLocationVC
        vc.isFromDelivery = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(setGivenDiscountValue), name: ReDeemDiscountBoolNotification.RedeemGiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(fromItemCartScreenUpdate), name: ItemCartBoolNotification.ItemCartNotification, object: nil)
    }
    
    @objc func setGivenDiscountValue(notification: NSNotification) {
        
        if let object = notification.object as? [String: Any] {
            if let discount = object["DiscountAmount"] as? String {
                self.redeemDiscount = discount
                print(discount)
                
                if discount == "0.0" {
                    self.isavaildiscount = "0"
                }
                else {
                    self.isavaildiscount = "1"
                }
            }
            if let redeemPonts = object["RedeemPoints"] as? String {
                self.redeemPoints = redeemPonts
                print(redeemPonts)
            }
            
            if let pointid = object["PointId"] as? String {
                self.pointID = pointid
                print(pointid)
            }
        }
    }
    
    @objc func fromItemCartScreenUpdate(notification: NSNotification) {
        
        if let object = notification.object as? [String: Any] {
            
            if let itemCartNotif = object["FromItemCart"] as? String {
                let itemBoll = itemCartNotif
                print("From-ItemCart-VC: \(itemBoll)")
                
                self.redeemDiscount = "0.0"
            }
        }
    }
    

    func gotoItemListScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kItemCartControllerID) as! ItemCartVC
        vc.product = selectedPdt
        vc.editingMode = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoOrderCompleteVC(orderNumber : String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kOrderCompleteControllerID) as! OrderCompleteVC
        vc.orderNo = orderNumber
        //vc.editingMode = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func addNibView() {
        
        //Guest User Handling
        if getUser() != nil {
            
            addViewWithAnimation()
            
            
            /*
            orderTypeView = Bundle.main.loadNibNamed("OrderTypeView", owner: self, options: nil)?.last as? OrderTypeView
            orderTypeView!.delegate = self
            //self.view.isUserInteractionEnabled = false
            //orderTypeView?.isUserInteractionEnabled = true
            self.view.addSubview(self.orderTypeView!)
            orderTypeView?.addViewWithAnimation()
            
            orderTypeView!.translatesAutoresizingMaskIntoConstraints = false
            orderTypeView!.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,
                                     constant: 0).isActive = true
            orderTypeView!.bottomAnchor.constraint(equalTo: self.view!.bottomAnchor,
                                 constant: 0).isActive = true
            orderTypeView!.heightAnchor.constraint(equalToConstant: 365).isActive = true
            orderTypeView!.widthAnchor.constraint(equalToConstant: self.view.frame.size.width).isActive = true*/
            
       }else{
            //guest user case
            let optionMenu = UIAlertController(title: "", message: "This feature requires an account.", preferredStyle: .alert)
            
            let LoginAction = UIAlertAction(title: "Login", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.gotoLoginScreen()
            })
            
            let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in })
            
            optionMenu.addAction(LoginAction)
            optionMenu.addAction(CancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func addViewWithAnimation() {
        self.tblView.isUserInteractionEnabled = false
        
        //Animation
        instructionsView.alpha = 0
        txtInstructions.alpha = 0
        segment.alpha = 0
        txtDeliveryAddress.alpha = 0
        seperatorView.alpha = 0
        btnDone.alpha = 0
        lblAlergies.alpha = 0
        
        //self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromTop, animations: {
            //Animation
            self.instructionsView.isHidden = false
            
            self.instructionsView.alpha = 1
            self.txtInstructions.alpha = 1
            self.segment.alpha = 1
            self.txtDeliveryAddress.alpha = 1
            self.seperatorView.alpha = 1
            self.btnDone.alpha = 1
            self.lblAlergies.alpha = 1
            
            //self.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        }, completion: {(finished) in
            //self.currentTabBar?.setBar(hidden: true, animated: true)
            //self.navigationController?.isNavigationBarHidden = true
        })
    }
    
    func removeViewWithAnimation() {
        self.tblView.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromBottom, animations: {
            self.instructionsView.alpha = 0
            self.txtInstructions.alpha = 0
            self.segment.alpha = 0
            self.txtDeliveryAddress.alpha = 0
            self.seperatorView.alpha = 0
            self.btnDone.alpha = 0
            self.lblAlergies.alpha = 0
            
        }, completion: { (finished) in
            //self.currentTabBar?.setBar(hidden: false, animated: true)
            //self.navigationController?.isNavigationBarHidden = false
            //self.removeFromSuperview()
        })
    }
    
    func isValid() -> Bool {
        var returnValue = true
        
        if orderType == "3" {
            if txtDeliveryAddress.text!.isEmpty {
                //message = "Please enter delivery address."
                returnValue = false
            }
        }
        
        if !returnValue {
            showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "Address Required.")
        }
        
        return returnValue
    }
    
    func doneTapped(deliveryAddress: String, Instructions: String, orderType: String) {
        self.orderType = orderType
        self.orderInstructions = Instructions
        self.orderDeliveryaddress = deliveryAddress
        
        showOrderPlaceAlert()
        
    }
    
    func gotoLoginScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
        let navCon = UINavigationController(rootViewController: welcomeVC)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = navCon
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func apiPlaceOrder() {
        
        let arrProducts = dbManager.getAllProducts(isTopupOrders: false, resturantId: "\(resturantObj!.BusinessId!)")
        var arrDict = [[String: Any]]()
        var JSONText = ""
        let resturantId = "\(arrProducts[0].restaurantId!)"
        
        
        for product in arrProducts {
            let dicts = ["MenuId" : product.menuId!,
                         "Quantity": product.orderQuantity!,
                         "UnitPrice": product.itemPrice!,
                         "MenuOptionIds": product.menuOptionId!] as [String : Any]
            arrDict.append(dicts)
        }
        
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: arrDict, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            JSONText = String(data: jsonData, encoding: .utf8)!
            
            // you can now cast it with the right type
            /*
            if let dictFromJSON = decoded as? [String:String] {
                // use dictFromJSON
                //print(dictFromJSON)
            }*/
        } catch {
            print(error.localizedDescription)
        }
       
        print(JSONText)
        APIClient.PlaceOrderRequest(jsonData: JSONText, businessId: resturantId, orderType: orderType, deliveryAdress: orderDeliveryaddress, Instructions: orderInstructions, DiscountAmount: self.redeemDiscount, DiscountPoint: self.redeemPoints, pointId: self.pointID, isAvailDiscount: self.isavaildiscount) { (response, error, message) in
            
            if response != nil {
                if response?.status == "True" {
                    
                    
                    if let user = getUser() {
                        print("RewardPoints: \(String(describing: response!.RewardPoints))")
                        user.rewardPoints = Int((response!.RewardPoints)!)
                        setUser(userObj: user)
                    }
                    
                   self.dbManager.deleteAllProducts(topUpOrder: false)
                    self.gotoOrderCompleteVC(orderNumber: String (response!.orderId!))
                }else{
                    showToast(viewControl: self, titleMsg: "Error", msgTitle: response!.message!)
                }
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
            }
        }
    }
    
    func calculateTotalPrice() {
        for item in self.items {
            let total = Double(item.orderQuantity!) * item.itemPrice!
            self.total = total + self.total
            let final_total = self.total - Double (redeemDiscount)!
            lblTotal.text = "$\(String(format: "%.2f", final_total))"
            
            totalItems = self.tblView.numberOfRows(inSection: 0)
            subTotal = "\(String(format: "%.2f", self.total))"
        }
    }
    
    

}

extension OrderCartVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.items.count
        
        switch section {
        case 0:
            return self.items.count
        case 1:
            return 1
        default :
            return self.items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable)
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: reuseIdentifierTable)
        }
        
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! OrderListCell
            
            let model = self.items[indexPath.row]
            
            cell.lblProductName.text = model.menuName!
            cell.lblResturantName.text = model.restauntName!
            cell.lblQuantity.text = "\(model.orderQuantity!)"
            
            let total = Double(model.orderQuantity!) * model.itemPrice!
            print("$\(total.rounded(toPlaces: 2))")
            //cell.lblPrice.text = "$\(total.rounded(toPlaces: 2))"
            cell.lblPrice.text = "$\(String(format: "%.2f", total))"
            
            //cell.lblPerItemPrice.text = "$\(model.itemPrice!.rounded(toPlaces: 2))"
            cell.lblPerItemPrice.text = "$ \(String(format: "%.2f", model.itemPrice!))"
            
            
            cell.delegate = self
            cell.btnMinus.tag = indexPath.row
            cell.btnPlus.tag = indexPath.row
            cell.btnCross.tag = indexPath.row
            
            return cell
            
        case 1:
            let cellTwo = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTableTwo, for: indexPath as IndexPath) as! OrderListCellTwo
            
            //cellTwo.viewParent.addBorder
            cellTwo.viewParent.layer.borderWidth = 0.5
            cellTwo.viewParent.layer.cornerRadius = 5
            cellTwo.viewParent.layer.borderColor = UIColorFromRGB(rgbValue: 0xacacac).cgColor
            
            cellTwo.lblItem.text = String (totalItems)
            cellTwo.lblSubTotal.text = ("$ \(subTotal)")
            
            let redeemDic = Double (redeemDiscount)//String(format: "%.2f", currentRatio)
            if redeemDic! > 0 {
                cellTwo.lblDiscount.text = String ("($ \((String(format: "%.2f", Double (redeemDic!)))))")
            }
            else {
                cellTwo.lblDiscount.text = String ("$ \((String(format: "%.2f", Double (redeemDic!))))")
            }
            
            
            cellTwo.btnRedeemPoints.addTarget(self, action: #selector(gotoRedeemPointScreen), for: .touchUpInside)
            return cellTwo
            
        default:
            break
        }
        
        return cell1!
    }
    
    @objc func gotoRedeemPointScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kRedeemPointsControllerID) as! RedeemPointsVC
        vc.totalCount = subTotal
        vc.fromScreen = "AddOrder"
        //vc.editingMode = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            let model = self.items[indexPath.row]
            selectedPdt = model
            gotoItemListScreen()
        case 1:
            print("Section-2")
            
        default:
            print("Default")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if UIDevice.current.userInterfaceIdiom == .pad { return 98 } else { return 78 }
        } else {
            return 140
        }
        
    }
    

    
}

extension OrderCartVC: OrderListDelegate {
    func quantityUpdate(tag: Int, isPlus: Bool) {
        let model = self.items[tag]
        sharedProduct = model
        if isPlus {
            //Plus Function
            sharedProduct.orderQuantity = model.orderQuantity! + 1
            dbManager.updateProduct(product: sharedProduct)
        }else{
            //Minus Function
            sharedProduct.orderQuantity = model.orderQuantity! - 1
            
            if sharedProduct.orderQuantity! <= 0 {
                showAlert(title: "Quantity", message: "Item quantity cannot go below 1", viewController: self)
                return
            }
            dbManager.updateProduct(product: sharedProduct)
        }
        
        self.redeemDiscount = "0.0"

        reloadTable()
    }
    
    func deleteTapped(tag: Int) {
        
        let deleteMenu = UIAlertController(title: "", message: "Are you sure you want to remove this item from your order?", preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let model = self.items[tag]
            self.sharedProduct = model
            self.dbManager.deleteProduct(product: self.sharedProduct)
            self.redeemDiscount = "0.0"
            
            self.reloadTable()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        deleteMenu.addAction(cancel)
        deleteMenu.addAction(deleteAction)
        self.present(deleteMenu, animated: true, completion: nil)
        
    }
    
    func showOrderPlaceAlert() {
        let placeMenu = UIAlertController(title: "Confirm your order now?", message: "", preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.apiPlaceOrder()
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        placeMenu.addAction(cancel)
        placeMenu.addAction(YesAction)
        
        self.present(placeMenu, animated: true, completion: nil)
    }
    
    func reloadTable() {
        self.total = 0.0
        self.items = dbManager.getProdcutWithOrderId(orderId: 0, resturantId: "\(resturantObj!.BusinessId!)")
        
        if self.items.count == 0 {
            //when product quantity go to zero
            self.navigationController?.popViewController(animated: true)
        }
        
        tblView.reloadData()
        calculateTotalPrice()
    }
    
}

extension OrderCartVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        animateUp()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateDown()
    }
}

extension OrderCartVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        animateUp()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateDown()
    }
    
    func animateUp() {
        UIView.animate(withDuration: 0.3, animations: {
            self.bottomConstraint.constant = 245
            self.view.layoutIfNeeded()
            
        }) { (completed) in
            
        }
    }
    
    func animateDown() {
        UIView.animate(withDuration: 0.3, animations: {
            self.bottomConstraint.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (completed) in
            
        }
    }
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
