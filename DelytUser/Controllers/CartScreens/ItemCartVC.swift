//
//  ItemCartVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/20/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import Foundation

class ItemCartVC: UIViewController {
    let dbManager = DatabaseHelper()
    let reuseIdentifierTable = "ItemListCell"
    
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAddOrder: UIButton!
    
    var optionList = [optionObj]()
    var arrSelectedCells = [IndexPath]()
    var product = Product()
    var totalAmount = 0.0
    var editingMode = false
    var isTopUp = false
    
    @IBAction func minusTapped(_ sender: Any) {
        //Minus Function
        product.orderQuantity = product.orderQuantity! - 1
        
        if product.orderQuantity! <= 0 {
            showAlert(title: "Quantity", message: "Item quantity cannot go below 1", viewController: self)
            product.orderQuantity = product.orderQuantity! + 1
            return
        }
        
        //handling for first time add product in db
        if product.pkId != nil {
            dbManager.updateProduct(product: product)
        }
        
        lblQuantity.text = "\(product.orderQuantity!)"
    }
    
    @IBAction func plusTapped(_ sender: Any) {
        //Plus Function
        product.orderQuantity = product.orderQuantity! + 1
        
        //handling for first time add product in db
        if product.pkId != nil {
            dbManager.updateProduct(product: product)
        }
        
        lblQuantity.text = "\(product.orderQuantity!)"
    }
    @IBAction func addToOrder(_ sender: Any) {
        
        //print("combination = \(String(describing: product.menuOptionId!))")
        if editingMode {
            print("Editing Mode")
            //old state of product base price
            product.itemPrice = product.basePrice
        }
        
        //PRICE CALCULATION:
        //selected menu option id added in db product
        var menuOptionIDs = ""
        var sumAllVariations = 0.0
        var finalPrice = 0.0
    
        print("Total variations = \(self.optionList.count)")
        
        for (section, item) in optionList.enumerated() {
            for (row, option) in (item.menuOptionAdd?.enumerated())! {
                
                let searchIP = IndexPath(row: row, section: section)
                if self.arrSelectedCells.contains(searchIP) {
                    menuOptionIDs.append("\(String(describing: option.MenuOptionId!)),")
                    
                    //Condition #0
                    if item.optionAdd?.IsAddAmount == false {
                        print("Condition #0")
                        
                        finalPrice = finalPrice + 0
                        //finalPrice = product.itemPrice!
                        
                    }//Condition #1 + (Sum of All Variations)
                    else if item.optionAdd?.IsAddAmount == true && item.optionAdd?.IsMultiSelect == true && item.optionAdd?.IsAmountComulative == true {
                    
                        print("Condition #1")
                        if let price = Double(option.MenuOptionValue!) {
                            sumAllVariations = sumAllVariations + price
                        }
                        
                        finalPrice = product.itemPrice! + sumAllVariations
                        
                        
                    }//Condition #2
                    else if item.optionAdd?.IsAddAmount == true && item.optionAdd?.IsMultiSelect == true && item.optionAdd?.IsAmountComulative == false {
                        
                        print("Condition #2")
                        if let price = Double(option.MenuOptionValue!) {
                            sumAllVariations = sumAllVariations + price
                        }
                        
                        finalPrice = sumAllVariations
                        
                    }//Condition #3
                    else if item.optionAdd?.IsAddAmount == true && item.optionAdd?.IsMultiSelect == false && item.optionAdd?.IsAmountComulative == true {
                        
                        print("Condition #3")
                        if let price = Double(option.MenuOptionValue!) {
                            sumAllVariations = sumAllVariations + price
                        }
                        
                        finalPrice = product.itemPrice! + sumAllVariations
                        
                    }//Condition #4
                    else if item.optionAdd?.IsAddAmount == true && item.optionAdd?.IsMultiSelect == false && item.optionAdd?.IsAmountComulative == false {
                        
                        print("Condition #4")
                        if let price = Double(option.MenuOptionValue!) {
                            sumAllVariations = sumAllVariations + price
                        }
                        
                        finalPrice = sumAllVariations
                        
                    }
                    else{
                        //Condition # 5
                        finalPrice = product.itemPrice!
                    }
                }
            }
        }
        
        //Adjustments for conidtion #5
        /*
        if (finalPrice < 1) {
            finalPrice = finalPrice + product.itemPrice!
        }*/
        
        if (finalPrice == 0) {
            finalPrice =  product.itemPrice!
        }
        
        
        print("===================================")
        print("Base price = \(product.itemPrice!)")
        print("Sum of All Variations = \(sumAllVariations)")
        print("Final Price = \(finalPrice)")
        print("===================================")
        
        product.menuOptionId = String(menuOptionIDs.dropLast())
        print("combination = \(String(describing: product.menuOptionId!))")
        
        //empty handling
        if product.menuOptionId == "" {
            showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "Please select a variation.")
            return
        }
        
        product.basePrice = product.itemPrice!
        product.itemPrice = finalPrice
        
        let products = dbManager.getProdcutWithCombination(optionsIds: product.menuOptionId!, product: product)
        if products.count > 0 {
            
            //Quantity Updated
            product = products[0]
            
            if !editingMode {
                product.orderQuantity = product.orderQuantity! + 1
            }
            
            dbManager.updateProduct(product: product)
            showToast(viewControl: self, titleMsg: "", msgTitle: "Item updated in cart.")
        }else{
            
            
            if editingMode {
                //Update Old Product
                //product = products[0]
                //product.orderQuantity = product.orderQuantity! + 1
                dbManager.updateProduct(product: product)
                showToast(viewControl: self, titleMsg: "", msgTitle: "Item updated in cart.")
                
            }else{
                //Add New Product
                dbManager.insertProduct(product: product)
                showToast(viewControl: self, titleMsg: "", msgTitle: "Item added in cart.")
            }
            
            
            
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
            
            let object: [String: Any] = ["FromItemCart": "True", "RedeemPoints": "True"]
            NotificationCenter.default.post(name: ItemCartBoolNotification.ItemCartNotification, object: object)
            
            if self.isTopUp {
                self.backToAcceptedScreen()
            }else{
                //if editing mode then 1 back
                if self.editingMode {
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.popToMenu()
                }
                
            }
        })
    }
    
    
    func popToMenu() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for controller in viewControllers {
                if controller.isKind(of: ResturantVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func backToAcceptedScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for controller in viewControllers {
                if controller.isKind(of: AcceptedScreenVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.separatorStyle = .none
        print(product.optionJson!)
        lblQuantity.text = "\(product.orderQuantity!)"
        decodeRecordsJson()
        
        self.title = product.menuName!
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        if (!editingMode) {
            makingIndexForFirstTime()
            btnAddOrder.setTitle("Add to Order", for: .normal)
        }else{
            fetchOldSelectedIndexes()
            btnAddOrder.setTitle("Update Item", for: .normal)
        }
        
        btnAddOrder.layer.cornerRadius = 4
        
    }
    
    
    func fetchOldSelectedIndexes() {
        if product.menuOptionId!.count > 0 {
            
            let variationsIds = product.menuOptionId?.components(separatedBy: ",").map { Int($0)!}
            
            for (indexSec, element) in self.optionList.enumerated() {
                for (indexRow, item) in (element.menuOptionAdd?.enumerated())! {
                    for id in variationsIds!.enumerated() {
                        if id.element == item.MenuOptionId {
                            let indexP = IndexPath(row: indexRow, section: indexSec)
                            arrSelectedCells.append(indexP)
                        }
                    }
                }
            }
        }
       
        
        tblView.reloadData()
    }
    
    func dummydata() {
        //
        let indexP = IndexPath(row: 0, section: 0)
        arrSelectedCells.append(indexP)
        
        let indexP2 = IndexPath(row: 1, section: 1)
        arrSelectedCells.append(indexP2)
        
        let indexP3 = IndexPath(row: 2, section: 4)
        arrSelectedCells.append(indexP3)
    }
    
    func makingIndexForFirstTime() {
        
        for (index, _) in self.optionList.enumerated() {
            
            if (self.optionList[index].optionAdd?.IsSelectable == true) {
                
                ////default selection of 1st item
                let indexP = IndexPath(row: 0, section: index)
                if (!arrSelectedCells .contains(indexP)) {
                    arrSelectedCells.append(indexP)
                }
            }
            
            
            if (self.optionList[index].optionAdd?.IsAddAmount == true && self.optionList[index].optionAdd?.IsSelectable == true) {
                //totalAmount = totalAmount + Double(self.optionList[index].menuOptionAdd![0].MenuOptionValue!)
            }
    }
    
}
    
    func decodeRecordsJson() {
        //Decoding to normal object
        let data = product.optionJson?.data(using: .utf8);
        do {
            let arr = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [NSDictionary]
            
            for item in arr {
                
                var options = optionObj()
                options.menuOptionAdd = [menuOptionAdd]()
                options.optionAdd = optionAdd()
                
                //print(item.value(forKeyPath: "OptionAdd.OptionName")!)
                options.optionAdd!.IsKeyValue = item.value(forKeyPath: "OptionAdd.IsKeyValue")! as? Bool
                options.optionAdd!.OptionId = item.value(forKeyPath: "OptionAdd.OptionId")! as? Int
                options.optionAdd!.IsSelectable = item.value(forKeyPath: "OptionAdd.IsSelectable")! as? Bool
                options.optionAdd!.IsAddAmount = item.value(forKeyPath: "OptionAdd.IsAddAmount")! as? Bool
                options.optionAdd!.IsMultiSelect = item.value(forKeyPath: "OptionAdd.IsMultiSelect")! as? Bool
                options.optionAdd!.OptionName = item.value(forKeyPath: "OptionAdd.OptionName")! as? String
                options.optionAdd!.IsActiveOption = item.value(forKeyPath: "OptionAdd.IsActiveOption")! as? Int
                options.optionAdd!.IsSelectionMandatory = item.value(forKeyPath: "OptionAdd.IsSelectionMandatory")! as? Bool
                options.optionAdd!.IsAmountComulative = item.value(forKeyPath: "OptionAdd.IsAmountComulative")! as? Bool
                
                
                let arrMenu = item["MenuOptionAdd"] as! [NSDictionary]
                for dict in arrMenu {
                    var menu = menuOptionAdd()
                    menu.MenuOptionId = dict.value(forKeyPath: "MenuOptionId")! as? Int
                    menu.MenuOptionValue = dict.value(forKeyPath: "MenuOptionValue") as? String
                    menu.MenuOptionKey = dict.value(forKeyPath: "MenuOptionKey")! as? String
                    options.menuOptionAdd?.append(menu)
                    //print(dict.value(forKeyPath: "MenuOptionKey")!)
                }
                
                optionList.append(options)
            }
            
            tblView.reloadData()
            
        } catch {
            print("json error: \(error.localizedDescription)")
        }
    }

}

extension ItemCartVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.optionList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.optionList[section].menuOptionAdd!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! ItemListCell
        
        let model = self.optionList[indexPath.section].menuOptionAdd![indexPath.row]
        cell.lblKey.text = model.MenuOptionKey
        if model.MenuOptionValue == " " {
            cell.lblValue.text = ""
        }else{
            //Nil handling, when no price appears in item
            if model.MenuOptionValue == nil {
                cell.lblValue.text = ""
            }else{
                cell.lblValue.text = "$ \(String(describing: model.MenuOptionValue!))"
            }
        }
        
        
        let value = self.isRowSelected(tableView, indexPath: indexPath)
        cell.btnTick.isHidden = value
        
        return cell
    }
    
    func isRowSelected(_ tableView: UITableView, indexPath: IndexPath) -> Bool {
        return (self.arrSelectedCells.contains(indexPath)) ? false : true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let model = self.optionList[indexPath.section].menuOptionAdd![indexPath.row]
        
        let modelOptions = self.optionList[indexPath.section].optionAdd
        print("========================")
        print("IsAddAmount= \(modelOptions!.IsAddAmount!)")
        print("IsKeyValue= \(modelOptions!.IsKeyValue!)")
        print("IsSelectable= \(modelOptions!.IsSelectable!)")
        print("IsMultiSelect= \(modelOptions!.IsMultiSelect!)")
        print("IsAmountComulative= \(modelOptions!.IsAmountComulative!)")
        print("========================")
        
        if modelOptions?.IsSelectable == false { return } // return if no selection is on
        
        // handling for single-selection
        if modelOptions?.IsMultiSelect == false {
            //remove all items of section
            for item in self.arrSelectedCells {
                if indexPath.section == item.section {
                    self.arrSelectedCells.remove(element: item)
                }
            }
            //add current
            self.arrSelectedCells.append(indexPath)
            
            
        }
        
        // handling for multi-selection
        if modelOptions?.IsMultiSelect == true {
            //add current
            if (!self.arrSelectedCells.contains(indexPath)) {
                self.arrSelectedCells.append(indexPath)
            }else{
                self.arrSelectedCells.remove(element: indexPath)
            }
        }
        
        //reload
        //tableView.reloadSections(IndexSet(indexPath), with: .fade)
        //tableView.reloadRows(at: [indexPath], with: .fade)
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadData()
        for item in self.arrSelectedCells {
            print(item)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.optionList[section].optionAdd?.OptionName!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        /*
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 75
        }else{
            
        }*/
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
           return 45
            
        }else{
            return 30
        }
        
    }
}

struct optionObj {
    var menuOptionAdd: [menuOptionAdd]?
    var optionAdd: optionAdd?
    init(){}
    
    init(_ dictionary: [String: Any]) {
        self.menuOptionAdd = (dictionary["MenuOptionAdd"] as? [menuOptionAdd])
        self.optionAdd = dictionary["OptionAdd"] as? optionAdd
    }
}

struct menuOptionAdd {
    var MenuOptionKey: String?
    var MenuOptionValue: String?
    var MenuOptionId: Int?
    init(){}
    
    init(_ dictionary: [String: Any]) {
        self.MenuOptionId = dictionary["MenuOptionId"] as? Int ?? 0
        self.MenuOptionKey = dictionary["MenuOptionKey"] as? String ?? ""
        self.MenuOptionValue = dictionary["MenuOptionValue"] as? String ?? ""
    }
}

struct optionAdd {
    var IsKeyValue: Bool?
    var OptionId: Int?
    var IsSelectable: Bool?
    var IsAddAmount: Bool?
    var IsMultiSelect: Bool?
    var OptionName: String?
    var IsActiveOption: Int?
    var IsSelectionMandatory: Bool?
    var IsAmountComulative: Bool?
    
    init(){}
    
    init(_ dictionary: [String: Any]) {
        self.OptionName = dictionary["OptionName"] as? String ?? ""
        self.IsKeyValue = dictionary["IsKeyValue"] as? Bool ?? false
        self.OptionId = dictionary["OptionId"] as? Int ?? 0
        self.IsSelectable = dictionary["IsSelectable"] as? Bool ?? false
        self.IsAddAmount = dictionary["IsAddAmount"] as? Bool ?? false
        self.IsMultiSelect = dictionary["IsMultiSelect"] as? Bool ?? false
    }
}
