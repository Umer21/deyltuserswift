//
//  OrderTypeView.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/20/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol OrderViewDelegate {
    func dismissView()
    func doneTapped(deliveryAddress: String, Instructions: String, orderType: String)
    func showValidation()
    
    func fieldBecomeActive()
    func fieldBecomeDeactive()
}

class OrderTypeView: UIView {
    var delegate: OrderViewDelegate?
    var orderType = "1"
    
    @IBOutlet weak var constraintHdelivery: NSLayoutConstraint!
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblAlergy: UILabel!
    @IBOutlet weak var txtInstructions: UITextView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var txtDeliveryAddress: UITextField!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        self.txtDeliveryAddress.delegate = self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //commonInit()
       
       
    }
    @IBAction func segValueChanged(_ sender: Any) {
        let title = segment.titleForSegment(at: self.segment.selectedSegmentIndex)
        if title == "DineIn" {
            orderType = "1"
            txtDeliveryAddress.text = ""
            txtDeliveryAddress.isEnabled = false
        }else if title == "TakeOut" {
            orderType = "2"
            txtDeliveryAddress.text = ""
            txtDeliveryAddress.isEnabled = false
        }else if title == "Delivery" {
            orderType = "3"
            txtDeliveryAddress.isEnabled = true
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
         //self.txtDeliveryAddress.delegate = self
    }
    @IBAction func doneTapped(_ sender: Any) {
        
        if orderType == "1" || orderType == "2"{
            self.delegate?.dismissView()
        }
        
        
        if isValid() {
            self.delegate?.doneTapped(deliveryAddress: txtDeliveryAddress.text!, Instructions: txtInstructions.text!, orderType: orderType)
        }
        
    }
    
    func isValid() -> Bool {
        var returnValue = true
        
        if orderType == "3" {
            if txtDeliveryAddress.text!.isEmpty {
                //message = "Please enter delivery address."
                returnValue = false
            }
        }
        
        if !returnValue {
            self.delegate?.showValidation()
        }
        
        return returnValue
    }
    
    
    func addViewWithAnimation() {
        
        //Animation
        txtInstructions.alpha = 0
        segment.alpha = 0
        txtDeliveryAddress.alpha = 0
        sepratorView.alpha = 0
        btnDone.alpha = 0
        lblAlergy.alpha = 0
        
        //self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromTop, animations: {
            //Animation
            self.txtInstructions.alpha = 1
            self.segment.alpha = 1
            self.txtDeliveryAddress.alpha = 1
            self.sepratorView.alpha = 1
            self.btnDone.alpha = 1
            self.lblAlergy.alpha = 1
            
            //self.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        }, completion: {(finished) in
            //self.currentTabBar?.setBar(hidden: true, animated: true)
            //self.navigationController?.isNavigationBarHidden = true
        })
    }
    
    func removeViewWithAnimation() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromBottom, animations: {
            self.txtInstructions.alpha = 0
            self.segment.alpha = 0
            self.txtDeliveryAddress.alpha = 0
            self.sepratorView.alpha = 0
            self.btnDone.alpha = 0
            self.lblAlergy.alpha = 0
            
        }, completion: { (finished) in
            //self.currentTabBar?.setBar(hidden: false, animated: true)
            //self.navigationController?.isNavigationBarHidden = false
            self.removeFromSuperview()
        })
    }
}

extension OrderTypeView: UITextViewDelegate {
    
}

extension OrderTypeView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.delegate?.fieldBecomeDeactive()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.delegate?.fieldBecomeActive()
        return true
    }
}
