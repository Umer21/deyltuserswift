//
//  ScanCardVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import Braintree
import PKHUD
import JGProgressHUD
import WebKit

class ScanCardVC: UIViewController {

    var wePay = WePay()
    var accountId = Int()
    var isTermChecked = false
    var isFromInternalScreen = false
    var isFbUser = false
    
    var userObj = userObject()
    var cardId = ""
    var selectedImage = UIImage()
    var riskToken = ""
    var ipAddress = ipAddressMethod()
    var clientId = ""
    var cardNo = ""
    var cardExpiry = ""
    var cvv = ""
    
    @IBOutlet weak var webViewRiskToken: WKWebView!
    @IBOutlet weak var lblTermTxt: UILabel!
    @IBOutlet weak var btnOnLbl: UIButton!
    @IBOutlet weak var lblDefaultCard: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var defaultSwitch: UISwitch!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var sepView: UIView!
    @IBOutlet weak var cardView: BTUICardFormView!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    let hud = JGProgressHUD(style: .dark)
   
    
    @IBAction func termsCheckTapped(_ sender: Any) {
        
        if !isTermChecked {
            btnTerms .setImage(UIImage(named: "icon_checkbox_tick"), for: .normal)
        }else{
            btnTerms .setImage(UIImage(named: "icon_checkbox"), for: .normal)
        }
        isTermChecked = !isTermChecked
    }
    
    @IBAction func SkipCardTapped(_ sender: Any) {
        if !isFromInternalScreen {
            if !isTermChecked {
                showToast(viewControl: self, titleMsg: "", msgTitle: "Please accept Terms & Conditions.")
                return
            }
        }
        
        apiSignUp(withCard: false)
    }
    
    @IBAction func FinishTapped(_ sender: Any) {

        if !isFromInternalScreen {
            if !isTermChecked {
                showToast(viewControl: self, titleMsg: "", msgTitle: "Please accept Terms & Conditions.")
                return
            }
        }
        
        let btCard = BTCard()
        btCard.number = self.cardView.number
        btCard.expirationMonth = self.cardView.expirationMonth
        btCard.expirationYear = self.cardView.expirationYear
        btCard.cvv = self.cardView.cvv
        
        
        if (!self.cardView.valid) {
            showAlert(title: "", message: "Invalid Card Details", viewController: self)
            return
        }
        
        // elavon payment method work
        self.cardNo = "\(btCard.number!)"
        self.cardExpiry = "\(btCard.expirationMonth!)/\(btCard.expirationYear!)"
        self.cvv = "\(btCard.cvv!)"
        
        if self.isFromInternalScreen {
            //self.apiAddCard()
            self.apiAddCardElavon(cardNumber: self.cardNo, expiry: self.cardExpiry, cvv: "\(btCard.cvv!)")
        }else{
            //self.apiAddForSignupCard()
            self.apiSignUp(withCard: true)
        }
        
    /*
        
       //new work coverage goes here
        print("\(userObj.email)")
        APIClient.GetCardIDRequest(clientId: self.clientId, riskToken: self.riskToken, ip: self.ipAddress!, expMon: btCard.expirationMonth!, expYear: btCard.expirationYear!, cvv: btCard.cvv!, cardNo: btCard.number!, email: "\(userObj.email)", userName: "\(userObj.fname) \(userObj.lname)", postalCode: userObj.zipCode) { (response, error, message) in
            
            
            if response != nil {
                self.cardId = response!
                
                if self.isFromInternalScreen {
                    self.apiAddCard()
                }else{
                    //self.apiAddForSignupCard()
                    self.apiSignUp(withCard: true)
                }
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
            
        }
 */
        /*
        let paymentInfo = WPPaymentInfo(firstName: userObj.fname, lastName: userObj.lname, email: "support@delytapp.com", billingAddress: WPAddress(zip: userObj.zipCode), shippingAddress: nil, cardNumber: btCard.number, cvv: btCard.cvv, expMonth: btCard.expirationMonth, expYear: btCard.expirationYear, virtualTerminal: false)
        
        */
        //hud.show(in: self.view)
        
        //OLD method to get wepay token
        //self.wePay .tokenizePaymentInfo(paymentInfo, tokenizationDelegate: self)
        
    }
    
    func apiAddCardConverge(){
        APIClient.AddCardConvergeRequest(customerId: "", isDefaultCard: "", cardNo: "", expiryDate: "", cvv: "", firstName: "", lastName: "", zipcode: "") { (response, error, message) in
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        // Do any additional setup after loading the view.
        userObj = SharedManager.shared.requestForUser()
      
        cardView.theme = BTUI.braintreeTheme()
        cardView.optionalFields = .cvv
        
        self.title = "Add Credit Card"
        //self.cardView.addObserver(self, forKeyPath: "valid", options: [], context: nil)
        CardIOUtilities.preload()
        
      
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "By proceeding, I agree to Terms & Conditions")
        //attributeString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(26, 18))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: NSMakeRange(26, 18))
        attributeString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(26, 18))
        lblTermTxt.attributedText = attributeString
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        
        
        
        if isFromInternalScreen {
            btnSkip.isHidden = true
            sepView.isHidden = true
            defaultSwitch.isHidden = false
            lblDefaultCard.isHidden = false
            topConstraint.constant = 65
            
            lblTermTxt.isHidden = true
            btnOnLbl.isHidden = true
            btnTerms.isHidden = true
            
            widthConstraint.constant = 100
            
            //retain user object.
            if let user = getUser() {
                userObj.fname = user.firstName!
                userObj.lname = user.lastName!
                userObj.zipCode = user.zipCode!
                userObj.email = user.email!
            }
            
            
        }else{
            btnSkip.isHidden = false
            sepView.isHidden = false
            defaultSwitch.isHidden = true
            lblDefaultCard.isHidden = true
            topConstraint.constant = 30
            
            lblTermTxt.isHidden = false
            btnOnLbl.isHidden = false
            btnTerms.isHidden = false
        }
        
        
        setNavBar()
        apiGetWePayDetails()
        generateRiskToken()
    }
    
    
    
    func generateRiskToken(){
        let url = URL(string: "https://www.delytapp.com/RiskToken")
        let urlRequest = URLRequest(url: url!)

        // enable JS
        webViewRiskToken.configuration.preferences.javaScriptEnabled = true
        webViewRiskToken.load(urlRequest)
        webViewRiskToken.navigationDelegate = self
        
    }
    

    func setNavBar() {
        
        let scanButton = UIBarButtonItem(image: UIImage(named: "scan_camera"), style: .plain, target: self, action: #selector(scanCard))
        self.navigationItem.rightBarButtonItems = [scanButton]
           
    }
    
    func apiGetWePayDetails() {
        APIClient.GetWePayDetailsRequest { (response, error, message) in
            
            if response != nil {
                
                if let Id = response?.wepayData!.ClientId {
                    self.clientId = String(Id)
                }
                self.accountId = Int(response?.wepayData?.AccountID ?? "0")!
                self.fetchOtherDetails(clientId: self.clientId)
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
            
        }
    }
    
    func fetchOtherDetails(clientId:String) {
        
        let env = getEnvironment(environment: Environment)
        let config: WPConfig = WPConfig(clientId: clientId, environment: env)
        config.useLocation = true
        config.stopCardReaderAfterOperation = false
        config.restartTransactionAfterOtherErrors = false
        
        // Initialize WePay
        self.wePay = WePay(config: config)
    }
    
    func getEnvironment(environment: String) -> String {
      
        var Env = ""
        if (environment == "Stage") {
            Env = kWPEnvironmentStage
        }else if (environment == "Production") {
            Env = kWPEnvironmentProduction
        }
        //TODO for debugging
        //Env = kWPEnvironmentStage
        
        return Env
    }
    
    @objc func scanCard() {
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC?.modalPresentationStyle = .formSheet
        cardIOVC?.navigationBar.tintColor = .black
        present(cardIOVC!, animated: true, completion: nil)
    }
    
}

extension ScanCardVC: BTUICardFormViewDelegate {
    func cardFormViewDidChange(_ cardFormView: BTUICardFormView!) {
        if cardFormView.valid {
            print("\(String(describing: cardFormView.number))")
            print("\(String(describing: cardFormView.cvv))")
            
        }else{
            print("Invalid Card")
        }
    }
    
}

extension ScanCardVC: CardIOPaymentViewControllerDelegate {
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        //resultLabel.text = "user canceled"
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv )

            if info.cardholderName != nil{
                str.appending("\n Name: \(String(describing: info.cardholderName))")
            }
            
            print(str)
            
            self.cardView.cvv = info.cvv
            self.cardView.setExpirationMonth(Int(info.expiryMonth), year: Int(info.expiryYear))
            self.cardView.number = info.cardNumber

            //resultLabel.text = str as String
        }

        paymentViewController?.dismiss(animated: true, completion: nil)
    }
}

extension ScanCardVC: WPTokenizationDelegate {
    
    func paymentInfo(_ paymentInfo: WPPaymentInfo!, didTokenize paymentToken: WPPaymentToken!) {
        
        let str: NSMutableAttributedString = NSMutableAttributedString(string: "paymentInfo:didTokenize: \n")
        let info: NSAttributedString = NSAttributedString(
            string: paymentToken.description,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0, blue: 1, alpha: 1)]
        )
        str.append(info)
        //PKHUD.sharedHUD.hide()
        hud.dismiss(afterDelay: 1.0)
        
        print(str)
        print(paymentToken.tokenId!)
        self.cardId = paymentToken.tokenId!
        
        
        if isFromInternalScreen {
            apiAddCard()
        }else{
            self.apiSignUp(withCard: false)
        }
        
    }
    
    func paymentInfo(_ paymentInfo: WPPaymentInfo!, didFailTokenization error: Error!) {
        print(error.debugDescription)
        showAlert(title: "", message: error.localizedDescription, viewController: self)
        //PKHUD.sharedHUD.hide()
        hud.dismiss(afterDelay: 1.0)
    }
    
    func apiAddForSignupCard() {
        
        print("\(getUser()!.userId!)")
        print("\(self.cardId)")
        
        APIClient.AddCardRequest(customerId: "\(String(describing: getUser()!.userId!))", cardId: self.cardId, isDefaultCard: self.defaultSwitch.isOn ? "1" : "0") { (response, error, message) in
            
            if response != nil {
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                print(message!)
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                /*
                let optionMenu = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    //self.navigationController?.popViewController(animated: true)
                })
                optionMenu.addAction(okAction)
                self.present(optionMenu, animated: true, completion: nil)*/
                
            }
        }
    }
    
    func apiAddCardElavon(cardNumber: String, expiry: String, cvv: String){
        
        APIClient.AddCardElavonRequest(customerId: "\(String(describing: getUser()!.userId!))", firstName: "\(String(describing: getUser()!.firstName!))", lastName: "\(String(describing: getUser()!.lastName!))", Email: "\(String(describing: getUser()!.email!))", zipCode: "\(String(describing: getUser()!.zipCode!))", cardNumber: cardNumber, expiry: expiry, cvv: cvv, isDefaultCard: self.defaultSwitch.isOn ? "1" : "0") { (response, error, message) in
            
            if response != nil {
                if response == "True" {
                    showToast(viewControl: self, titleMsg: "", msgTitle: "Credit card added successfully.")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    //self.navigationController?.popViewController(animated: true)
                })
                optionMenu.addAction(okAction)
                self.present(optionMenu, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    func apiAddCard() {
        
        APIClient.AddCardRequest(customerId: "\(String(describing: getUser()!.userId!))", cardId: self.cardId, isDefaultCard: self.defaultSwitch.isOn ? "1" : "0") { (response, error, message) in
            
            if response != nil {
                if response == "True" {
                    showToast(viewControl: self, titleMsg: "", msgTitle: "Credit card added successfully.")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    //self.navigationController?.popViewController(animated: true)
                })
                optionMenu.addAction(okAction)
                self.present(optionMenu, animated: true, completion: nil)
                
            }
        }
    }
    
    func apiSignUp(withCard: Bool){
        
        //self.cardId = "79139614523"//Testing
        APIClient.SignUpRequest2(userObject: userObj, cardNo: self.cardNo, expiry: self.cardExpiry, cvv: self.cvv, userType: isFbUser ? kFacebookUser : kNativeUser, isPic: userObj.isImage, UserImage: userObj.profileImage, withCard: withCard) { (response, error, message) in
            
            if response != nil {
                
                if let user = response?.response {
                    
                    //TODO coment below line fix
                    setUser(userObj: user)
                    self.apiAddForSignupCard()
                    
                    if user.isUserPhoneNumberVerified == false {
                        self.gotoVerifyScreen(phone: user.phoneNumber!, userId: "\(user.userId!)", email: user.email!)
                    }
                }
                
                if response?.status == "False" {
                    //showAlert(title: "", message: (response?.message!)!, viewController: self)
                    /*
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })*/
                    
                    let optionMenu = UIAlertController(title: "", message: (response?.message!)!, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    optionMenu.addAction(okAction)
                    self.present(optionMenu, animated: true, completion: nil)
                    
                    
                }
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func gotoVerifyScreen(phone: String, userId: String, email:String) {
        let storyboard = UIStoryboard(name: kMainStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kVerifyPhoneControllerID) as! VerifyPhoneVC
        //vc.modalPresentationStyle = .fullScreen
        vc.phone = phone
        vc.userId = userId
        vc.email = email
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ScanCardVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
        print("loaded")
        
       webView.evaluateJavaScript("document.getElementsByClassName('no-js')[0].innerText") {(result, error) in
        
        guard error == nil else {
            print(error!)
            return
        }

        let riskToken = result as! String
        let finalToken = riskToken.replacingOccurrences(of: "-", with: "")
        
        print(finalToken)
        self.riskToken = finalToken
      }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        print(error.localizedDescription)
    }
}
