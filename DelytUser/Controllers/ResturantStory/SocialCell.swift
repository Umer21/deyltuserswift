//
//  SocialCell.swift
//  DelytUser
//
//  Created by UmarFarooq on 03/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SocialCell: UITableViewCell {

    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnTwitter: UIButton!
    @IBOutlet weak var btnInsta: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
