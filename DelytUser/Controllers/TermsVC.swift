//
//  TermsVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import WebKit

class TermsVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        /*
         NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",SERVER_URL, @"JDocs/TermsAndConditions/Terms-Conditions-User.pdf"]];
         NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
         [[_webview_terms scrollView] setContentOffset:CGPointMake(0,self.view.frame.size.height) animated:YES];
         [_webview_terms stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.scrollTo(0.0, 50.0)"]];
         [_webview_terms loadRequest:request];
         */
        self.title = "Terms and Conditions"
     
        let link = URL(string:ServerURL+"/JDocs/TermsAndConditions/Terms-Conditions-User.pdf")!
        let request = URLRequest(url: link)
        webView.load(request)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TermsVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("Start loading")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("End loading")
    }
}
