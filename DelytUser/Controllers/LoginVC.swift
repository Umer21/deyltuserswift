//
//  LoginVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit

class LoginVC: UIViewController {

    var fbEmail = ""
    var fbName = ""
    var fbImageUrl = ""
    var isFacebookUser = false
    
    //let loginManager = LoginManager()
    
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    
    @IBAction func signInTapped(_ sender: Any) {
        txtPass.resignFirstResponder()
        apiLogin(type: kNativeUser, emalTxt: txtEmail.text!, PassTxt: txtPass.text!)
    }
    
    @IBAction func loginFBTapped(_ sender: Any) {
        isFacebookUser = true
        fbLogin()
    }
    
    @IBAction func nativeLoginTapped(_ sender: Any) {
        
    }
    @IBAction func forgotPasswordTapped(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        btnSignIn.layer.cornerRadius = 3
        
        //TODO: fast debugging
        /*
        txtEmail.text = "shaheryar@inabia.com"
        txtPass.text = "12345678"
        */
        /*
        txtEmail.text = "muk_hull@yahoo.com"
        txtPass.text = "12345678"
        */
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func apiLogin(type: String, emalTxt: String, PassTxt: String) {
        
        if isValidate(type: type) {
            APIClient.LoginRequest(email: emalTxt, password: PassTxt, type: type) { (responce, error, message) in
                
                if responce != nil {
                    
                    if self.isFacebookUser {
                        responce?.userPicUrl = self.fbImageUrl
                    }
                    
                    if !responce!.isUserPhoneNumberVerified! {
                        
                        //if user phone number not verified
                        setUser(userObj: responce)
                        self.gotoVerifyScreen(phone: responce!.phoneNumber!, userId: "\(responce!.userId!)", email: responce!.email!)
                        
                    }else {
                        
                        setUser(userObj: responce)
                        self.gotoHomeScreen()
                        
                    }
                    
                    
                }
                
                if error != nil {
                    print(error.debugDescription)
                }
                
                if message != nil {
                    
                    if message == "User Not Registered" {
                        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                            self.gotoSignUpScreen()
                        })
                    }
                    else
                    {
                        //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                        
                        let optionMenu = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                            (alert: UIAlertAction!) -> Void in
                            
                            
                        })
                        
                        optionMenu.addAction(okAction)
                        self.present(optionMenu, animated: true, completion: nil)
                        
                        
                    }
                }
            }
        }
    }
    
    
    func gotoSignUpScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kSignupControllerID) as! SignupVC
        vc.isFacebookUser = true
        vc.userEmail = fbEmail
        vc.imgUrl = fbImageUrl
        
        let fullNameArr = fbName.components(separatedBy: " ")
        vc.userFirstName = fullNameArr[0]
        vc.userLastName = fullNameArr[1]
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoHomeScreen(){
        
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: kNavControllerID) as! UINavigationController
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        navigationController.setViewControllers([storyboard.instantiateViewController(withIdentifier: kHomeControllerID)], animated: false)
        mainViewController.rootViewController = navigationController
        mainViewController.modalPresentationStyle = .fullScreen
        mainViewController.setup(type: 0)
        //self.present(mainViewController, animated: true, completion: nil)
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = mainViewController
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func gotoVerifyScreen(phone: String, userId: String, email:String) {
        let storyboard = UIStoryboard(name: kMainStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kVerifyPhoneControllerID) as! VerifyPhoneVC
        //vc.modalPresentationStyle = .fullScreen
        vc.phone = phone
        vc.userId = userId
        vc.email = email
        vc.isRegistered = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func isValidate(type: String)-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        if type != kFacebookUser {
            if (self.txtPass.text?.isEmpty)! {
                message = "Please enter password."
                returnValue = false
            }
            
            if (self.txtEmail.text?.isEmpty)! {
                message = "Please enter email."
                returnValue = false
            }
        }
        
        
        
        
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    
    //MARK:- Facebook Login
    
    func fbLogin() {
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions:[ .publicProfile, .email], viewController: self) { loginResult in
            
            switch loginResult {
            
            case .failed(let error):
                //HUD.hide()
                print(error.localizedDescription)
                showToast(viewControl: self, titleMsg: "Error", msgTitle: error.localizedDescription)
            
            case .cancelled:
                //HUD.hide()
                print("User cancelled login process.")
                showToast(viewControl: self, titleMsg: "Error", msgTitle: "User cancelled login process.")
            
            case .success( _, _, _):
                print("Logged in!")
                
                self.getFBUserData()
            }
        }
    }
    
    
    
    func getFBUserData() {
        //which if my function to get facebook user details
        if((AccessToken.current) != nil){
            
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    let dict = result as! [String : AnyObject]
                    print(result!)
                    print(dict)
                    let picutreDic = dict as NSDictionary
                    let tmpURL1 = picutreDic.object(forKey: "picture") as! NSDictionary
                    let tmpURL2 = tmpURL1.object(forKey: "data") as! NSDictionary
                    self.fbImageUrl = tmpURL2.object(forKey: "url") as! String
                    
                    
                    
                    let nameOfUser = picutreDic.object(forKey: "name") as! String
                    self.fbName = nameOfUser
                    
                    var tmpEmailAdd = ""
                    if let emailAddress = picutreDic.object(forKey: "email") {
                        tmpEmailAdd = emailAddress as! String
                        self.fbEmail = tmpEmailAdd
                    }
                    else {
                        var usrName = nameOfUser
                        usrName = usrName.replacingOccurrences(of: " ", with: "")
                        tmpEmailAdd = usrName+"@facebook.com"
                    }
                    
                    //api call
                    self.apiLogin(type: kFacebookUser, emalTxt: tmpEmailAdd, PassTxt: "")
                   
                }
                
                print(error?.localizedDescription as Any)
            })
        }
    }
}

extension LoginVC: UINavigationBarDelegate {
    
}
