//
//  TipVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol TipGivenDelegate {
    func tipGivenSuccessfully()
}

class TipVC: UIViewController {
    var delegate: TipGivenDelegate?
    
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtOther: UITextField!
    
    @IBOutlet weak var businessImg: UIImageView!
    var isFromOrderScreen = false
    var isTipPercentage = false
    var tipAmount = "10"
    var isFromHomeScreen = false
    var isShowTipOneTime = false
    var nextOrderId = ""
    var orderObj: HistoryObj?
    var isNoTip = false
    
    @IBOutlet weak var segmentIB: UISegmentedControl!
    @IBAction func doneTapped(_ sender: Any) {
        apiUpdateTip()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)

        setNavbar()
        txtOther.isEnabled = false
        isTipPercentage = true
        
        if isFromHomeScreen {
            apiGetNextOrderRated()
        }else{
            populateData()
        }
        
    }
    
    func setNavbar() {
        let titleTextAttributesN = [NSAttributedString.Key.foregroundColor: UIColorFromRGB(rgbValue: 0x45112D)]
        let titleTextAttributesS = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentIB.setTitleTextAttributes(titleTextAttributesN, for: .normal)
        segmentIB.setTitleTextAttributes(titleTextAttributesS, for: .selected)
        
        self.title = "Order Completed"
        
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backTapped))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func backTapped(sender: UIBarButtonItem) {
        NotificationCenter.default.post(name: TipScreenBoolNotification.TipNotification, object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func apiGetNextOrderRated() {
        
        APIClient.NextOrderToRatedRequest(isPullToRefresh: isShowTipOneTime) { (response, error, message) in
            
            if response != nil {
                
                self.lblOrder.text = "Please select tip amount for Order # \(response!.nextOrder!.OrderId!)"
                //self.lblDate.text = response!.nextOrder!.OrderedOn!
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
                let convertedDate = dateFormatter.string(from: parseDateToDateFormat(response!.nextOrder!.OrderedOn!))
                self.lblDate.text = "\(formatDate(date: response!.nextOrder!.OrderedOn!)) \(formateTime(date: response!.nextOrder!.OrderedOn!))"
                
                if let url = response?.nextOrder?.BusinessImageUrl{
                    self.businessImg.af_setImage(withURL: URL(string: url)!, placeholderImage: UIImage(named: "icon_noimage")!)
                }else{
                    self.businessImg.image = UIImage(named: "icon_noimage")
                }//library
                
                self.nextOrderId = "\(response!.nextOrder!.OrderId!)"
                
                //retaining obj for review
                self.orderObj = HistoryObj()
                self.orderObj?.orderId = response!.nextOrder!.OrderId!
                
                if response?.nextOrder?.BusinessImageUrl == nil{
                    self.orderObj?.businessImageUrl = ""
                }else{
                    self.orderObj?.businessImageUrl = response!.nextOrder!.BusinessImageUrl!
                }
                
                self.orderObj?.businessName = response!.nextOrder!.BusinessName!
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func populateData() {
        if let object = orderObj {
            lblOrder.text = "Please select tip amount for Order # \(object.orderId!)"
            
            lblDate.text = getFormattedDateForTipScreen(strDate: object.orderedOn!, currentFomat: "yyyy-MM-dd'T'HH:mm:ss.SSS", expectedFromat: "MMM dd yyyy   hh:mm aaa")//convertedDate
            
            businessImg.af_setImage(withURL: URL(string: object.businessImageUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
            
        }
        
    }
    
    func apiUpdateTip() {
        
        var percentage = "0"
        if isTipPercentage {
            percentage = "1"
        }
        
        var orderId = ""
        if isFromHomeScreen {
            orderId = nextOrderId
            
        }else{
            orderId = "\(String(describing: orderObj!.orderId!))"
        }
        
        if txtOther.isEnabled == true {
            if txtOther.text == "" {
                showToast(viewControl: self, titleMsg: "", msgTitle: "Please enter tip.")
                return
            }else{
                tipAmount = txtOther.text!
            }
        }
        
        APIClient.PostTipRequest(orderId: orderId, tipAmount: tipAmount, tipPercentage: percentage) { (response, error, message) in
            
            if response != nil {
                
                if let user = getUser() {
                    user.rewardPoints = Int(response!.rewardPoints!)
                    setUser(userObj: user)
                }
                
                if !self.isNoTip{
                    showToast(viewControl: self, titleMsg: "", msgTitle: "Tip has been given")
                }
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                    if !self.isFromOrderScreen {
                        self.gotoReviewScreen()
                    }else{
                        self.delegate?.tipGivenSuccessfully()
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                })
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func gotoReviewScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReviewControllerID) as! ReviewVC
        vc.isFromHomeScreen = true
        vc.orderObj = orderObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func segmentTapped(_ sender: Any) {
        let control = sender as! UISegmentedControl
        switch (control.selectedSegmentIndex) {
        case 0:
            txtOther.isEnabled = false
            txtOther.text = ""
            isTipPercentage = true
            tipAmount = "10"
        break
        case 1:
            txtOther.isEnabled = false
            txtOther.text = ""
            isTipPercentage = true
            tipAmount = "15"
        break
        case 2:
            txtOther.isEnabled = false
            txtOther.text = ""
            isTipPercentage = true
            tipAmount = "20"
        break
        case 3:
            txtOther.isEnabled = true
            isTipPercentage = false
            tipAmount = "0"
            
        break
        case 4:
            txtOther.isEnabled = false
            txtOther.text = ""
            isTipPercentage = false
            tipAmount = "0"
            isNoTip = true
            
        default:
            print("default")
        }
    }
   
    

}
