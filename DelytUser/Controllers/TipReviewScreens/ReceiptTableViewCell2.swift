//
//  ReceiptTableViewCell2.swift
//  DelytUser
//
//  Created by Inabia1 on 10/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class ReceiptTableViewCell2: UITableViewCell {

    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var viewItem: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
