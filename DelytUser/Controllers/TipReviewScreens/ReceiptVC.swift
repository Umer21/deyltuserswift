//
//  ReceiptVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class ReceiptVC: UIViewController {
    
    //@IBOutlet weak var btnRefund: UIButton!
    @IBOutlet weak var lblBusinessAddress: UILabel!
    @IBOutlet weak var lblBusinessTitle: UILabel!
    @IBOutlet weak var imgBusiness: UIImageView!
    var orderID: String?
    var isRefunded = 0
    var orderReceiptModel: CustomerOrderReceipt?
    var isApiDataUpdated = false
    var managerReason = ""
    var myComents = ""
    var orderStatus = 0
    @IBOutlet weak var tableViewReceipt: UITableView!
    
    /*
    @IBAction func askRefundTapped(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kRefundControllerID) as! RefundVC
        vc.orderID = orderID!
        vc.isRefunded = self.isRefunded
        vc.managerReason = managerReason
        vc.myComents = myComents
        vc.orderStatus = orderStatus
        self.navigationController?.pushViewController(vc, animated: true)
        
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        

        // Do any additional setup after loading the view.
       // print(orderID)
        self.title = "Order # " + orderID!
        apiGetReceipt()
        
        let headerNib = UINib.init(nibName: "Header", bundle: Bundle.main)
        tableViewReceipt.register(headerNib, forHeaderFooterViewReuseIdentifier: "Header")
        tableViewReceipt.isHidden = true
        tableViewReceipt.separatorStyle = .none
        
        
        print(isRefunded)
        /*
        if isRefunded == 0 {
            btnRefund.isHidden = true
        }else if isRefunded == 1 {
            btnRefund.isHidden = false
        }else{
            btnRefund.isHidden = false
        }*/
        
    }
    
    func apiGetReceipt() {
        APIClient.GetOrderReceipt(id: orderID!) { (response, error, message) in
            if response != nil {
                
                self.orderReceiptModel = (response?.customerOrderReceipt)!
                self.isApiDataUpdated = true
                self.tableViewReceipt.isHidden = false
                self.tableViewReceipt.reloadData()
                
                self.populateData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: "No receipt available", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                optionMenu.addAction(okAction)
                
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
    }
    
    func populateData()
    {
        if orderReceiptModel?.businessLogoUrl != "" {
            /*profileImg.af_setImage(withURL: URL(string: user.userPicUrl!)!, placeholderImage: UIImage(named: "icon_account_camera")!)*/
            
            imgBusiness.af_setImage(withURL: URL(string: orderReceiptModel!.businessLogoUrl!)!)
            
            //imgBusiness.layer.cornerRadius = imgBusiness.frame.size.height/2
            imgBusiness.layer.masksToBounds = true
            
        }
        
        lblBusinessTitle.text = orderReceiptModel?.businessName
        lblBusinessAddress.text = orderReceiptModel?.businessAddress
    }
    

}

extension ReceiptVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if !isApiDataUpdated {
                return 2
            }else{
                return orderReceiptModel!.receiptOrderLine!.count
            }
            
        case 1:
            if orderReceiptModel?.IsRefunded == "1" {
                return 6
            }else{
                return 5
            }
        default :
            return 5
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        switch indexPath.section {
        case 0:
            let cell = tableViewReceipt.dequeueReusableCell(withIdentifier: "cell", for:  indexPath as IndexPath) as! ReceiptTableViewCell1
            
            let orderlineModel = orderReceiptModel?.receiptOrderLine![indexPath.row]
            cell.lblMenuName.text = orderlineModel?.menuName
            cell.lblMenuOption.text = orderlineModel?.menuOptions
            cell.lblQuantity.text = orderlineModel?.quantity
            
            if let uP = orderlineModel?.unitPrice
            {
                cell.lblUnitCost.text = "x " + uP
            }
            else{
                cell.lblUnitCost.text = ""
            }
            
            cell.lblAmount.text = orderlineModel?.amount
            //cell.viewMenu.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
            //cell.viewQuantity.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
            //cell.viewMenu.addBottomBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
            //cell.viewQuantity.addBottomBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
            //cell.viewAmount.addBottomBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
            
            return cell
            
            //break
        case 1:
        
            let cell = tableViewReceipt.dequeueReusableCell(withIdentifier: "cell1", for:  indexPath as IndexPath) as! ReceiptTableViewCell2
            
            switch indexPath.row {
            case 0:
                cell.lblItem.text = "Sub Total"
                cell.lblAmount.text = (" \(orderReceiptModel?.subTotal ?? "0.0")")

                
                return cell
                //break
                
            case 1:
                cell.lblItem.text = "Discount"
                
                if let rDisc = orderReceiptModel?.discount {
                    if Double (rDisc) ?? 0.0 > 0 {
                        cell.lblAmount.text = String ("( \(rDisc))")
                    }
                    else {
                        cell.lblAmount.text = String (" \(rDisc)")
                    }
                    
                }
                
                //cell.lblAmount.text = orderReceiptModel?.discount

                
                return cell
                //break
                
            case 2:
                cell.lblItem.text = "Tax"
                cell.lblAmount.text = orderReceiptModel?.taxAmount

                
                return cell
                //break
                
            case 3:
                
                //print(orderReceiptModel?.tipType)
                if((orderReceiptModel?.tipType ?? "").isEmpty) {
                    cell.lblItem.text = "Tip "
                }
                else {
                    if orderReceiptModel?.tipType != "(None)" {
                        cell.lblItem.text = "Tip " + (orderReceiptModel!.tipType!)
                    }
                    
                }
                
                
                cell.lblAmount.text = orderReceiptModel?.tip

                
                return cell
                //break
                
            case 4:
                
                if orderReceiptModel?.IsRefunded == "1" {
                    cell.lblItem.text = "Refund Amount"
                    cell.lblAmount.text = "\(String(describing: orderReceiptModel!.total!))"
                }else{
                    cell.lblItem.text = "Total"
                    cell.lblAmount.text = orderReceiptModel?.total
                }
                return cell
                
            case 5:
                cell.lblItem.text = "Total"
                cell.lblAmount.text = "$ 0"
                

                return cell
                //break
                
            default:
                break
            }
            
            break
            
        default:
            break
        }
        
        return cell1!
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 50
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         
        switch section {
            case 0:
                
                let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Header") as! Header
                return headerView
            default:
        
                return nil
        }
    }
}

extension UIView {
  func addTopBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
    self.layer.addSublayer(border)
  }

  func addRightBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
    self.layer.addSublayer(border)
  }

  func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
    self.layer.addSublayer(border)
  }

  func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
    self.layer.addSublayer(border)
  }
}
