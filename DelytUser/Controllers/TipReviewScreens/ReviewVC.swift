//
//  ReviewVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import HCSStarRatingView


class ReviewVC: UIViewController {
    
    @IBOutlet weak var lblResturant: UILabel!
    @IBOutlet weak var lblRestValue: UILabel!
    @IBOutlet weak var starsView: HCSStarRatingView!
    @IBOutlet weak var txtComent: UITextView!
    @IBOutlet weak var doneTapped: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    var orderObj: HistoryObj?
    var isFromHomeScreen = false
    var isFromOrderScreen = false
    
    @IBAction func doneTapped(_ sender: Any) {
        print(starsView.value)
        if starsView.value == 0.0 {
            showToast(viewControl: self, titleMsg: "", msgTitle: "Please rate your experience")
            return
        }
        
        if txtComent.text == "Write a comment" {
            txtComent.text = ""
        }
        apiPostReview()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        self.title = "Order Completed"
        txtComent.text = "Write a comment"
        txtComent.textColor = UIColor.lightGray
        txtComent.delegate = self
        
        self.navigationItem.hidesBackButton = true

        populateDate()
    }
    
    
    
    func populateDate() {
        if let object = orderObj {
            
            if object.businessImageUrl == ""{
                imgView.image = UIImage(named: "icon_noimage")
            }else{
                imgView.af_setImage(withURL: URL(string: object.businessImageUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
            }
            
            
            lblRestValue.text = "\(String(describing: object.businessName!))"
        }
    }
    
    func apiPostReview() {
        let starvalue = starsView.value
        APIClient.PostReviewRequest(orderId: "\(String(describing: orderObj!.orderId!))", OrderRating: Int(starvalue), reviewText: txtComent.text!) { (response, error, message) in
            
            if response != nil {
                //showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                
                DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                    if self.isFromHomeScreen {
                        self.popBack(3)
                    }else{
                        
                        var userInfo = [String:String]()
                        userInfo["UserName"] = "Dan"
                        userInfo["Something"] = "Could be any object including a custom Type."
                        
                        NotificationCenter.default.post(name: ReviewOneBoolNotification.ReviewGiveNotification, object: userInfo)
                        //self.delegate?.reviewGivenSuccessfully()
                        self.popBack(4)
                    }
                })
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReviewVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write a comment"
            textView.textColor = UIColor.lightGray
        }
    }
}
