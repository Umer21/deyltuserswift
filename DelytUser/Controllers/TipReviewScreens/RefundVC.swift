//
//  RefundVC.swift
//  DelytUser
//
//  Created by UmarFarooq on 06/08/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class RefundVC: UIViewController {
    var orderID = ""
    var isRefunded = 0
    var managerReason = ""
    var myComents = ""
    var orderStatus = 0

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblManagerTitle: UILabel!
    @IBOutlet weak var txtReason: UITextView!
    @IBOutlet weak var txtManagerReason: UITextView!
    @IBAction func submitTapped(_ sender: Any) {
        
        
        if isRefunded == 1 || isRefunded == 2 {
            showToast(viewControl: self, titleMsg: "", msgTitle: "Already request sent for refund")
            
        }else{
            
            if txtReason.text == "" {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: "Please type reason for refund.")
            }else{
                self.apiRefund()
            }
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtManagerReason.text = managerReason
        txtReason.text = myComents
        
        //hide manager controls in this scenario
        if orderStatus == 6 || isRefunded == 0 {
            //request sent
            txtManagerReason.isHidden = true
            lblManagerTitle.isHidden = true
        }else{
            txtManagerReason.isHidden = false
            lblManagerTitle.isHidden = false
        }
        
        
        if isRefunded == 1 || isRefunded == 2 {
            btnSubmit.isHidden = true
            txtReason.isEditable = false
            
        }else{
            btnSubmit.isHidden = false
            txtReason.isEditable = true
        }
        
        if managerReason == "" {
            txtManagerReason.isHidden = true
            lblManagerTitle.isHidden = true
        }
        
    }
    
    func apiRefund() {
        APIClient.RefundRequest(id: orderID, reason: txtReason.text!) { (response, error, message) in
            
            if response != nil {
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: message!, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    //self.navigationController?.popViewController(animated: true)
                    self.popToMenu()
                })
                
                optionMenu.addAction(okAction)
                
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
        
    }
    
    func popToMenu() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for controller in viewControllers {
                if controller.isKind(of: OrderVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
