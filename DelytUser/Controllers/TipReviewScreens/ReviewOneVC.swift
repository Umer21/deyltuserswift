//
//  ReviewOneVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit


class ReviewOneVC: UIViewController {

    var orderObj: HistoryObj?
    var isFromOrderScreen = false
    
    @IBOutlet weak var giveReviewIB: UIButton!
    @IBAction func giveReviewTapped(_ sender: Any) {
        gotoReviewScreen()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        giveReviewIB.layer.cornerRadius = 5
    }
    
    func gotoReviewScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReviewControllerID) as! ReviewVC
        vc.orderObj = orderObj
        vc.isFromOrderScreen = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


