//
//  ReceiptTableViewCell1.swift
//  DelytUser
//
//  Created by Inabia1 on 10/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class ReceiptTableViewCell1: UITableViewCell {

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblUnitCost: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblMenuOption: UILabel!
    @IBOutlet weak var lblMenuName: UILabel!
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var viewQuantity: UIView!
    @IBOutlet weak var viewMenu: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
