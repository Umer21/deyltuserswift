//
//  ChangePassVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class ChangePassVC: UIViewController {

    var email = ""
    
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    @IBAction func changePassTapped(_ sender: Any) {
        apiChangePassword()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func apiChangePassword() {
        if isValidate() {
            APIClient.ChangePasswordWithCodeRequest(email: email, code: txtCode.text!, newPassword: txtConfirmPass.text!) { (response, error, message) in
                
               // print(response?.status)
                if response != nil {
                    //setUser(userObj: responce)
                    if (response?.status == "True"){
                        showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                            self.popBack(3)
                        })
                    }
                    else{
                        showToast(viewControl: self, titleMsg: "", msgTitle: "Please enter valid code")
                    }
                    
                    //self.gotoChangeScreen()
                }
                
                if error != nil {
                    print(error.debugDescription)
                }
                
                if message != nil {
                    showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                }
                
                
            }
        }
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func isValidate()-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        
        if (self.txtConfirmPass.text?.isEmpty)! {
            message = "Please enter password."
            returnValue = false
        }
        
        if (self.txtConfirmPass.text != self.txtPass.text) {
            message = "Please enter match passwords."
            returnValue = false
        }
        
        if (self.txtPass.text?.isEmpty)! {
            message = "Please enter password."
            returnValue = false
        }
        
        if (self.txtCode.text?.isEmpty)! {
            message = "Please enter code."
            returnValue = false
        }
        
       
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
