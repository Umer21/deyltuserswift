//
//  SignupVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import SwiftPhoneNumberFormatter



class SignupVC: UIViewController {

    @IBOutlet weak var contenViewHeight: NSLayoutConstraint!
    @IBAction func btnPhoto(_ sender: Any) {
        if isFacebookUser {
            showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "Facebook user can't change the profile picture.")
        }else{
            takeImageOptions()
        }
    }
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    @IBOutlet weak var txtPhone: PhoneFormattedTextField!
    @IBOutlet weak var txtZipcode: UITextField!
    @IBOutlet weak var receiptImg: UIImageView!
    
    var isFacebookUser = false
    var userFirstName = ""
    var userLastName = ""
    var userEmail = ""
    var imgUrl = ""
    
    var isReciept = false
    var selectedImage : UIImage?
    var isImage = false
    
    @IBAction func receptTapped(_ sender: Any) {
        
        if !isReciept {
            receiptImg.image = UIImage(named: "icon_checkbox_tick")
        }else{
            receiptImg.image = UIImage(named: "icon_checkbox")
        }
        isReciept = !isReciept
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        gotoNextScreen()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Sign Up"
        
        
        if isFacebookUser {
            populateDate()
        }
        
        phoneNumberFormat()
        //addDummydata()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            contenViewHeight.constant = 1040
        }
        
        
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        
        self.imgView.layer.cornerRadius = self.imgView.frame.height / 2
        imgView.layer.borderWidth = 3
        imgView.layer.borderColor = UIColor.white.cgColor
        self.imgView.clipsToBounds = true
    }
    
    
    func populateDate() {
        
        txtFName.text = userFirstName
        txtLName.text = userLastName
        txtEmail.text = userEmail
        
        txtFName.isEnabled = false
        txtLName.isEnabled = false
        txtEmail.isEnabled = false
        txtPass.isEnabled = false
        txtConfirmPass.isEnabled = false
        
        
        self.imgView.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
    }
    
    func addDummydata() {
        txtFName.text = "Umar"
        txtLName.text = "Farooq"
        txtEmail.text = "umar81@mailinator.com"
        txtPass.text = "11111111"
        txtConfirmPass.text = "11111111"
        txtPhone.text = "+923322235956"
        txtZipcode.text = "75850"
        
    }
    
    func phoneNumberFormat() {
        
        //txtPhone.prefix = "+"
        
        //US Format
        txtPhone.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "+1 (###) ###-####")
        
        //PK Format
        //txtPhone.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "+## (###) ###-####")
        
    }
    
    func gotoNextScreen() {
       
        if isValidate() {
            let user = SharedManager.shared.requestForUser()
            user.fname = txtFName.text!
            user.lname = txtLName.text!
            user.email = txtEmail.text!
            user.password = txtPass.text!
            user.phone = txtPhone.text!
            user.zipCode = txtZipcode.text!
            user.allowEmail = isReciept
            user.profileImage = selectedImage
            if isFacebookUser {
                user.profileImage = self.imgView.image
                self.isImage = true
            }
            user.isImage = self.isImage
            
            gotoCardScreen()
        }
    }
    
    func gotoCardScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kScanCardControllerID) as! ScanCardVC
        vc.modalPresentationStyle = .fullScreen
        vc.isFbUser = isFacebookUser
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func isValidate()-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        /*
        if (!self.isReciept) {
            message = "Please enable email receipt checkbox."
            returnValue = false
        }*/
        
        if (self.txtZipcode.text!.count > 8) {
            message = "Please enter maximum 8 digits zipcode."
            returnValue = false
        }
        
        if (self.txtZipcode.text!.count < 5) {
            message = "Please enter minimum 5 digits zipcode."
            returnValue = false
        }
        
        if (self.txtZipcode.text == "") {
            message = "Please enter zip code."
            returnValue = false
        }
        
        if self.txtPhone.text == "+" {
            message = "Please enter phone no."
            returnValue = false
        }
        
        
        if (self.txtPhone.text?.isEmpty)! {
            message = "Please enter phone no."
            returnValue = false
        }
        
        if !isFacebookUser {
            
            if (self.txtConfirmPass.text?.isEmpty)! {
                message = "Please enter confirm password."
                returnValue = false
            }
            
            if (self.txtConfirmPass.text != self.txtPass.text){
                message = "Password and confirm password doesn't match."
                returnValue = false
            }
            
            if (self.txtConfirmPass.text!.count < 8) {
                message = "Confirm password cannot be less than 8 characters"
                returnValue = false
            }
            
            
            if (self.txtPass.text?.isEmpty)! {
                message = "Please enter password."
                returnValue = false
            }
            
            if (self.txtPass.text!.count < 8) {
                message = "Password cannot be less than 8 characters"
                returnValue = false
            }
            
        }
        
        
        
        if (self.txtEmail.text?.isEmpty)! {
            message = "Please enter email address"
            returnValue = false
        }
        
        if isValidateEmail(email: self.txtEmail.text!) == false {
            message = "Please enter correct email address"
            returnValue = false
        }
        
        
        if (self.txtLName.text?.isEmpty)! {
            message = "Please enter last name"
            returnValue = false
        }
        
        if (self.txtFName.text?.isEmpty)! {
            message = "Please enter first name."
            returnValue = false
        }
        
        
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
        }
        
        return returnValue
    }
    
    func takeImageOptions(){
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
//            let isCameraAvalible = UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
//            if(isCameraAvalible) {
//                self.takePhoto()
//            }
            
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            
            switch cameraAuthorizationStatus {
                case .notDetermined: self.requestCameraPermission()
                case .authorized: self.takePhoto()
                case .restricted, .denied: self.alertCameraAccessNeeded()
            }
            
        })
        
        let saveAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.PhotoLibray()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func takePhoto() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func PhotoLibray() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }
    

    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.takePhoto()
        })
    }
    
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
     
        let alert = UIAlertController(
             title: "Need Camera Access",
            message: "Camera access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
         )
     
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
    
        present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SignupVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let originalImage = info[.originalImage] as? UIImage {
            self.isImage = true
            selectedImage = originalImage
            self.imgView.image = selectedImage
        }
        self.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated:true, completion: nil)
    }
}
