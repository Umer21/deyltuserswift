//
//  CardCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var lblCardNo: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        //self.accessoryType = selected ? .checkmark : .none
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()

       //Your separatorLineHeight with scalefactor
       let separatorLineHeight: CGFloat = 5/UIScreen.main.scale

       let separator = UIView()

       separator.frame = CGRect(x: self.frame.origin.x,
                                y: self.frame.size.height - separatorLineHeight,
                            width: self.frame.size.width,
                           height: separatorLineHeight)

       separator.backgroundColor = UIColorFromRGB(rgbValue: 0xf9f9f9)

       self.addSubview(separator)
    }

}
