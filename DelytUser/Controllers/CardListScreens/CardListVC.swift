//
//  CardListVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class CardListVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    let reuseIdentifier = "CardCell"
    var items = [WePayCreditCardList]()
    var selectedCardIndex = 0
    
    @IBOutlet weak var lblNoCards: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        setNavBar()
        self.tblView.isHidden = true
        tblView.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         apiGetPaymentMethodsApi()
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavBar() {
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        self.navigationItem.rightBarButtonItems = [addButton]
        
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        self.lblNoCards.isHidden = true
           
    }
    
    @objc func addTapped() {
        gotoCardScreen()
    }
    
    func gotoCardScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kScanCardControllerID) as! ScanCardVC
        vc.isFromInternalScreen = true
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func apiGetPaymentMethodsApi() {
        APIClient.GetPaymentMethods { (response, error, message) in
            
            if response != nil {
                self.items = (response?.cardList!)!
                
                if self.items.count > 0 {
                    self.lblNoCards.isHidden = true
                    self.tblView.isHidden = false
                }else{
                    self.lblNoCards.isHidden = false
                    self.tblView.isHidden = true
                }
                
                self.tblView.reloadData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func apiMakeCardDefault(cardId: Int) {
        APIClient.SetCardToDefault(cardId: cardId) { (response, error, message) in
            
            if response != nil {
                
                showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                
                DispatchQueue.main.asyncAfter(deadline: .now() +  1.0, execute: {
                    self.apiGetPaymentMethodsApi()
                })
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
}

extension CardListVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CardCell
        
        let model = items[indexPath.row]
        
        if model.LastFour != nil{
            cell.lblCardNo.text = "\(model.LastFour!)"
        }else{
            cell.lblCardNo.text = "*****"
        }
        
        
        if (model.ImageUrl != "") {
            cell.imgCard.af_setImage(withURL: URL(string: model.ImageUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
        }
        
        if model.IsDefaultCard! {
            cell.accessoryType = .checkmark
            selectedCardIndex = indexPath.row
        }else{
            cell.accessoryType = .none
        }
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 70
        }else{
            return 50
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedCardIndex == indexPath.row {
            showAlert(title: "", message: "This is already your default card.", viewController: self)
            return
        }
       
        
        
        let model = items[indexPath.row]
        
        let optionMenu = UIAlertController(title: "", message: "Are you sure you want to make it default card?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let okAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.apiMakeCardDefault(cardId: model.CardId!)
        })
        
        
        optionMenu.addAction(cancelAction)
        optionMenu.addAction(okAction)
        //self.present(optionMenu, animated: true, completion: nil)
        DispatchQueue.main.async {
            self.present(optionMenu, animated: false, completion:nil)
        }
    }
    
}
