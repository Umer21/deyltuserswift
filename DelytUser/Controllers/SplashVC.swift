//
//  ViewController.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {


    @IBOutlet weak var lblVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
            
            if let user = getUser() {
                if user.isUserPhoneNumberVerified == true {
                    self.gotoHomeScreen()
                }else{
                    self.gotoWelcomeScreen()
                }
                
            }else{
                self.gotoWelcomeScreen()
            }
        })
        
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        
        var environment = ""
        if baseUrl.contains("staging") {
            environment = "Staging"
        }else{
            environment = "Live"
        }
        
        lblVersion.text = "Version-\(appVersionString) - \(environment)"
        
    }
    
    
    func gotoHomeScreen(){
       
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: kNavControllerID) as! UINavigationController
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        let vc = storyboard.instantiateViewController(withIdentifier: kHomeControllerID) as! HomeVC
        vc.isGuestUser = false
        
        navigationController.setViewControllers([vc], animated: false)
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 0)
        mainViewController.modalPresentationStyle = .fullScreen
        
        //self.present(mainViewController, animated: true, completion: nil)
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = mainViewController
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func gotoWelcomeScreen() {
        //let vc = self.storyboard?.instantiateViewController(withIdentifier: "welcomeViewController") as! WellcomeVC
        //let navCon = UINavigationController(rootViewController: vc)
        //self.navigationController?.pushViewController(vc, animated: true)
        self.performSegue(withIdentifier: "gotoWelecomeSceen", sender: self)
        
    }
    
    


}

