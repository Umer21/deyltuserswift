//
//  FAQVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/17/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class FAQVC: UIViewController {

    var items =  [UserFaqSetting]()
    var question: String?
    var answer: String?
    
    @IBOutlet weak var tableViewFAQ: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        apiGetUsrInfo()
        
        tableViewFAQ.separatorStyle = .none
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    

    func setNavBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        
    }
    

   func apiGetUsrInfo() {
        APIClient.GetUserSettingsInformation()
        { (response, error, message) in
            if response != nil {
                
                self.items = (response?.userFaqSetting)!
                self.tableViewFAQ.reloadData()
               
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
}

extension FAQVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! FaqCell
        
        let model = self.items[indexPath.section]
        cell.lblFaq.text = model.userQuestion
        //cell.lblFaq.addBottomBorderWithColor(color: UIColorFromRGB(rgbValue: 0x000000), width: 1)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.items[indexPath.section]
        question = model.userQuestion
        answer = model.userAnswer
        gotoFAqDetailScreen()
        
    }
    
    func gotoFAqDetailScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kFAQDetailControllerID) as! FAQDetailVC
        vc.faqQuestion = question
        vc.faqAnswer = answer
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
