//
//  HomeVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import CoreLocation
import AlamofireImage
import ESPullToRefresh
import GoogleMaps
import Alamofire


class HomeVC: UIViewController {

    var isGuestUser = false
    var isShowTipOneTime = false
    var isFilterRecords = false
    var isFromTipScreen = false
    var isForFirstTime = false
    var isGoneForTip = false
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    let reuseIdentifierTable = "ResturantCell"
    var indicator = UIActivityIndicatorView()
    var btnLocation = UIButton()
    
    var items = [ResturantsObj]()
    var filterRecords = [ResturantsObj]()
    
    let locationManager = CLLocationManager()
    var Lat = "12.2122"
    var Long = "65.3434"
    var selectedResturantId = ""
    let geocoder = GMSGeocoder()
    
    @IBOutlet weak var lblNodata: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setNavBar()
        getCurrentLocation()
        
        //apiResturants(Lat: Lat, Long: Long, isPullToRef: false)
        self.lblNodata.isHidden = false
        //self.tblView.isHidden = true
        self.tblView.separatorStyle = .none
        
        self.tblView.es.addPullToRefresh {
            [unowned self] in
            /// Do anything you want...
            self.isShowTipOneTime = false
            self.apiResturants(Lat: self.Lat, Long: self.Long, isPullToRef: true)
        }
        
        //testing
        //requestABC()
    }
    
    func requestABC() {

        let url = URL(string: "https://stage.wepayapi.com/v2/credit_card/create")!

        do {
            var JSONText = ""
            
            let address = ["country" : "US",
            "postal_code": "94025"] as [String : Any]
            
            let dicts = [
                "client_id" : 68297,
                "user_name" : "Bob Smith",
                "email" : "test@example.com",
                "cc_number" : "549619858458476922",
                "cvv" : "123",
                "expiration_month" : 04,
                "expiration_year" : 2022,
                "original_ip" : "24.64.162.191",
                "original_device" : "apple",
                "reference_id" : "23_timestamp",
                "card_on_file" : true,
                "recurring" : false,
                "address" : address
                ] as [String : Any]
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dicts, options: .prettyPrinted)
                // here "jsonData" is the dictionary encoded in JSON data
                JSONText = String(data: jsonData, encoding: .utf8)!
                
                var request = URLRequest(url: url)
                request.httpMethod = HTTPMethod.post.rawValue
                request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                request.setValue("a4a167f44c5fa45b788da95a7ca7bc6ac", forHTTPHeaderField: "WePay-Risk-Token")
                request.setValue("24.64.162.191", forHTTPHeaderField: "Client-IP")
                request.httpBody = jsonData

                Alamofire.request(request).responseJSON {
                    (response) in
                    print(response)
                    print(response.description)
                    
                    //to get status code
                    if let status = response.response?.statusCode {
                        switch(status){
                        case 200:
                            print("success")
                            //to get JSON return value
                            if let result = response.result.value {
                                let JSON = result as! NSDictionary
                                print(JSON)
                                let cardId = JSON.object(forKey: "credit_card_id")!
                                print("\(cardId)")
                            }
                        default:
                            print("error with response status: \(status)")
                        }
                    }
                    
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
            
        } catch {
            print("Failed to serialise and send JSON")
        }
    }
    
    
    func getaddress(latitude: Double, longitude: Double) {
        
        let coordinate = CLLocationCoordinate2DMake(latitude,longitude)
            
         geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
             if let address = response?.firstResult() {
                 let lines = address.lines! as [String]
                self.btnLocation .setTitle("\(lines[0])", for: .normal)
                self.btnLocation.titleLabel?.lineBreakMode = .byTruncatingTail
                
                 //currentAdd(returnAddress: currentAddress)
             }
            
            //Handling for Tip screen from home screen
            if !self.isFromTipScreen {
                self.apiResturants(Lat: String (latitude), Long: String (longitude), isPullToRef: false)
            }
            //TODO: bug fix
            /*
            if !self.isForFirstTime {
                self.isForFirstTime = true
                self.apiResturants(Lat: self.Lat, Long: self.Long, isPullToRef: false)
            }*/
            self.locationManager.stopUpdatingLocation()
         }
     }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        print("viewWillTransition----------")
        
        if UIDevice.current.orientation.isLandscape {
            print("landscape")
        } else {
            print("portrait")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //AppUtility.lockOrientation(.portrait)
        //aa,bb,cc,dd
        
        registerNotifications()
        
        
        self.navigationController?.navigationBar.isHidden = false
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let latitude = Double(getValueForKey(keyValue: "Latitude"))
        let longitude =  Double(getValueForKey(keyValue: "Longitude"))
               
        //locationManager. ()
        getaddress(latitude: latitude ?? Double(Lat)!, longitude: longitude ?? Double(Long) as! Double)
        
        if let lt = latitude {
            Lat = String(lt)
        }
        
        if let lng = longitude{
            Long = String(lng)
        }
        
        //Move into getaddress function
        //Handling for Tip screen from home screen
        /*if !isFromTipScreen {
            apiResturants(Lat: String (latitude!), Long: String (longitude!), isPullToRef: false)
        }*/
        
        //If user denied location services
        let status = CLLocationManager.authorizationStatus()
        if status == .denied {
            if latitude == nil {
                gotoSettings()
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterNotifications()
        isFromTipScreen = false
    }
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadHomeService), name: ReloadHomeServiceNotification.ReloadNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setBoolForService), name: TipScreenBoolNotification.TipNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: ReloadHomeServiceNotification.ReloadNotification, object: nil)
        
    }
    
    @objc func setBoolForService() {
        isFromTipScreen = true
    }
    
    @objc func reloadHomeService() {
        isShowTipOneTime = true
        apiResturants(Lat: Lat, Long: Long, isPullToRef: false)
    }
    
    func apiResturants(Lat: String, Long: String, isPullToRef: Bool) {
        
        var userid = ""
        if isGuestUser {
            userid = "0"
        }else{
            if let usr = getUser() {
                userid = "\(String(describing: getUser()!.userId!))"
            }
            
        }
        
        var tags = ""
        for item in SharedManager.shared.filtersIds {
            tags.append("\(item),")
        }
        
        //Redmond Lat Longs
        // Lat 47.673988074527756, Long -122.12151188403368, user id = 10416
        
        
        APIClient.GetResturantsByLocation(id: userid, tags: tags, Lat: Lat, Long: Long, isPullToRef: isPullToRef) { (response, error, message) in
            self.tblView.es.stopPullToRefresh()
            self.indicator.stopAnimating()
            
            if response != nil {
                
                if response?.result?.count ?? 0 > 0 {
                    self.items = (response?.result)!
                    
                    if let userObj = getUser() {
                        print("RewardPoints: \(response!.RewardPoints)")
                        userObj.rewardPoints = Int((response!.RewardPoints)!)
                        setUser(userObj: userObj)
                    }
                    
                }else{
                    self.items.removeAll()
                }
                
                if let range = response?.AllowedBusinessRange {
                  setDefaultValue(keyValue: "Range", valueIs: range)
                }
                
                if self.items.count > 0 {
                    self.lblNodata.isHidden = true
                    //self.tblView.isHidden = false
                }else{
                    self.lblNodata.isHidden = false
                    //self.tblView.isHidden = true
                }
                
                
                self.tblView.reloadData()
                
                /*
                if response?.isPendingReviews == true {
                    self.gotoTipScreen()
                }*/
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            //if there is pending review remaining
            if getValueForKey(keyValue: "isPendingReview") == "true" {
                self.gotoTipScreen()
            }
            
            if message != nil {
                if message == "No Restaurant Found" {
                    
                    if self.isGuestUser {
                       self.lblNodata.text = "No restaurants within your search limits. Try changing location."
                    }else{
                        self.lblNodata.text = "No restaurants within your search limits. Try adjusting your search radius."
                    }
                    
                    self.lblNodata.isHidden = false
                    
                    self.items.removeAll()
                    self.tblView.reloadData()
                    self.tblView.isHidden = false
                }
            }
        }
    }
    
    func gotoTipScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kTipControllerID) as! TipVC
        vc.isFromHomeScreen = true
        vc.isShowTipOneTime = isShowTipOneTime
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCurrentLocation() {
        // Ask for Authorisation from the User.
        //self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func setNavBar() {
        
        
        let menuButton = UIBarButtonItem(image: UIImage(named: "icon_dashboard_menu"), style: .plain, target: self, action: #selector(menuButtonTapped))
        self.navigationItem.leftBarButtonItems = [menuButton]
        
       
        let filterButton = UIBarButtonItem(image: UIImage(named: "icon_menu_filter"), style: .plain, target: self, action: #selector(filterButtonTapped))
        
        let orderButton = UIBarButtonItem(image: UIImage(named: "icon_dashboard_cart"), style: .plain, target: self, action: #selector(orderButtonTapped))
        
        self.navigationItem.rightBarButtonItems = [filterButton, orderButton]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.backgroundColor = kThemeDarkRedColor
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        
        addCenterView()
    }
    
    func addCenterView() {
        /*
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        indicator.color = .white
        indicator .startAnimating()
        indicator.backgroundColor = .yellow
        */
        
        btnLocation = UIButton(type: .system)
        btnLocation.addTarget(self, action: #selector(locationTapped), for: .touchUpInside)
        btnLocation .setTitle("Loading", for: .normal)
        btnLocation .setTitleColor(.white, for: .normal)
        //btnLocation.titleLabel?.clipsToBounds = true
        
        var width = 160
        if UIDevice.current.userInterfaceIdiom == .pad {
            width = 400
        }
        //x = indicator.frame.origin.x + indicator.frame.size.width + 8
        //y = indicator.frame.origin.y
        
        let cgsize = btnLocation .sizeThatFits(CGSize(width: width, height: 40))
        btnLocation.frame = CGRect(x: 0, y: 0, width: CGFloat(width), height: cgsize.height)
        //btnLocation.backgroundColor = .orange
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 40))
        titleView .addSubview(btnLocation)
        //titleView.backgroundColor = .green
        
        self.navigationItem.titleView = titleView
    }
    
    
    @objc func filterButtonTapped() {
        gotoFiltersScreen()
    }
    
    @objc func orderButtonTapped() {
        
        if let _ = getUser() {
            gotoOrderHistoryScreen()
        }else{
            //guest user case
            let optionMenu = UIAlertController(title: "", message: "This feature requires an account.", preferredStyle: .alert)
            
            let LoginAction = UIAlertAction(title: "Login", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.gotoLoginScreen()
            })
            
            let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in })
            
            optionMenu.addAction(LoginAction)
            optionMenu.addAction(CancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    func gotoLoginScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
        let navCon = UINavigationController(rootViewController: welcomeVC)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = navCon
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    @objc func menuButtonTapped() {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @objc func locationTapped() {
        gotoLocationScreen()
    }
    
    func gotoOrderHistoryScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kOrderHistoryControllerID) as! OrderVC
        vc.isRecentOrders = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoLocationScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kLocationControllerID) as! LocationVC
        //let navCon = UINavigationController(rootViewController: vc)
        self.navigationController?.pushViewController(vc, animated: true)
        //self.performSegue(withIdentifier: "gotoWelecomeSceen", sender: self)
    }
    
    func gotoFiltersScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kFiltersControllerID) as! filtersVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoResturantScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kResturantControllerID) as! ResturantVC
        vc.resturantId = selectedResturantId
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*func latLong(lat: Double,long: Double)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            print("Response GeoLocation : \(placemarks)")
            var placemark: CLPlacemark!
            placemark = placemarks?[0]
            
        })
    }*/

}

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFilterRecords {
            return self.filterRecords.count
        }else{
            return self.items.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! ResturantCell
        
        var model = ResturantsObj()
        print(isFilterRecords)
        if isFilterRecords {
            print(self.filterRecords.count)
            
            model = self.filterRecords[indexPath.row]
        }else{
            model = self.items[indexPath.row]
        }
        
        cell.resName.text = model.BusinessName!
        cell.resAddress.text = model.BusinessAddress!
        cell.resReviews.text = "\(Int(model.Reviews!)) Reviews"
        cell.resDistance.text = "\(String(describing: model.DistanceMiles!.rounded(toPlaces: 2))) mi"
        cell.starView.value = CGFloat(model.BusinessRating!)
        
        if let imgUrl = model.BusinessLogoUrl {
            if imgUrl == "" {
                cell.imgResturant.image = UIImage(named: "icon_filter_noimage")
            }else{
                cell.imgResturant.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var model = ResturantsObj()
        
        if isFilterRecords {
            model = self.filterRecords[indexPath.row]
        }else{
            model = self.items[indexPath.row]
        }
        
        selectedResturantId = "\(String(describing: model.BusinessId!))"
        gotoResturantScreen()
    }
    
}



extension HomeVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
       // print("place name = \(latLong(lat: Double (locValue.latitude), long: Double(locValue.longitude)))")
        
        indicator.stopAnimating()
        //apiResturants(Lat: "\(locValue.latitude)", Long: "\(locValue.longitude)")
        Lat = "\(locValue.latitude)"
        print("C-Latitude------------\(Lat)")
        Long = "\(locValue.longitude)"
        print("C-Longitude------------\(Long)")
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (locValue.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (locValue.longitude))
        
        getaddress(latitude: locValue.latitude, longitude: locValue.longitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.notDetermined) {
            locationManager.requestWhenInUseAuthorization()

        }else{
            locationManager.startUpdatingLocation()
        }
    }
    
    func gotoSettings() {
        let alertController = UIAlertController (title: "Location service are off. Please allow to fetch resturants.", message: "Go to Settings?", preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        
        
        
        alertController.addAction(settingsAction)
        /*
        let cancelAction = UIAlertAction(title: "Set Mannually", style: .default, handler: nil)
        alertController.addAction(cancelAction)*/
        
        let cancelAction = UIAlertAction(title: "Set Manually", style: .default) { (_) in
            self.gotoLocationScreen()
        }
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
}

extension HomeVC: UISearchBarDelegate {
    internal func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //isFilterRecords = true;
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isFilterRecords = false;
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isFilterRecords = false;
        searchBar.resignFirstResponder()
    }


    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isFilterRecords = false;
        searchBar.resignFirstResponder()
    }

    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if((searchText.count) > 0) {
            self.filterRecords(filterText: searchText)
        }
        else {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                searchBar.resignFirstResponder()
            }
            
            self.filterRecords.removeAll()
            self.isFilterRecords = false
            self.tblView.reloadData()
           
        }
    }
    
    func filterRecords(filterText: String) {
        filterRecords = items.filter({(folderObject ) -> Bool in
            if(folderObject.BusinessName != nil) {
                let val = folderObject.BusinessName?.lowercased().contains(filterText.lowercased())
                return val!
            }
            return false
        })
       
        
        isFilterRecords = true
        self.tblView.reloadData()
        
        
        if filterRecords.count == 0 {
            showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "No resturant found.")
            //imgDataNotFound.isHidden = false
        }else{
            //imgDataNotFound.isHidden = true
        }
    }
}

extension HomeVC: filterApplyDelegate {
    func applyFilters() {
        apiResturants(Lat: Lat, Long: Long, isPullToRef: true)
    }
    
}
