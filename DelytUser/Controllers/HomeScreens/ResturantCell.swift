//
//  ResturantCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/14/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ResturantCell: UITableViewCell {

    @IBOutlet weak var imgResturant: UIImageView!
    @IBOutlet weak var resName: UILabel!
    @IBOutlet weak var resAddress: UILabel!
    @IBOutlet weak var resReviews: UILabel!
    @IBOutlet weak var resDistance: UILabel!
    @IBOutlet weak var starView: HCSStarRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()

       //Your separatorLineHeight with scalefactor
       let separatorLineHeight: CGFloat = 5/UIScreen.main.scale

       let separator = UIView()

       separator.frame = CGRect(x: self.frame.origin.x,
                                y: self.frame.size.height - separatorLineHeight,
                            width: self.frame.size.width,
                           height: separatorLineHeight)

       separator.backgroundColor = UIColorFromRGB(rgbValue: 0xf9f9f9)

       self.addSubview(separator)
    }

}
