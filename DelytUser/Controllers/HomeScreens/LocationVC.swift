//
//  LocationVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/14/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SystemConfiguration
import CoreLocation
import MapKit

class LocationVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    var arrayOfPlaces = [GoogleMapObjects]()
    var isFromDelivery = false
    
    let locationManager = CLLocationManager()
    var currentLatitude: Double?
    var tempLatitude: Double?
    var currentLongitude: Double?
    var tempLongitude: Double?
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBAction func searchTapped(_ sender: Any) {
        gotoPlacesContoller()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Location"
        // GOOGLE MAPS SDK: COMPASS
        mapView.settings.compassButton = true

       // GOOGLE MAPS SDK: USER'S LOCATION
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        
        mapView.delegate = self
        
        currentLatitude = Double (getValueForKey(keyValue: "Latitude"))
        currentLongitude = Double (getValueForKey(keyValue: "Longitude"))
        
        let cord2D = CLLocationCoordinate2D(latitude: (currentLatitude ?? 0.000), longitude: (currentLongitude ?? 0.000))
        
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
        
        setNavBar()
        
    }
    
    func setNavBar() {
       
        let saveButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonTapped))
        self.navigationItem.rightBarButtonItems = [saveButton]
        
    }
     
     @objc func doneButtonTapped() {
         
        if let latitude = self.tempLatitude {
            if isFromDelivery {
                setDefaultValue(keyValue: "deliveryLatitude", valueIs: String(latitude))
            }else{
                setDefaultValue(keyValue: "Latitude", valueIs: String(latitude))
            }
        }
        
        if let longitude = self.tempLongitude {
            
            if isFromDelivery {
                setDefaultValue(keyValue: "deliveryLongitude", valueIs: String(longitude))
            }else{
                setDefaultValue(keyValue: "Longitude", valueIs: String(longitude))
            }
            
        }
        
        self.navigationController?.popViewController(animated: true)
         
     }
    
    func gotoPlacesContoller() {
        txtSearch.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
       
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
      let geocoder = GMSGeocoder()
        
      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        guard let address = response?.firstResult(),
            let lines = address.lines else
        {
          return
        }
          
        self.tempLatitude = coordinate.latitude
        self.tempLongitude = coordinate.longitude
        
        self.txtSearch.text = lines.joined(separator: "\n")
          
        setDefaultValue(keyValue: "dAddressName", valueIs: self.txtSearch.text!)
          
        UIView.animate(withDuration: 0.25) {
          self.view.layoutIfNeeded()
            
            self.locationManager.stopUpdatingLocation()
        }
      }
    }

}

// MARK: - CLLocationManagerDelegate

extension LocationVC: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    
    guard status == .authorizedWhenInUse else {
      return
    }

    print("AAA-didChangeAuthorization")
    
    locationManager.startUpdatingLocation()
      
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
    
    print("AAA-didUpdateLocations")
      
    mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    locationManager.stopUpdatingLocation()
  }
}

// MARK: - GMSMapViewDelegate
extension LocationVC: GMSMapViewDelegate {
  
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("AAA-idleAt")
        print("AAA-idleAt\(position.target)")
      reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
      //addressLabel.lock()
    }
}

extension LocationVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        dismiss(animated: true, completion: nil)
        
        print("AAA-didAutocompleteWith")
        
        self.mapView.clear()
        self.txtSearch.text = place.name
        
        /*
        let placeGmap = GoogleMapObjects()
        placeGmap.lat = place.coordinate.latitude
        placeGmap.long = place.coordinate.longitude
        placeGmap.address = place.name*/
        
        //self.delegate?.getThePlaceAddress(vc: self, place: placeGmap, tag: self.FieldTag)
    
        let cord2D = CLLocationCoordinate2D(latitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude))
        
        /*let marker = GMSMarker()
        marker.position =  cord2D
        marker.title = "Location"
        marker.snippet = place.name
        
        let markerImage = UIImage(named: "icon_offer_pickup_1")!
        let markerView = UIImageView(image: markerImage)
        marker.iconView = markerView
        marker.map = self.mapView*/
        
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
        
        //self.btnOk.isEnabled = true
        //self.btnOk.backgroundColor = UIColor.init(netHex: 0x2CB4C6)
        UINavigationBar.appearance().barTintColor = kMerunColor
        UINavigationBar.appearance().tintColor = .white
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        UINavigationBar.appearance().barTintColor = kMerunColor
        UINavigationBar.appearance().tintColor = .white
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class GoogleMapObjects: NSObject {
    var title: String?
    var address: String?
    var lat: Double?
    var long: Double?
    
    override init() {
        self.title = ""
        self.address = ""
        self.lat = 0
        self.long = 0

    }
    
    init( tmpTitle: String, address: String/*, lat: Double, long: Double*/) {
        self.title = tmpTitle
        self.address = address
    }

}
