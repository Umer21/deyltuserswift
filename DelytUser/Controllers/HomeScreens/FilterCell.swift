//
//  FilterCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/16/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit


class FilterCell: UITableViewCell {
    //var delegate: updateRecordDelegate?

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        self.accessoryType = selected ? .checkmark : .none
        
        //self.delegate?.updateRecords()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


}
