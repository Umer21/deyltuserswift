//
//  filtersVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/16/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol filterApplyDelegate {
    func applyFilters()
}

class filtersVC: UIViewController {
    var delegate: filterApplyDelegate?

    @IBOutlet weak var tblView: UITableView!
    var items = [FiltersObj]()
    let reuseIdentifierTable = "FilterCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        apiGetFilters()
        self.tblView.allowsMultipleSelection = true
        self.tblView.allowsMultipleSelectionDuringEditing = true
        
        let filters = SharedManager.shared.requestForFilters()
        print("selected cell are: \(filters)")
        setNavBar()
    }
    
    func setNavBar() {
        self.title = "Cuisines"
           
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneTapped))
           self.navigationItem.rightBarButtonItems = [doneButton]
         
    }
    
    @objc func doneTapped() {
        SharedManager.shared.filtersIds.removeAll()
        
        if let _ = self.tblView.indexPathForSelectedRow?.count {
            for indexPaths in self.tblView.indexPathsForSelectedRows! {
                let model = self.items[indexPaths.row]
                SharedManager.shared.filtersIds.append(model.tagId!)
            }
        }
        
        print("\(SharedManager.shared.filtersIds)")
        self.delegate?.applyFilters()
        self.navigationController?.popViewController(animated: true)
    }
    
    func apiGetFilters() {
        APIClient.GetFilters { (response, error, message) in
            
            if response != nil {
                self.items = (response?.result)!
                self.tblView.reloadData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
            
        }
    }
    
    
    func updateRecords() {
        SharedManager.shared.filtersIds.removeAll()
        
        if let _ = self.tblView.indexPathForSelectedRow?.count {
            for indexPaths in self.tblView.indexPathsForSelectedRows! {
                let model = self.items[indexPaths.row]
                SharedManager.shared.filtersIds.append(model.tagId!)
            }
        }
        
        print("\(SharedManager.shared.filtersIds)")
    }

}

extension filtersVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! FilterCell
        
        let model = self.items[indexPath.row]
        cell.lblName.text = model.TagName!
        
        let filters = SharedManager.shared.requestForFilters()
        
        if filters.count > 0 {
            if filters.contains(model.tagId!) {
                cell .setSelected(true, animated: true)
                let selectedIndexPath = NSIndexPath(row: indexPath.row, section: 0)
                self.tblView.selectRow(at: selectedIndexPath as IndexPath, animated: true, scrollPosition: UITableView.ScrollPosition.none)
                cell.lblName.textColor = hexStringToUIColor(hex: "#983063")
                cell.imgView.image = nil
                if let imgUrl = model.TagHoverIconUrl {
                    cell.imgView.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
                }
            }else{
                cell.lblName.textColor = hexStringToUIColor(hex: "#767475")
                cell.imgView.image = nil
                if let imgUrl = model.TagIconUrl {
                    cell.imgView.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
                }
                
            }
        }else{
            
            cell.lblName.textColor = hexStringToUIColor(hex: "#767475")
            
            if let imgUrl = model.TagIconUrl {
                cell.imgView.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
            }
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        updateRecords()
        self.tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        updateRecords()
        self.tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 110
        }else{
            return 70
        }
    }
}



