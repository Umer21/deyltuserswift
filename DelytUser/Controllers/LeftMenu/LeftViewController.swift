//
//  LeftViewController.swift
//  LGSideMenuControllerDemo
//
import LGSideMenuController
import PKHUD

class LeftViewController: UITableViewController {
    var selectedImage : UIImage?
    var isGuestUser = false
    
    @IBAction func myFolderTapped(_ sender: Any) {
        print("my folder tapped")
    }
    
    private let titlesArray = ["",
                               "",
                               "Profile",
                               "Order History",
                               "Payment",
                               "Contact Us",
                               "Terms",
                               "FAQs",
                               "Change Password",
                               "Notifications",
                               "Settings",
                               "Sign Out"]
    //Guest User
    private let titlesArrayGuest = ["", "Contact Us", "Terms", "FAQs", "Sign In"]
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 44.0, right: 0.0)
        let tempImageView = UIImageView(image: UIImage(named: "bg_sidebar"))
        tempImageView.frame = self.tableView.frame
        self.tableView.backgroundView = tempImageView
        //tableView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: -8)
        
        //Guest User Handling
        if getUser() != nil {
            isGuestUser = false
        }else{
            isGuestUser = true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    
    override var prefersStatusBarHidden: Bool {
        
        tableView.reloadData()
        return true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isGuestUser {
            return titlesArrayGuest.count
        }else{
            return titlesArray.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LeftViewCell
        let cellReward = tableView.dequeueReusableCell(withIdentifier: "RewardCell") as! RewardCell
        
    
        if isGuestUser {
            cell.titleLabel.text = titlesArrayGuest[indexPath.row]
        }else{
            cell.titleLabel.text = titlesArray[indexPath.row]
        }
        
        if indexPath.row == 0  {
            
            if indexPath.row == 0 {
                if let user = getUser() {
                    cell.titleLabel.text = "\(String(describing: user.firstName!)) \(String(describing: user.lastName!))"
                }else{
                    cell.titleLabel.text = "Guest User"
                }
                
            }
            
            cell.imgView.image = nil
            cell.leadingConstraint.constant  = -15
            cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
        }
        
        if isGuestUser {
            //Guest User
            if indexPath.row == 1 {
                cell.imgView.image = UIImage(named: "icon_contact")
            }else if indexPath.row == 2 {
                cell.imgView.image = UIImage(named: "icon_terms")
            }else if indexPath.row == 3 {
                cell.imgView.image = UIImage(named: "icon_faq")
            }else if indexPath.row == 4 {
                cell.imgView.image = UIImage(named: "icon_signin")
            }
        }else{
            //Session user
            if indexPath.row == 1 {
                
                if let user = getUser() {
                    
                    if user.rewardPoints == nil {
                        cellReward.lblRewards.text = "0 Points"
                    }else{
                        cellReward.lblRewards.text = "\(String(describing: user.rewardPoints!)) Points"
                    }
                }
                
                
                //Border work
                cellReward.borderView.layer.masksToBounds = true
                cellReward.borderView.layer.cornerRadius = 20
                cellReward.borderView.layer.borderWidth = 1
                cellReward.contentView.backgroundColor = .clear// hexStringToUIColor(hex: "#f1f1f1")//.clear
                
                let borderColor: UIColor = hexStringToUIColor(hex: "#e1e1e1")//.lightGray
                cellReward.borderView.layer.borderColor = borderColor.cgColor
                
                cellReward.selectionStyle = .none
                //cellReward.leftInset = -30
                //cellReward.rightInset = 20
                return cellReward
                
            }else if indexPath.row == 2 {
                cell.imgView.image = UIImage(named: "icon_profile")
            }else if indexPath.row == 3 {
                cell.imgView.image = UIImage(named: "icon_order")
            }else if indexPath.row == 4 {
                cell.imgView.image = UIImage(named: "icon_payment")
            }else if indexPath.row == 5 {
                cell.imgView.image = UIImage(named: "icon_contact")
            }else if indexPath.row == 6 {
                cell.imgView.image = UIImage(named: "icon_terms")
            }else if indexPath.row == 7 {
                cell.imgView.image = UIImage(named: "icon_faq")
            }else if indexPath.row == 8 {
                cell.imgView.image = UIImage(named: "icon_chpassword")
            }else if indexPath.row == 9 {
                cell.imgView.image = UIImage(named: "icon_notification")
            }else if indexPath.row == 10 {
                cell.imgView.image = UIImage(named: "icon_settings")
            }else if indexPath.row == 11 {
                cell.imgView.image = UIImage(named: "icon_signout")
            }
        }
        
        
        cell.selectionStyle = .none
        return cell
        
    }

    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0) {
            return 65
        }else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 70
                
            } else {
                
                return 55
                
            }
        }
        //return (indexPath.row == 0) ? 65 : 50
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainViewController = sideMenuController!
        
        if isGuestUser {
            if indexPath.row == 1 {
                //Contact Us
                let vc = self.storyboard?.instantiateViewController(withIdentifier: kContactControllerID) as! ContactUsVC
                vc.title = "Contact Us"//.uppercased()
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            
            if indexPath.row == 2 {
                //Terms
               let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: kTermsControllerID) as! TermsVC
               vc.title = "Terms"
               let navigationController = mainViewController.rootViewController as! UINavigationController
               navigationController.pushViewController(vc, animated: true)
               mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }
            
            if indexPath.row == 3 {
               //FAQs
                let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kFAQControllerID) as! FAQVC
                vc.title = "FAQ"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }
            
            if indexPath.row == 4 {
                //goto welcome screen
                setUser(userObj: nil)
                //popToSignIn()
                
                
                let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
                let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
                let navCon = UINavigationController(rootViewController: welcomeVC)
                let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                window?.rootViewController = navCon
                UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            }
            
        }else{
            //loggedin user
            
            if indexPath.row == 0 {
                if mainViewController.isLeftViewAlwaysVisibleForCurrentOrientation {
                    mainViewController.showRightView(animated: true, completionHandler: nil)
                }
                else {
                    mainViewController.hideLeftView(animated: true, completionHandler: {
                        mainViewController.showRightView(animated: true, completionHandler: nil)
                    })
                }
            }
            else if indexPath.row == 2 {
                // Profile
                
                let storyboard = UIStoryboard(name: kProfileStoryboard, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kMyProfileControllerID) as! ProfileVC
                vc.title = "Profile"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }else if indexPath.row == 3 {
                
                //Order History
              
                let vc = self.storyboard?.instantiateViewController(withIdentifier: kOrderHistoryControllerID) as! OrderVC
                vc.isRecentOrders = false
                vc.title = "Order History".uppercased()
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
                
            }else if indexPath.row == 4 {
                
                //Payment
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: kPaymentMethodsControllerID) as! CardListVC
                
                vc.title = "Payment Method"//.uppercased()
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
                
                
            }else if indexPath.row == 5 {
                
                //Contact Us
                let vc = self.storyboard?.instantiateViewController(withIdentifier: kContactControllerID) as! ContactUsVC
                vc.title = "Contact Us"//.uppercased()
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }else if indexPath.row == 6 {
                
                //Terms
                let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kTermsControllerID) as! TermsVC
                vc.title = "Terms"
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }else if indexPath.row == 7 {
                
                //FAQs
                let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kFAQControllerID) as! FAQVC
                vc.title = "FAQ"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }else if indexPath.row == 8 {
                
                //Change Pass
                if let user = getUser() {
                    if user.userSignupTypeId == 1 {
                        mainViewController.hideLeftView(animated: true, completionHandler: nil)
                        showAlert(title: "", message: "Facebook user can't change the password.", viewController: self)
                        return
                    }
                }
                
                let storyboard = UIStoryboard(name: kChangePasswordStoryboard, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kChangePasswordControllerIDNew) as! ChangePasswordVC
                vc.title = "Change Password"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
               
                
            }else if indexPath.row == 9 {
                
                //Notifications
                let storyboard = UIStoryboard(name: kNotificationsStoryboard, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kNotificationListControllerID) as! NotificationListVC
                vc.title = "Notifications"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
               
                
            }
            else if indexPath.row == 10 {
                
                //Settings
                let storyboard = UIStoryboard(name: kSettingsStroyboard, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: kSettingsControllerID) as! SettingsVC
                vc.title = "Settings"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(vc, animated: true)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                
            }
            else if indexPath.row == 11 {
                
                //Logout
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
                let optionMenu = UIAlertController(title: "", message: "Are you sure you want to signout?", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    //remove udid token
                    //self.apiRemoveUdid()
                    self.logout()
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                })
                
                
                optionMenu.addAction(cancelAction)
                optionMenu.addAction(okAction)
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
    }
    
    
    
    func gotoProfileScreen() {
        
    }
    
    func logout() {
        if let usr = getUser() {
            self.apiLogout(userID: "\(usr.userId!)")
        }
    }
    
    func apiLogout(userID: String) {
        APIClient.LogoutRequest(userId: userID) { (user, error, message) in
            if user != nil {
                //print(res?.responce?.message!)
            }
            if error != nil {
                print(error.debugDescription)
                //showAlert(controller: self, messageStr: "Error", titleStr: error.debugDescription)
            }
            if message != nil {
                print(message!)
                //showAlert(controller: self, messageStr: "Error", titleStr: message!)
                
                if !(message?.contains("Successfully"))! {
                    
                    //showAlert(title: "", message: message!, viewController: self)
                }
            }
            
            if error == nil {
                
                setUser(userObj: nil)
                
                let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
                let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
                
                showToast(viewControl: welcomeVC, titleMsg: "", msgTitle: "\(message!)")
                
                //showAlert(title: "\(message!)", message: "", viewController: welcomeVC)
                
                
                let navCon = UINavigationController(rootViewController: welcomeVC)
                let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                window?.rootViewController = navCon
                UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
                
                
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        print("viewWillTransition----------")
        
        if UIDevice.current.orientation.isLandscape {
            //self.tableView.isScrollEnabled = true
        } else {
            //self.tableView.isScrollEnabled = false
            self.tableView.reloadData()
            self.scrollToTop()
        }
    }
    
    func scrollToTop(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func popToSignIn() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for controller in viewControllers {
                if controller.isKind(of: LoginVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    /*
    func apiRemoveUdid() {
        let uUID = UIDevice.current.identifierForVendor!.uuidString
        APIClient.DeleteDeviceTokenRequest(udid: uUID) { (res, error, message) in
            if res != nil {
                print(res?.responce?.message!)
            }
            if error != nil {
                print(error.debugDescription)
                //showAlert(controller: self, messageStr: "Error", titleStr: error.debugDescription)
            }
            if message != nil {
                print(message!)
                //showAlert(controller: self, messageStr: "Error", titleStr: message!)
            }
        }
    }*/
    
    /*
    @objc func takePhotoandShow() {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let isCameraAvalible = UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            if(isCameraAvalible) {
                self.takePhoto()
            }
        })
        
        let saveAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.PhotoLibray()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancel)
        self.present(optionMenu, animated: true, completion: nil)
    }*/
    
    /*
    func takePhoto() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func PhotoLibray() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate =  self
        self.present(imagePicker, animated: true, completion: nil)
    }*/
    
}
/*
extension LeftViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.selectedImage = pickedImage
            picUpdateApi(image: self.selectedImage!)
        }
        self.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated:true, completion: nil)
    }
    
}*/

/*
extension LeftViewController: editDelegate {
    func editProfileImage() {
        takePhotoandShow()
    }
    
    func editProfile() {
        let mainViewController = sideMenuController!
        
        let editVC = UIStoryboard(name:kProfileStoryboard, bundle:nil).instantiateViewController(withIdentifier: kEditControllerID) as! ProfileVC
        editVC.title = kTitleEditProfile
        
        let navigationController = mainViewController.rootViewController as! NavigationController
        navigationController.pushViewController(editVC, animated: true)
        mainViewController.hideLeftView(animated: true, completionHandler: nil)
    }
}*/
