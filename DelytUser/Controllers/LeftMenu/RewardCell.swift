//
//  RewardCell.swift
//  DelytUser
//
//  Created by UmarFarooq on 25/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class RewardCell: UITableViewCell {
    
    @IBOutlet weak var lblRewards: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
