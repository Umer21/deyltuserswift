//
//  LeftViewCell.swift
//  LGSideMenuControllerDemo
//
import LGSideMenuController

class LeftViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear
        //titleLabel.textColor = .white
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        titleLabel.alpha = highlighted ? 0.5 : 1.0
        
    }

}
