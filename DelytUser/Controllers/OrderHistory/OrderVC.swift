//
//  OrderVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/29/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class OrderVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    let identifierHistory = "cell_orderList"
    var items = [HistoryObj]()
    var isRecentOrders = false
    var selectedObj : HistoryObj?
    var pageCount = 1
    var totalCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblView.isHidden = true
        self.title = "Orders"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.tblView.separatorStyle = .none
        
        self.tblView.es.addInfiniteScrolling {
            [unowned self] in
            /// Do anything you want...
            /// ...
            /// If common end
            self.pageCount = self.pageCount + 1
            
            print("page = \(self.pageCount)")
            print("total = \(self.totalCount)")
            print("array count = \(self.items.count)")
            
            if self.items.count < self.totalCount {
                self.apiOrderHistory()
            }else{
                /// If no more data
                self.tblView.es.noticeNoMoreData()
            }
            
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.resetTable()
        apiOrderHistory()
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterNotifications()
    }
    
    @objc func reloadOrders() {
        self.resetTable()
        apiOrderHistory()
    }
    
    func resetTable(){
        self.tblView.es.resetNoMoreData()
        self.pageCount = 1
        self.items.removeAll()
    }
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOrders), name: CompleteNotification.CompleteOrderNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: CompleteNotification.CompleteOrderNotification, object: nil)
        
    }
    
    func apiOrderHistory() {
      
        APIClient.GetOrderHistroy(isRecentOrder: isRecentOrders, page: pageCount) { (response, error, message) in
            
            if response != nil {
                
                self.totalCount = Int(response!.totalCount!)!
                
                self.items += (response?.result)!//.reversed()
                
                if self.items.count > 0 {
                    //self.lblNodata.isHidden = true
                    self.tblView.isHidden = false
                }else{
                    //self.lblNodata.isHidden = false
                    self.tblView.isHidden = true
                    
                    self.popDoalog()
                }
                
                self.tblView.es.stopLoadingMore()
                self.tblView.reloadData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func popDoalog()
    {
        let optionMenu = UIAlertController(title: "Empty Order", message: "No order has been placed", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.navigationController?.popViewController(animated: true)
            
        })
        
       
        optionMenu.addAction(okAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func gotoCompletedScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kCompletedControllerID) as! CompletedScreenVC
        vc.orderObj = selectedObj
        
        if selectedObj?.RefundReason == nil {
            selectedObj?.RefundReason = ""
        }
        
        vc.myComments = selectedObj!.RefundReason!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoAcceptedScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kAcceptedControllerID) as! AcceptedScreenVC
        vc.orderObj = selectedObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoCancelledScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kCancelledControllerID) as! CancelledScreenVC
        vc.orderObj = selectedObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoCanCancelScreenVC() {
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kCanCancelScreenControllerID) as! CanCancelVC
        vc.orderObj = selectedObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoReceiptScreen(orderid: String, isRefunded: Int, reason: String, mycoment: String, orderStatus: Int) {
        
        
        let storyBoard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kRefundControllerID) as! RefundVC
        vc.orderID = orderid
        vc.isRefunded = isRefunded
        vc.managerReason = reason
        vc.myComents = mycoment
        vc.orderStatus = orderStatus
        self.navigationController?.pushViewController(vc, animated: true)
        
        /*
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReceiptControllerID) as! ReceiptVC
        
        vc.orderID = orderid
        vc.isRefunded = isRefunded
        vc.managerReason = reason
        vc.myComents = mycoment
        self.navigationController?.pushViewController(vc, animated: true)*/
        
    }

}

extension OrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierHistory, for: indexPath as IndexPath) as! OrderHistoryCell
        
        if items.count > 0 {
            let model = items[indexPath.row]
            
            cell.lblDate.text = "\(formatDate(date: model.orderedOn!)) \(formateTime(date: model.orderedOn!))"
            cell.lblTitle.text = model.businessName!
            cell.lblOrderNo.text = "Order # \(model.orderId!)"
            cell.lblOrderStatus.text = "\(model.orderStatusDetail!)"
            
            //0xFFCC00
            if model.orderStatusDetail == "Completed" || model.orderStatusDetail == "Ready"{
                cell.lblOrderStatus.textColor = UIColorFromRGB(rgbValue: 0x669900)
            }else if model.orderStatusDetail == "Accepted"{
                cell.lblOrderStatus.textColor = UIColorFromRGB(rgbValue: 0xFFCC00)
            }else if model.orderStatusDetail == "Initiated" {
                cell.lblOrderStatus.textColor = .black
            }else if model.orderStatusDetail == "Refund" || model.orderStatusDetail == "Refunded" {
                cell.lblOrderStatus.textColor = UIColorFromRGB(rgbValue: 0x0096FF)
            }else{
                cell.lblOrderStatus.textColor = kThemeRedColor
            }
            
            if model.businessLogoUrl != "" {
                cell.imgOrder.af_setImage(withURL: URL(string: model.businessLogoUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 120
        }else{
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = items[indexPath.row]
        selectedObj = model
        
        if model.orderStatus == -1 {
            showToast(viewControl: self, titleMsg: "", msgTitle: "Order has been rollbacked.")
            return
        }
        
        
        if (model.orderStatusDetail == "Completed") {
            gotoCompletedScreen()
            
        }else if (model.orderStatusDetail == "Accepted" || model.orderStatusDetail == "Initiated" || model.orderStatusDetail == "Ready") {
            
            //topup cancel
            if (model.orderLineCanCancel!.count > 0) {
                
                var isTopUp = false
                for (_ , element) in model.orderLineCanCancel!.enumerated() {
                    // your code
                    if element.topUpCode != "" {
                        isTopUp = true
                    }
                }
                
                if isTopUp {
                    gotoCanCancelScreenVC()
                }else{
                    gotoAcceptedScreen()
                }
                
            }else{
                gotoAcceptedScreen()
            }
            
        }else if (model.orderStatusDetail == "Refund" || model.orderStatusDetail == "Refunded"){
            
            
            if selectedObj?.RefundRejectReason == nil {
                selectedObj?.RefundRejectReason = ""
            }
            
            if selectedObj?.IsRefundRequest == nil {
                selectedObj?.IsRefundRequest = 0
            }
            
            if selectedObj?.RefundReason == nil {
                selectedObj?.RefundReason = ""
            }
            
            gotoReceiptScreen(orderid: "\(String(describing: selectedObj!.orderId!))", isRefunded: selectedObj!.IsRefundRequest!, reason: selectedObj!.RefundRejectReason!, mycoment: selectedObj!.RefundReason!, orderStatus: selectedObj!.orderStatus!)
            
        }else{
           gotoCancelledScreen()
        }
        
    }
    
}
