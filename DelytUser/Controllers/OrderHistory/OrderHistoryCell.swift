//
//  OrderHistoryCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/29/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var imgOrder: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()

       //Your separatorLineHeight with scalefactor
       let separatorLineHeight: CGFloat = 15/UIScreen.main.scale

       let separator = UIView()

       separator.frame = CGRect(x: self.frame.origin.x,
                                y: self.frame.size.height - separatorLineHeight,
                            width: self.frame.size.width,
                           height: separatorLineHeight)

       separator.backgroundColor = UIColorFromRGB(rgbValue: 0xf3f3f3)

       self.addSubview(separator)
    }

}
