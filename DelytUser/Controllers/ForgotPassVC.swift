//
//  ForgotPassVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBAction func resetTapped(_ sender: Any) {
        self.apiResetPassword()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    func apiResetPassword() {
        
        if isValidate() {
            APIClient.ResetPasswordRequest(email: txtEmail.text!) { (response, error, message) in
                
                if response != nil {
                    //setUser(userObj: responce)
                    showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        self.gotoChangeScreen()
                    })
                    
                }
                
                if error != nil {
                    print(error.debugDescription)
                }
                
                if message != nil {
                    showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                }
                
            }
        }
    }
    
    func gotoChangeScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard , bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kChangePasswordControllerID) as! ChangePassVC
        vc.modalPresentationStyle = .fullScreen
        vc.email = txtEmail.text!
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func isValidate() -> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        if (self.txtEmail.text?.isEmpty)! {
            message = "Please enter email."
            returnValue = false
        }
        
        if isValidateEmail(email: self.txtEmail.text!) == false {
            message = "Please enter correct email address"
            returnValue = false
        }
        
        
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
