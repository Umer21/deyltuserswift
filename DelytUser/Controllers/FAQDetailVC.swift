//
//  FAQDetailVC.swift
//  DelytUser
//
//  Created by Inabia1 on 06/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class FAQDetailVC: UIViewController {

    @IBOutlet weak var lblFaqAnswer: UILabel!
    @IBOutlet weak var lblFaqQuestion: UILabel!
    
    var faqQuestion: String?
    var faqAnswer: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //print("Question---\(faqQuestion)")
        //print("Answer-----\(faqAnswer)")
        showInformation()
    }
    
    func showInformation()
    {
        self.title = "FAQ Detail"
        lblFaqQuestion.text = faqQuestion
        lblFaqAnswer.text = faqAnswer
    }
    

   

}
