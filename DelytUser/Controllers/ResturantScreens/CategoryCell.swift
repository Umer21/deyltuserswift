//
//  CategoryCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var lbl_categoryName: UILabel!
    @IBOutlet weak var img_category: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
