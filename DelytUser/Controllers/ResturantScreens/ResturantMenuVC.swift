//
//  ResturantMenuVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class ResturantMenuVC: UIViewController {

    let reuseIdentifierMenu = "MenuCell"
    
    var items = [MenuObj]()
    let dbManager = DatabaseHelper()
    var sharedProduct = SharedManager.shared.requestForSharedProduct()
    
    var selectedOptions = [OptionList]()
    var selectedImages = [MenuImages]()
    var selectMenuObj = MenuObj()
    var selectedTitle = ""
    var resturantObj: BusinessObj?
    var isTopUp = false
    
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var tbleView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        self.title = selectedTitle
        
        tbleView.separatorColor = UIColorFromRGB(rgbValue: 0xf3f3f3)
        btnCart.titleLabel?.textAlignment = .center
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        cartBarUpdate()
    }
    
    func cartBarUpdate() {
        if !isTopUp {
            let arrProducts = dbManager.getProdcutWithOrderId(orderId: 0, resturantId: "\(String(describing: resturantObj!.BusinessId!))")
            
            if arrProducts.count > 0 {
                
                if UIDevice.current.userInterfaceIdiom == .pad {
                    heightContraint.constant = 65
                }else{
                   heightContraint.constant = 44
                }
                
                btnCart.setTitle("View Cart \n \(arrProducts.count) item added", for: .normal)
                btnCart.isHidden = false
            }else{
                heightContraint.constant = 0
                btnCart.isHidden = true
            }
        }else{
            heightContraint.constant = 0
            btnCart.isHidden = true
        }
        
        
    }
    
    func gotoDetailsScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kResturantMenuDetailControllerID) as! MenuDetailVC
        vc.items = selectedOptions
        vc.arrImages = selectedImages
        vc.menuObj = selectMenuObj
        vc.resturantObj = self.resturantObj
        vc.isTopUp = isTopUp
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "gotoCartScreen") {
            let vc = segue.destination as! OrderCartVC
            vc.resturantObj = self.resturantObj
        }
    }
}

extension ResturantMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierMenu, for: indexPath as IndexPath) as! MenuCell
        
        let model = self.items[indexPath.row]
        
        cell.lbl_itemName.text = model.MenuName!
        //print((model.MenuPrice!.rounded(toPlaces: 4)))
        
        cell.lbl_itemPrice.text = "$ \(String(format: "%.2f", model.MenuPrice!))"//model.MenuPrice!.rounded(toPlaces: 2)
        cell.lbl_itemDescription.text = "\(model.MenuShortDesc!)"
        cell.delegate = self
        cell.btn_addToCart.tag = indexPath.row
        cell.btn_addToCart.layer.cornerRadius = 5
        
        //cell.img_menu?.backgroundColor = .orange
        cell.img_menu.layer.cornerRadius = 7
        cell.img_menu.clipsToBounds = true
        
        if model.MenuImages!.count > 0 {
            //cell.img_menu.image = UIImage(named: "menu_item_transparent")
            
            if let imgUrl = model.MenuImages?[0].MenuImageUrl {
                cell.img_menu.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "menu_item_transparent")!)
            }
        }else{
            cell.img_menu.image = UIImage(named: "menu_item_transparent")
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 220
        }else{
            return 144
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = self.items[indexPath.row];
        self.selectedOptions = model.OptionList!
        self.selectedImages = model.MenuImages!
        self.selectMenuObj = model
        
        gotoDetailsScreen()
    }
    
}

extension ResturantMenuVC: buttonClickDelegate {
    func buttonTapped(tag: Int) {
        let model = self.items[tag];
        
        //Encoding Object JsonString
        let jsonStr = model.OptionList!.toJSONString(prettyPrint: true)
        print(jsonStr!)
       
        if let user = getUser() {
            
            
            sharedProduct.userId = Int64(user.userId!)
            sharedProduct.menuId = Int64(model.MenuId!)
            sharedProduct.menuName = model.MenuName!
            sharedProduct.orderQuantity = 1
            sharedProduct.itemPrice = model.MenuPrice
            sharedProduct.basePrice = model.MenuPrice
            sharedProduct.sendToServer = "no"
            sharedProduct.optionJson = jsonStr!
            sharedProduct.orderType = "neworder"
            
            if !isTopUp {
                sharedProduct.orderId = 0
            }
            
            if model.OptionList!.count > 1 {
                //for multi-selection product
                
                gotoItemListScreen()
            }else{
                
                if model.OptionList![0].MenuOptionAdd!.count > 1 {
                    gotoItemListScreen()
                }else{
                    var menuOptionIDs = ""
                    for item in model.OptionList! {
                        for option in item.MenuOptionAdd! {
                            menuOptionIDs.append("\(String(describing: option.MenuOptionId!)),")
                        }
                    }
                    sharedProduct.menuOptionId = String(menuOptionIDs.dropLast())
                    
                    let products = dbManager.getProdcutWithCombination(optionsIds: sharedProduct.menuOptionId!, product: sharedProduct)
                    if products.count > 0 {
                        
                        //Quantity Updated
                        sharedProduct = products[0]
                        sharedProduct.orderQuantity = sharedProduct.orderQuantity! + 1
                        dbManager.updateProduct(product: sharedProduct)
                        showToast(viewControl: self, titleMsg: "", msgTitle: "Product quantity updated in cart.")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                            
                            if self.isTopUp {
                                self.popBack(3)
                            }
                            else {
                                //self.popBack(2)
                            }
                            
                        })
                    }else{
                        
                        //Product added in Cart.
                        sharedProduct.orderQuantity = 1
                        dbManager.insertProduct(product: sharedProduct)
                        
                        if !isTopUp {
                            showToast(viewControl: self, titleMsg: "", msgTitle: "Item added in cart.")
                        }else{
                            showToast(viewControl: self, titleMsg: "", msgTitle: "Item added in Top Up Cart.")
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                            if self.isTopUp {
                                self.popBack(3)
                            }
                            else {
                                //self.popBack(2)
                            }
                        })
                        
                    }
                    
                }
                
            }
            
        }else{
            //guest user case
            let optionMenu = UIAlertController(title: "", message: "This feature requires an account.", preferredStyle: .alert)
            
            let LoginAction = UIAlertAction(title: "Login", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.gotoLoginScreen()
            })
            
            let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in })
            
            optionMenu.addAction(LoginAction)
            optionMenu.addAction(CancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
        
        cartBarUpdate()
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    
    
    func gotoItemListScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kItemCartControllerID) as! ItemCartVC
        vc.product = sharedProduct
        vc.editingMode = false
        vc.isTopUp = self.isTopUp
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoLoginScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
        let navCon = UINavigationController(rootViewController: welcomeVC)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = navCon
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
}

