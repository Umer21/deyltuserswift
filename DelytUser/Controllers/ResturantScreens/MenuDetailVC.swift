//
//  MenuDetailVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class MenuDetailVC: UIViewController {

    let reuseIdentifierIntro = "md_IntroCell"
    let reuseIdentifierImgCell = "md_ImgCell"
    let reuseIdentifierDescCell = "md_DescriptionCell"
    let reuseIdentifierSingleCell = "md_SingleCell"
    let reuseIdentifierDoubleCell = "md_DoubleCell"
    let reuseIdentifierCollectionCell = "md_CollectionCell"
    
    var sharedProduct = SharedManager.shared.requestForSharedProduct()
    
    var items = [OptionList]()
    var arrImages = [MenuImages]()
    var menuObj: MenuObj?
    let dbManager = DatabaseHelper()
    var resturantObj: BusinessObj?
    var isTopUp = false
    
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        self.title = "Details"
        btnCart.titleLabel?.textAlignment = .center
        tblView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //cartBarUpdate()
        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        
        cartBarUpdates()
    }
    
    func cartBarUpdates() {
        if !isTopUp {
            let arrProducts = dbManager.getProdcutWithOrderId(orderId: 0, resturantId: "\(String(describing: resturantObj!.BusinessId!))")
            
            if arrProducts.count > 0 {
                
                if UIDevice.current.userInterfaceIdiom == .pad {
                    heightContraint.constant = 65
                }else{
                   heightContraint.constant = 44
                }
                
                btnCart.setTitle("View Cart \n \(arrProducts.count) item added", for: .normal)
            }else{
                heightContraint.constant = 0
            }
        }else{
            heightContraint.constant = 0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "gotoCartScreen") {
            let vc = segue.destination as! OrderCartVC
            vc.resturantObj = self.resturantObj
        }
    }

}

extension MenuDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.items.count + 3 // first three sections are must
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 || section == 2 {
            return 1
        }else{
            return self.items[section - 3].MenuOptionAdd!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierIntro, for: indexPath as IndexPath) as! md_IntroCell
            cell.lbl_itemName.text = self.menuObj?.MenuName!
            cell.lbl_itemText.text = self.menuObj?.MenuShortDesc!
            cell.btn_addToCart.layer.cornerRadius = 5
        
            cell.delegate = self
            
            if self.arrImages.count > 0 {
                let url = self.arrImages[0].MenuImageUrl!
                cell.img_item.af_setImage(withURL: URL(string: url)!, placeholderImage: UIImage(named: "menu_item_transparent")!)//icon_noimage
                cell.img_item.layer.cornerRadius = 7
                cell.img_item.clipsToBounds = true
            }
            
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierImgCell, for: indexPath as IndexPath) as! md_ImgCell
            
            return cell
        }else if indexPath.section == 2 {
           
                let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierDescCell, for: indexPath as IndexPath) as! md_DescriptionCell
                
                
                cell.lbl_itemDescription.text = self.menuObj?.MenuLongDesc!
                
                return cell
            
        }else{
            
            let model = self.items[indexPath.section - 3].MenuOptionAdd![indexPath.row]
            
            if model.MenuOptionValue != nil {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierDoubleCell, for: indexPath as IndexPath) as! md_DoubleCell
                
                //let model = self.items[indexPath.section - 3].MenuOptionAdd![indexPath.row]
                cell.lbl_leftHeading.text = model.MenuOptionKey!
                if model.MenuOptionValue != nil {
                    if model.MenuOptionValue == " "{
                        cell.lbl_rightHeading.text = ""
                    }else{
                        cell.lbl_rightHeading.text = "$ \(model.MenuOptionValue!)"
                    }
                    
                }
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierSingleCell, for: indexPath as IndexPath) as! md_SingleCell
                
                //let model = self.items[indexPath.section - 3].MenuOptionAdd![indexPath.row]
                cell.lbl_leftHeadingSingle.text = model.MenuOptionKey!
                //cell.lbl_itemText.text = model.MenuOptionValue!
                
                
                return cell
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 150
            }else{
                return 120
            }
            
        case 1:
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 150
            }else{
                return 110
            }
            
        case 2:
            if menuObj?.MenuLongDesc == "" {
                return 0
            }
            else {
                
                var font = UIFont()
                if UIDevice.current.userInterfaceIdiom == .pad {
                    font = UIFont(name: "Helvetica Neue", size: 18.0)!
                }else{
                    font = UIFont(name: "Helvetica Neue", size: 14.0)!
                }
                
                return heightForText(text: menuObj!.MenuLongDesc!, Font: font, Width: tableView.frame.width) + 25
            }
        
        default:
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 || section == 1 || section == 2 {
            return ""
        }else{
            let model = self.items[section - 3].OptionAdd
            return model!.OptionName
        }
    }
    /*
    func heightForText(text: String,Font: UIFont,Width: CGFloat) -> CGFloat{

        let constrainedSize = CGSize.init(width:Width, height: CGFloat(MAXFLOAT))
        let attributesDictionary = NSDictionary.init(object: Font, forKey:NSAttributedString.Key.font as NSCopying)
        let mutablestring = NSAttributedString.init(string: text, attributes: attributesDictionary as? [NSAttributedString.Key : Any])
        var requiredHeight = mutablestring.boundingRect(with:constrainedSize, options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), context: nil)
        if requiredHeight.size.width > Width {
            requiredHeight = CGRect.init(x: 0, y: 0, width: Width, height: requiredHeight.height)

        }
        return requiredHeight.size.height;
    }*/
    
}

extension MenuDetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierCollectionCell, for: indexPath as IndexPath) as! DetailCollectionCell
        cell.tag = indexPath.row
        
        
       if self.arrImages.count > 0 {
           let url = self.arrImages[indexPath.row].MenuImageUrl!
           cell.img_menu.af_setImage(withURL: URL(string: url)!, placeholderImage: UIImage(named: "menu_item_transparent")!)
           cell.img_menu.layer.cornerRadius = 7
           cell.img_menu.clipsToBounds = true
        }
       
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: 140, height: 105)
        }else{
            return CGSize(width: 120, height: 85)
        }
        
    }
}

extension MenuDetailVC: IntroCellDelegate {
    func addToCardTapped() {
        
        let model = self.menuObj!
        //Encoding Object JsonString
        let jsonStr = model.OptionList!.toJSONString(prettyPrint: true)
        print(jsonStr!)
        
        if let user = getUser() {
            
            sharedProduct.userId = Int64(user.userId!)
            sharedProduct.menuId = Int64(model.MenuId!)
            sharedProduct.menuName = model.MenuName!
            sharedProduct.orderQuantity = 1
            sharedProduct.itemPrice = model.MenuPrice
            sharedProduct.sendToServer = "no"
            sharedProduct.optionJson = jsonStr!
            sharedProduct.orderType = "neworder"
            sharedProduct.basePrice = model.MenuPrice!
            
            if !isTopUp {
                sharedProduct.orderId = 0
            }
            
            
            if model.OptionList!.count > 1 {
                //for multi-selection product
                
                gotoItemListScreen()
            }else{
                
                if model.OptionList![0].MenuOptionAdd!.count > 1 {
                    gotoItemListScreen()
                }else{
                    var menuOptionIDs = ""
                    for item in model.OptionList! {
                        for option in item.MenuOptionAdd! {
                            menuOptionIDs.append("\(String(describing: option.MenuOptionId!)),")
                        }
                    }
                    sharedProduct.menuOptionId = String(menuOptionIDs.dropLast())
                    
                    let products = dbManager.getProdcutWithCombination(optionsIds: sharedProduct.menuOptionId!, product: sharedProduct)
                    if products.count > 0 {
                        
                        //Quantity Updated
                        sharedProduct = products[0]
                        sharedProduct.orderQuantity = sharedProduct.orderQuantity! + 1
                        dbManager.updateProduct(product: sharedProduct)
                        showToast(viewControl: self, titleMsg: "", msgTitle: "Item updated in cart.")
                        
                    }else{
                        
                        //Product added in Cart.
                        sharedProduct.orderQuantity = 1
                        dbManager.insertProduct(product: sharedProduct)
                        
                        if !isTopUp {
                            showToast(viewControl: self, titleMsg: "", msgTitle: "Item added in cart.")
                        }else{
                            showToast(viewControl: self, titleMsg: "", msgTitle: "Item added in Top Up Cart.")
                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        
                        if !self.isTopUp {
                            self.popToMenu()
                        }else{
                            self.backToAcceptedScreen()
                        }
                        
                    })
                    
                }
            }
        }else{
            //guest user case
            let optionMenu = UIAlertController(title: "", message: "This feature requires an account.", preferredStyle: .alert)
            
            let LoginAction = UIAlertAction(title: "Login", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.gotoLoginScreen()
            })
            
            let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in })
            
            optionMenu.addAction(LoginAction)
            optionMenu.addAction(CancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
        
        cartBarUpdates()
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func popToMenu() {
        
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for controller in viewControllers {
                if controller.isKind(of: ResturantMenuVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func backToAcceptedScreen() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            for controller in viewControllers {
                if controller.isKind(of: AcceptedScreenVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func gotoItemListScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kItemCartControllerID) as! ItemCartVC
        vc.product = sharedProduct
        vc.editingMode = false
        vc.isTopUp = self.isTopUp
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoLoginScreen() {
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
        let navCon = UINavigationController(rootViewController: welcomeVC)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = navCon
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
}

extension MenuDetailVC: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = item
    }
}
