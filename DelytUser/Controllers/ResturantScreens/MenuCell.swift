//
//  MenuCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol buttonClickDelegate {
    func buttonTapped(tag: Int)
}

class MenuCell: UITableViewCell {
    var delegate: buttonClickDelegate?
    
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var lbl_itemDescription: UILabel!
    @IBOutlet weak var lbl_itemPrice: UILabel!
    @IBOutlet weak var img_menu: UIImageView!
    @IBOutlet weak var btn_addToCart: UIButton!
    @IBAction func addOrderTapped(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.buttonTapped(tag: button.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
