//
//  IntroCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import HCSStarRatingView

class IntroCell: UITableViewCell {

    @IBOutlet weak var btn_deals: UIButton!
    @IBOutlet weak var btn_info: UIButton!
    @IBOutlet weak var btn_reviews: UIButton!
    @IBOutlet weak var lbl_restaurantDescription: UILabel!
    @IBOutlet weak var lbl_restaurantName: UILabel!
    @IBOutlet weak var lbl_restaurantReviewCount: UILabel!
    @IBOutlet weak var img_Logorestaurant: UIImageView!
    @IBOutlet weak var viewStars: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
