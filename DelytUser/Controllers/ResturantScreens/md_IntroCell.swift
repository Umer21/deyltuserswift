//
//  md_IntroCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
protocol IntroCellDelegate {
    func addToCardTapped()
}
class md_IntroCell: UITableViewCell {

    var delegate: IntroCellDelegate?
    
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var lbl_itemText: UILabel!
    @IBOutlet weak var img_item: UIImageView!
    @IBOutlet weak var btn_addToCart: UIButton!
    
    @IBAction func addToCardTapped(_ sender: Any) {
        self.delegate?.addToCardTapped()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()

       //Your separatorLineHeight with scalefactor
       let separatorLineHeight: CGFloat = 5/UIScreen.main.scale

       let separator = UIView()

       separator.frame = CGRect(x: self.frame.origin.x,
                                y: self.frame.size.height - separatorLineHeight,
                            width: self.frame.size.width,
                           height: separatorLineHeight)

       separator.backgroundColor = UIColorFromRGB(rgbValue: 0xf9f9f9)

       self.addSubview(separator)
    }

}
