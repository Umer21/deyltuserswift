//
//  ResturantVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 10/18/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class ResturantVC: UIViewController {

    @IBOutlet weak var tbleView: UITableView!
    let reuseIdentifierImage = "ImageCell"
    let reuseIdentifierIntro = "IntroCell"
    let reuseIdentifierCategory = "CategoryCell"
    
    var items = [CategoryListObj]()
    var resturantId = ""
    var imgTop = ""
    var businessObj: BusinessObj?
    var menuItems = [MenuObj]()
    var selectedTitle = ""
    var isTopUp = false
    var isLandscapeMode = false
    
    let dbManager = DatabaseHelper()
    let sharedProduct = SharedManager.shared.requestForSharedProduct()
    
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var heightContraint: NSLayoutConstraint!
    
    
    @IBAction func cartBarTapped(_ sender: Any) {
        //gotoOrderCartScreen()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tbleView.isHidden = true
        
        sharedProduct.restaurantId = Int64(resturantId)
        apiGetResturantDetails()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        btnCart.titleLabel?.textAlignment = .center
        tbleView.separatorColor = UIColorFromRGB(rgbValue: 0xf3f3f3)
        
        //Setting Orentation Bool
        if UIApplication.shared.statusBarOrientation.isLandscape {
            isLandscapeMode = true
        } else {
            isLandscapeMode = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewDidLayoutSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() { //This ensures all the layout has been applied properly.
        super.viewDidLayoutSubviews()
        cartBar()
    }
    
    func cartBar() {
        if !isTopUp {
            let arrProducts = dbManager.getProdcutWithOrderId(orderId: 0, resturantId: resturantId)
            
            if arrProducts.count > 0 {
                
                if UIDevice.current.userInterfaceIdiom == .pad {
                    heightContraint.constant = 65
                }else{
                   heightContraint.constant = 44
                }
                btnCart.setTitle("View Cart \n \(arrProducts.count) item added", for: .normal)
                btnCart.isHidden = false
            }else{
                heightContraint.constant = 0
                btnCart.isHidden = true
            }
        }else{
            heightContraint.constant = 0
            btnCart.isHidden = true
        }
        
        
        print("\(heightContraint.constant)")
    }
    
    
    
    func apiGetResturantDetails() {
        APIClient.GetResturantDetials(id: resturantId) { (response, error, message) in
            if response != nil {
                
                self.items = (response?.result?.categoryListObj)!
                if let obj = response?.result?.businessInfo?.businessImages {
                    if obj.count > 0 {
                        self.imgTop = obj[0].BusinessImageUrl!
                    }
                    
                }
                self.businessObj = (response?.result?.businessInfo?.businessObj)!
                self.sharedProduct.restauntName = self.businessObj?.BusinessName!
                //self.items = (response?.result)!
                
                if self.items.count > 0 {
                    //self.lblNodata.isHidden = true
                    self.tbleView.isHidden = false
                }else{
                    //self.lblNodata.isHidden = false
                    self.tbleView.isHidden = false
                }
                
                self.tbleView.reloadData()
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func gotoOrderCartScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kOrderCartControllerID) as! OrderCartVC
        vc.resturantObj = self.businessObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoMenuListScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kResturantDetailControllerID) as! ResturantMenuVC
        vc.items = self.menuItems
        vc.selectedTitle = selectedTitle
        vc.resturantObj = self.businessObj
        vc.isTopUp = isTopUp
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "gotoCartScreen") {
            let vc = segue.destination as! OrderCartVC
            vc.resturantObj = self.businessObj
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        print("viewWillTransition----------")
        
        if UIDevice.current.orientation.isLandscape {
            print("landscape")
            isLandscapeMode = true
        } else {
            print("portrait")
            isLandscapeMode = false
        }
        
        self.tbleView.reloadData()
        
    }

}

extension ResturantVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return self.items.count
        default:
            return 1
           //break
     }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierImage, for: indexPath as IndexPath) as! ImageCell
            
            if imgTop != "" {
                cell.img_restaurant.af_setImage(withURL: URL(string: imgTop)!, placeholderImage: UIImage(named: "icon_noimage2")!)
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierIntro, for: indexPath as IndexPath) as! IntroCell
            
            if let model = self.businessObj {
                cell.lbl_restaurantName.text = model.BusinessName!
                self.title = model.BusinessName!
                cell.lbl_restaurantDescription.text = model.BusinessAddress!
                cell.lbl_restaurantReviewCount.text = "(\(String(describing: model.Reviews!)))"
                cell.viewStars.value = CGFloat(model.BusinessRating!)
                if let imgUrl = model.BusinessLogoUrl {
                    cell.img_Logorestaurant.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
                }
                
                cell.btn_deals.addTarget(self, action: #selector(deals(sender:)), for: .touchUpInside)
                cell.btn_deals.tag = indexPath.row
                
                cell.btn_info.addTarget(self, action: #selector(info(sender:)), for: .touchUpInside)
                cell.btn_info.tag = indexPath.row
                
                cell.btn_reviews.addTarget(self, action: #selector(reviews(sender:)), for: .touchUpInside)
                cell.btn_reviews.tag = indexPath.row
                
                if  UIDevice.current.userInterfaceIdiom == .pad {
                    cell.btn_deals.imageEdgeInsets = UIEdgeInsets(top: -20, left: 48, bottom: 0, right: 0)
                    cell.btn_info.imageEdgeInsets = UIEdgeInsets(top: -20, left: 35, bottom: 0, right: 0)
                    cell.btn_reviews.imageEdgeInsets = UIEdgeInsets(top: -20, left: 48, bottom: 0, right: 0)
                    
                    cell.btn_deals.titleEdgeInsets = UIEdgeInsets(top: 50, left: -40, bottom: 0, right: 0)
                    cell.btn_info.titleEdgeInsets = UIEdgeInsets(top: 50, left: -45, bottom: 0, right: 0)
                    cell.btn_reviews.titleEdgeInsets = UIEdgeInsets(top: 50, left: -64, bottom: 0, right: 0)
                    
                }else{
                    cell.btn_deals.imageEdgeInsets = UIEdgeInsets(top: -20, left: 40, bottom: 0, right: 0)
                    cell.btn_info.imageEdgeInsets = UIEdgeInsets(top: -20, left: 26, bottom: 0, right: 0)
                    cell.btn_reviews.imageEdgeInsets = UIEdgeInsets(top: -20, left: 40, bottom: 0, right: 0)
                    
                    cell.btn_deals.titleEdgeInsets = UIEdgeInsets(top: 35, left: -31, bottom: 0, right: 0)
                    cell.btn_info.titleEdgeInsets = UIEdgeInsets(top: 35, left: -35, bottom: 0, right: 0)
                    cell.btn_reviews.titleEdgeInsets = UIEdgeInsets(top: 35, left: -38, bottom: 0, right: 0)
                }
                
            }
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCategory, for: indexPath as IndexPath) as! CategoryCell
            
            let model = self.items[indexPath.row]
            cell.lbl_categoryName.text = model.CategoryName!
        
        if let imgUrl = model.CategoryIconUrl {
            cell.img_category.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_filter_noimage")!)
        }
        
        return cell
        
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierImage, for: indexPath as IndexPath)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                return 170
            case .pad:
                if isLandscapeMode {
                    return 450
                }else {return 350}
                
            case .unspecified:
                return 170
            default:
                return 170
            }
            
        case 1:
            if  UIDevice.current.userInterfaceIdiom == .pad {
                return 340
            }else{
                return 200
            }
                
        case 2:
            if  UIDevice.current.userInterfaceIdiom == .pad {
                return 125
            }else {
                return 106
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 2 {
            if(self.items.count == 0)
            {
                self.tbleView.separatorStyle = .none
                return ""
            }
            else{
                self.tbleView.separatorStyle = .singleLine
                return "Menu"
            }
            
        }else{
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 || indexPath.section == 1 {
            return
        }
        
        let model = self.items[indexPath.row]
        self.menuItems = model.MenuList!
        selectedTitle = model.CategoryName!
        
        if(self.menuItems.count > 0)
        {
            gotoMenuListScreen()
        }
        else
        {
            showToast(viewControl: self, titleMsg: "", msgTitle: "Menu not available")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            
            if self.items.count > 0 {
                return 40
            }else{
                return 0
            }
            
        }else{
            return 0
        }
        
    }
    
    
    @objc func deals(sender: UIButton){
        
        print("Click Deals")
        
        let optionMenu = UIAlertController(title: "", message: "No deals available.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(okAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @objc func info(sender: UIButton){
        
        print("Click Info")
        gotoInfoScreen()
    }
    
    @objc func reviews(sender: UIButton){
        
        print("Click Reviews")
        gotoReviewScreen()
        
    }
    
    func gotoInfoScreen() {
        
        let storyboard = UIStoryboard(name: kReviewsStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kInfoControllerID) as! InfoVC
        vc.infoObj = self.businessObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoReviewScreen() {
        
        let storyboard = UIStoryboard(name: kReviewsStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReviewsControllerID) as! ReviewsVC
        vc.resturantId = resturantId
        vc.restaurantName = self.businessObj!.BusinessName!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}

