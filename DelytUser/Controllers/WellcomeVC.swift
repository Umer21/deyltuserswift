//
//  WellcomeVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 9/27/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import QuartzCore

class WellcomeVC: UIViewController {

    @IBOutlet weak var btnGuest: UIButton!
    @IBOutlet weak var btnCreateAcc: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBAction func loginGuestTapped(_ sender: Any) {
        gotoHomeScreen()
    }
    
    @IBAction func nativeLoginTapped(_ sender: Any) {
        
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        
        UINavigationBar.appearance().barTintColor = kThemeDarkRedColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        
        //UIApplication.shared.isStatusBarHidden = true
        
        btnGuest.layer.cornerRadius = 2
        btnCreateAcc.layer.borderWidth = 0.5
        btnCreateAcc.layer.borderColor = UIColor.white.cgColor
        btnCreateAcc.layer.cornerRadius = 2
        btnCreateAcc.clipsToBounds = true
        
        btnLogin.layer.borderColor = UIColor.white.cgColor
        btnLogin.layer.cornerRadius = 2
        btnLogin.clipsToBounds = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //self.setNeedsStatusBarAppearanceUpdate()
        //UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        print("-----------viewWillTransition----------")
        
        if UIDevice.current.orientation.isLandscape {
            imgView.image = UIImage(named: "bg_intro_lanscape")
        } else {
            imgView.image = UIImage(named: "bg_intro")
            
        }
        
        print(size)
    }
    
    
    func gotoHomeScreen(){
       
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: kNavControllerID) as! UINavigationController
        let mainViewController = storyboard.instantiateInitialViewController() as! MainViewController
        let vc = storyboard.instantiateViewController(withIdentifier: kHomeControllerID) as! HomeVC
        vc.isGuestUser = true
        
        navigationController.setViewControllers([vc], animated: false)
        mainViewController.rootViewController = navigationController
        mainViewController.modalPresentationStyle = .fullScreen
        
        //self.present(mainViewController, animated: true, completion: nil)
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = mainViewController
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    

}
