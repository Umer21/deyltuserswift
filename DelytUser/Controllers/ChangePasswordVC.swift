//
//  ChangePasswordVC.swift
//  DelytUser
//
//  Created by Inabia1 on 07/01/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var tfCurrentPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    var emailID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
    }
    
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    

    func setNavBar() {
        
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveButtonTapped))
        //self.navigationItem.leftBarButtonItems = [crossButton]
        self.navigationItem.rightBarButtonItems = [saveButton]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("iPad")
            
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20),NSAttributedString.Key.foregroundColor: UIColor.white]
            
            let font = UIFont.systemFont(ofSize: 20);
            saveButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for:UIControl.State.normal)
            
        }else{
            print("not iPad")
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),NSAttributedString.Key.foregroundColor: UIColor.white]
            
            let font = UIFont.systemFont(ofSize: 16);
            saveButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for:UIControl.State.normal)
        }
        
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        
    }
    
    @objc func saveButtonTapped() {
        if isValidate()
        {
            apiChangePassword()
            print("Done")
        }
    }
    
    func isValidate()-> Bool {
        //logics for validations goes here
        var returnValue = true
        var message = ""
        //return returnValue //TODO for fast navigation
        
        if (self.tfNewPassword.text?.isEmpty)! {
            message = "Please enter new password."
            returnValue = false
        }
        
        if (self.tfCurrentPassword.text?.isEmpty)! {
            message = "Please enter current password."
            returnValue = false
        }
        
        
        if (self.tfNewPassword.text != self.tfConfirmPassword.text) {
            message = "Please enter match passwords."
            returnValue = false
        }
        
        if (self.tfNewPassword.text == self.tfCurrentPassword.text) {
            message = "Old and new password should not be same."
            returnValue = false
        }
        
        if (self.tfNewPassword.text!.isEmpty && self.tfCurrentPassword.text!.isEmpty && self.tfConfirmPassword.text!.isEmpty) {
            message = "Fields cannot be empty."
            returnValue = false
        }
       
        if !returnValue {
            showToast(viewControl: self, titleMsg: "Validation", msgTitle: message)
            
        }
        
        return returnValue
    }
    
    func apiChangePassword() {
        if isValidate() {
            
            let userObj = getUser()
            
            if let custID = userObj?.email
            {
                emailID = String(custID)
            }
            
            APIClient.ChangeCustomerPassword(email: emailID!, oldPassword: tfCurrentPassword.text!, newPassword: tfNewPassword.text!) { (response, error, message) in
                
                if response != nil {
                    
                    if(response?.status == "True")
                    {
                        showToast(viewControl: self, titleMsg: "", msgTitle: "Password changed successfully")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                            
                            showToast(viewControl: self, titleMsg: "", msgTitle: "Signed Out Successfully.")
                            self.gotoHomeScreen()
                            
                        })
                        
                    }
                    else if(response?.status == "False"){
                        showToast(viewControl: self, titleMsg: "Error", msgTitle: response!.message!)
                    }
                }
                
                if error != nil {
                    print(error.debugDescription)
                }
            }
        }
    }
    
    func gotoHomeScreen()
    {
        setUser(userObj: nil)
        
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let welcomeVC = storyboard.instantiateViewController(withIdentifier: kWelcomeControllerID)
        let navCon = UINavigationController(rootViewController: welcomeVC)
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = navCon
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }

}
