//
//  CompletedScreenVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/21/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class CompletedScreenVC: UIViewController {

    let reuseIdentifierCellOne = "CompletedCellOne"
    let reuseIdentifierCellTwo = "CompletedCellTwo"
    
    var orderObj: HistoryObj?
    var businessLogoUrl = ""
    var date = ""
    var businessName = ""
    var orderid = ""
    var isRef = 0
    var managerReason = ""
    var myComments = ""
    var isQuatityUpdate = false
    
    let dbManager = DatabaseHelper()
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
        if let oID = orderObj?.orderId {
            orderid = String(oID)
        }
        
        if let isRe = orderObj?.IsRefundRequest {
            isRef = isRe
        }
        
        if let reason = orderObj?.RefundRejectReason {
            managerReason = reason
        }else{
            managerReason = "N/A"
        }
        
        if let comment = orderObj?.RefundReason {
            myComments = comment
        }
        
        //managerReason = "ABCB"
        
        self.title = "Order # \(orderid)"
        populateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        registerNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //unregisterNotifications()
    }
    
    func populateData() {
        if let object = orderObj {
            businessName = object.businessName!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
            let convertedDate = dateFormatter.string(from: parseDateToDateFormat(object.orderedOn!))
            //date = convertedDate
            date = "\(formatDate(date: object.orderedOn!)) \(formateTime(date: object.orderedOn!))"
            
            //date = object.orderedOn!
            businessLogoUrl = object.businessLogoUrl!
            
            if let oID = object.orderId
            {
                orderid = String(oID)
            }
            
            
            //lbl_ResturantName.text = object.businessName
            //lbl_Date.text = object.orderedOn
            
            //self.items = object.orderLineCancelled!
            tblView.reloadData()
        }
       
    }
    
    func gotoReceiptScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReceiptControllerID) as! ReceiptVC
        vc.orderID = orderid
        //print(orderObj?.IsRefundRequest)
        vc.isRefunded = isRef
        vc.managerReason = managerReason
        vc.myComents = myComments
        vc.orderStatus = orderObj!.orderStatus!
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func gotoTipScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kTipControllerID) as! TipVC
        vc.orderObj = orderObj
        vc.isFromOrderScreen = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoReviewScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReviewOneControllerID) as! ReviewOneVC
        vc.orderObj = orderObj
        vc.isFromOrderScreen = true
        //vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(setGivenReviewValue), name: ReviewOneBoolNotification.ReviewGiveNotification, object: nil)
    }
    
    func unregisterNotifications() {
        NotificationCenter.default.removeObserver(self, name: ReviewOneBoolNotification.ReviewGiveNotification, object: nil)
    }
    
    @objc func setGivenReviewValue(notification: NSNotification) {
        print("\(String(describing: notification.userInfo))")
        
        let info0 = notification.userInfo?["UserName"]
        let info1 = notification.userInfo?["Something"]
        
        print("\(info0), \(info1)")
         self.orderObj?.orderRating = 1
    }
    
}

extension CompletedScreenVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5//6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCellOne, for: indexPath as IndexPath) as! CompletedCellOne
            
            cell.lblResturantName.text = businessName
            cell.lblDate.text = date
            cell.contentView.isUserInteractionEnabled = false
            
             if (businessLogoUrl != "") {
                cell.imgResturant.af_setImage(withURL: URL(string: businessLogoUrl)!, placeholderImage: UIImage(named: "icon_noimage")!)
             }
        
            cell.selectionStyle = .none
             return cell
            //break
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCellTwo, for: indexPath as IndexPath) as! CompletedCellTwo
            cell.selectionStyle = .none
            cell.contentView.isUserInteractionEnabled = false
             return cell
            //break
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCellTwo, for: indexPath as IndexPath) as! CompletedCellTwo
            cell.btn.setTitle("Tip", for: .normal)
            cell.contentView.isUserInteractionEnabled = false
            
            cell.selectionStyle = .none
             return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCellTwo, for: indexPath as IndexPath) as! CompletedCellTwo
            cell.btn.setTitle("Review", for: .normal)
            cell.contentView.isUserInteractionEnabled = false
            cell.selectionStyle = .none
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCellTwo, for: indexPath as IndexPath) as! CompletedCellTwo
            cell.btn.setTitle("Ask for Refund", for: .normal)
            cell.contentView.isUserInteractionEnabled = false
            cell.selectionStyle = .none
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierCellTwo, for: indexPath as IndexPath) as! CompletedCellTwo
            cell.btn.setTitle("Add Order to Cart", for: .normal)
            cell.contentView.isUserInteractionEnabled = false
            cell.selectionStyle = .none
            return cell
            
        default:
            return tableView.dequeueReusableCell(withIdentifier: "", for: indexPath as IndexPath)
            //break
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 100
            }else {
                return 85
            }
        case 1,2,3,4,5:
            if UIDevice.current.userInterfaceIdiom == .pad {
                return 60
            }else {
                return 44
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //selectedResturantId = "\(String(describing: model.BusinessId!))"
        //gotoResturantScreen()
        
        switch indexPath.row {
        case 1:
            print("1")
            apiCheckStatus()
            
        case 2:
            
            if orderObj?.orderTip == nil {
                gotoTipScreen()
            }else{
                //showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "You have already given tip for the order!")
                
                self.popDailog()
            }
            
        case 3:
            
            if orderObj?.orderRating == 0 {
                gotoReviewScreen()
            }else{
                gotoReviewsScreen()
            }
        case 4:
            //apiCheckStatusGoRefund()
            self.gotoRefundScreen()
            
        case 5:
            print("Add same order to cart")
            let arrPlacedOrders = self.orderObj?.orderLine
            if arrPlacedOrders!.count > 0 {
                for item in (arrPlacedOrders?.enumerated())! {
                    //print(item.element.orderId)
                    for option in (item.element.OptionList?.enumerated())!{
                        self.encodeToDatabase(optionList: [option.element], menuId: item.element.menuId!, menuName: item.element.menuName!, unitPrice: item.element.unitPrice!, menuOptionId: item.element.orderLineMenuOptionViewModel![0].menuOptionId!, resturantName: orderObj!.businessName!, resturantId:orderObj!.businessId!)
                    }
                }
                
                //Goto Cart Screen
                DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                    
                    if self.isQuatityUpdate == false {
                        self.gotoOrderCartScreen(businessId: self.orderObj!.businessId!)
                    }
                })
            }
            
        default:
            print("default")
        }
    }
    
    func encodeToDatabase(optionList: [OptionList], menuId: Int, menuName: String, unitPrice: Double, menuOptionId:Int, resturantName: String, resturantId: Int){
        
        //Encoding Object JsonString
        let jsonStr = optionList.toJSONString(prettyPrint: true)
         print(jsonStr!)
        
         if let user = getUser() {
             
            let reOrderProduct = Product()
            
             reOrderProduct.userId = Int64(user.userId!)
             reOrderProduct.menuId = Int64(menuId)
             reOrderProduct.menuName = menuName
             reOrderProduct.orderQuantity = 1
             reOrderProduct.itemPrice = unitPrice
             reOrderProduct.basePrice = unitPrice
             reOrderProduct.sendToServer = "no"
             reOrderProduct.optionJson = jsonStr!
             reOrderProduct.orderType = "neworder"
             reOrderProduct.orderId = 0
             reOrderProduct.menuOptionId = "\(menuOptionId)"
             reOrderProduct.restauntName = resturantName
             reOrderProduct.restaurantId = Int64(resturantId)
            
            
             let products = dbManager.getProdcutWithCombination(optionsIds: reOrderProduct.menuOptionId!, product: reOrderProduct)
             if products.count > 0 {
                 
                 //Quantity Updated
                /*
                 reOrderProduct = products[0]
                 reOrderProduct.orderQuantity = reOrderProduct.orderQuantity! + 1
                 dbManager.updateProduct(product: reOrderProduct)*/
                 showToast(viewControl: self, titleMsg: "", msgTitle: "Order Already Added.")
                
                 isQuatityUpdate = true
                
             }else{
                 
                 //Product added in Cart.
                 reOrderProduct.orderQuantity = 1
                 dbManager.insertProduct(product: reOrderProduct)
                 showToast(viewControl: self, titleMsg: "", msgTitle: "Order Added Successfully.")
                 
             }
         }
    }
    
    func gotoOrderCartScreen(businessId: Int) {
        let storyboard = UIStoryboard(name: kCartStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kOrderCartControllerID) as! OrderCartVC
        vc.resturantObj = BusinessObj()
        
        vc.resturantObj?.IsAllowDineIn = self.orderObj?.IsAllowDineIn
        vc.resturantObj?.IsAllowTakeOut = self.orderObj?.IsAllowTakeOut
        vc.resturantObj?.IsAllowDelivery = self.orderObj?.IsAllowDelivery
        vc.resturantObj!.BusinessId = businessId
    
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoRefundScreen(){
        let storyBoard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: kRefundControllerID) as! RefundVC
        vc.orderID = "\(orderObj!.orderId!)"
        //handling for nil
        if orderObj!.IsRefundRequest == nil {
            orderObj?.IsRefundRequest = 0
        }
        vc.isRefunded = orderObj!.IsRefundRequest!
        vc.managerReason = managerReason
        vc.myComents = myComments
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func apiCheckStatus() {
        APIClient.CheckReceiptRequest(orderId: orderid) { (response, error, message) in
            if response != nil {
                //showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                
                if response?.customerOrderReceipt?.orderId != nil {
                    self.gotoReceiptScreen()
                }else{
                    showToast(viewControl: self, titleMsg: "", msgTitle: "Receipt is generating, wait for some time!")
                }
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func apiCheckStatusGoRefund() {
        APIClient.CheckReceiptRequest(orderId: orderid) { (response, error, message) in
            if response != nil {
                //showToast(viewControl: self, titleMsg: "", msgTitle: response!.message!)
                
                if response?.customerOrderReceipt?.orderId != nil {
                    self.gotoRefundScreen()
                }else{
                    showToast(viewControl: self, titleMsg: "", msgTitle: "Receipt is generating, wait for some time!")
                }
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
            }
        }
    }
    
    func popDailog() {
        let optionMenu = UIAlertController(title: "", message: "You have already given tip for the order!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
       
        optionMenu.addAction(okAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func gotoReviewsScreen() {
        let storyboard = UIStoryboard(name: kReviewsStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kReviewsControllerID) as! ReviewsVC
        
        vc.isFromSideMenu = true
        vc.reviewObj.customerName = orderObj!.businessReview!.customerName!
        vc.reviewObj.reviewDate = orderObj!.businessReview!.reviewDate!
        vc.reviewObj.reviewText = orderObj!.businessReview!.reviewText!
        vc.reviewObj.orderRating = orderObj!.businessReview!.orderRating!
        vc.restaurantName = orderObj!.businessName!
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension CompletedScreenVC: TipGivenDelegate {
    func tipGivenSuccessfully() {
        self.orderObj?.orderTip = 1
    }
    
}

