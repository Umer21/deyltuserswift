//
//  CancelledScreenVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/21/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class CancelledScreenVC: UIViewController {

    var orderObj: HistoryObj?
    let reuseIdentifierTable = "NotEditableOrderCell"
    var items = [OrderLine]()
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl_ResturantName: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtOrderCancelReason: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        populateData()
    }
    
    func populateData() {
        if let object = orderObj {
            
            self.title = "Order # \(object.orderId!)"
            lbl_ResturantName.text = object.businessName
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
            //let convertedDate = dateFormatter.string(from: parseDateToDateFormat(object.orderedOn!))
            //lbl_Date.text = convertedDate
            lbl_Date.text = "\(formatDate(date: object.orderedOn!)) \(formateTime(date: object.orderedOn!))"
            
            if (object.businessLogoUrl != "") {
                imgView.af_setImage(withURL: URL(string: object.businessLogoUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
            }
            
            var orderReason = ""
            for item in object.orderLineCancelled! {
                
                if item.cancelReason != nil && item.cancelReason != "" {
                    if !orderReason.contains(item.cancelReason!) {
                        orderReason.append("\(String(describing: item.cancelReason!))\n")
                    }
                    
                }
                
            }
            
            self.txtOrderCancelReason.isUserInteractionEnabled = false
            if(orderReason.isEmpty || orderReason == "" || orderReason.count < 0)
            {
                self.txtOrderCancelReason.text = "N/A"
            }
            else
            {
                
                self.txtOrderCancelReason.text = orderReason
            }
            
            self.items = object.orderLineCancelled!
            tblView.reloadData()
        }
       
    }
    

}

extension CancelledScreenVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! NotEditableOrderCell
        
        let model = self.items[indexPath.row]
        cell.lbl_itemName.text = model.menuName!
        //cell.lbl_itemStatus.text = model.orderLineStatusDescription
        cell.lbl_totalPriceItem.text = "$\(model.unitPrice!)"
        cell.lbl_quantityItem.text = "\(model.quantity!)"
        cell.lbl_singleItemPrice.text = "$\(model.unitPrice!)"
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 0
       }
       
       func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
           return 0
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.items[indexPath.row]
        //selectedResturantId = "\(String(describing: model.BusinessId!))"
        //gotoResturantScreen()
    }
    
}
