//
//  CanCancelCell.swift
//  DelytUser
//
//  Created by Muhammad Umar on 12/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class CanCancelCell: UITableViewCell {

    @IBOutlet weak var lbl_orderStatus: UILabel!
    @IBOutlet weak var lbl_orderDate: UILabel!
    @IBOutlet weak var lbl_orderTopUpName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
