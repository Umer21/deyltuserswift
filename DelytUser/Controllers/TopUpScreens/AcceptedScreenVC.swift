//
//  AcceptedScreenVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/21/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class AcceptedScreenVC: UIViewController {

    var orderObj: HistoryObj?
    let reuseIdentifierTable = "NotEditableOrderCell"
    let reuseIdentifierTableTwo = "NotEditableOrderCellTwo"
    var items = [Product]()
    var resturantID = ""
    var orderTypeView : OrderTypeView?
    var orderInstructions = ""
    var isProductAdded = false
    
    var total = 0.0
    var totalItems = 0
    var subTotal = "0.0"
    var subTotalFromService = "0.0"
    var redeemDiscountFromService = "0"
    var redeemDiscount = "0"
    var redeemPoints = "0"
    var pointID = "0"
    var isavaildiscount = "0"
    var isTopUpOrder = false
    
    @IBOutlet weak var txtOrderInstructions: UITextView!
    
    var sharedProduct = SharedManager.shared.requestForSharedProduct()
    let dbManager = DatabaseHelper()
    
    @IBOutlet weak var updateOrderIB: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbl_ResturantName: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_TotalPrice: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    
    //instruction view properties
    @IBOutlet weak var instructView: UIView!
    @IBOutlet weak var lblAlergies: UILabel!
    @IBOutlet weak var txtInstructions: UITextView!
    @IBOutlet weak var btnDone: UIButton!
    @IBAction func doneTapped(_ sender: Any) {
       removeViewWithAnimation()
              
        self.doneTapped(Instructions: txtInstructions.text!)
        /*if isValid() {
            self.doneTapped(Instructions: txtInstructions.text!)
        }*/
    }
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func addToOrderTapped(_ sender: Any) {
        sharedProduct.orderId = Int64(orderObj!.orderId!)
        gotoResturantScreen()
    }
    
    @IBAction func cancelOrderTapped(_ sender: Any) {
        cancelOrder()
    }
    
    @IBAction func updateOrderTapped(_ sender: Any) {
        
        if isProductAdded == false
        {
            print("added----------")
            let optionMenu = UIAlertController(title: "", message: "You have not changed any item", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
            })
            
            optionMenu.addAction(okAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
        else{
            addNibView()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(AcceptedScreenVC.self)
        self.instructView.isHidden = true
        
        updateOrderIB.layer.cornerRadius = 5
        
        tblView.backgroundColor = UIColorFromRGB(rgbValue: 0xececec)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        registerNotifications()
        populateData()
    }
    
    func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(setGivenDiscountValueForUpdate), name: ReDeemDiscountBoolNotificationForUpdateOrder.RedeemGiveNotificationUpdateOrder, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(fromItemCartScreenUpdate), name: ItemCartBoolNotification.ItemCartNotification, object: nil)
    }
    
    @objc func setGivenDiscountValueForUpdate(notification: NSNotification) {
        
        if let object = notification.object as? [String: Any] {
            if let discount = object["DiscountAmount"] as? String {
                self.redeemDiscount = discount
                print("Redeem-Discount-From-back: \(discount)")
                
                if discount == "0.0" {
                    self.isavaildiscount = "0"
                }
                else {
                    self.isavaildiscount = "1"
                }
            }
            if let redeemPonts = object["RedeemPoints"] as? String {
                self.redeemPoints = redeemPonts
                print(redeemPonts)
            }
            
            if let pointid = object["PointId"] as? String {
                self.pointID = pointid
                print(pointid)
            }
        }
    }
    
    @objc func fromItemCartScreenUpdate(notification: NSNotification) {
        
        if let object = notification.object as? [String: Any] {
            
            if let itemCartNotif = object["FromItemCart"] as? String {
                let itemBoll = itemCartNotif
                print("From-ItemCart-VC: \(itemBoll)")
                
                self.redeemDiscount = "0.0"
            }
        }
    }
    
    func addNibView() {
        addViewWithAnimation()
    }
    
    
    
    func populateData() {
        if let object = orderObj {
            
            self.title = "Order # \(object.orderId!)"
            
            redeemDiscountFromService = String (object.discount!)
            print("A--Redeem-Discount: \(redeemDiscountFromService)")
            subTotalFromService = String (object.subtotal!)// + Double (redeemDiscountFromService)!)
            print("A--subTotalFromService: \(subTotalFromService)")
            
            lbl_ResturantName.text = object.businessName
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
            //let convertedDate = dateFormatter.string(from: parseDateToDateFormat(object.orderedOn!))
            //lbl_Date.text = convertedDate
            lbl_Date.text = "\(formatDate(date: object.orderedOn!)) \(formateTime(date: object.orderedOn!))"
            lbl_Status.text = object.orderStatusDetail
            lbl_TotalPrice.text = "$ \(String(describing: object.orderTotalAmount!))"
            print("Total-Price---->  $\(object.orderTotalAmount!)")
            resturantID = "\(object.businessId!)"
            
            if (object.businessLogoUrl != "") {
                imgView.af_setImage(withURL: URL(string: object.businessLogoUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
            }
            
            self.items.removeAll()
            
            
            for item in (object.orderLine?.enumerated())! {
                let product = Product()
                product.menuName = item.element.menuName
                product.itemPrice = item.element.unitPrice
                product.orderStatus = item.element.orderLineStatusDescription
                product.orderQuantity = Int64(item.element.quantity!)
                product.isEditable = false
                self.isTopUpOrder = false
                
                if item.element.instructions != nil {
                    product.topupInstructions = item.element.instructions!
                }
                
                /*
                if item.element.instructions != nil {
                    if !orderInstructions.contains(item.element.instructions!) {
                        orderInstructions.append("\(String(describing: item.element.instructions!))\n")
                    }
                }*/
                
                
                self.items.append(product)
            }
            
            //if new placed order come in
            for item in (object.orderLineCanCancel?.enumerated())! {
                let product = Product()
                product.menuName = item.element.menuName
                product.itemPrice = item.element.unitPrice
                product.orderStatus = item.element.orderLineStatusDescription
                product.orderQuantity = Int64(item.element.quantity!)
                product.isEditable = false
                self.isTopUpOrder = false
                
                if item.element.instructions != nil {
                    product.topupInstructions = item.element.instructions!
                }
                
                /*
                if item.element.instructions != nil {
                    if !orderInstructions.contains(item.element.instructions!) {
                        orderInstructions.append("\(String(describing: item.element.instructions!))\n")
                    }
                }*/
                
                //order initiated not added in list only accepted orders
                if item.element.orderLineStatusDescription == "Initiated" {
                    
                }
                
                if item.element.topUpCode == "" {
                    
                    //empty strings means main order
                    self.items.append(product)
                }
                
            }
            
            //INSTRUCTION TEXT
            if self.items.count > 0 {
                for item in self.items {
                    if item.topupInstructions != nil {
                        if !orderInstructions.contains(item.topupInstructions!) {
                            orderInstructions.append("\(String(describing: item.topupInstructions!))\n")
                        }
                    }
                }
            }
            
            getdbOrders()
            
            txtOrderInstructions.text = orderInstructions
            
            //self.items = object.orderLine!
            
            
            
            tblView.reloadData()
            
        }
        
       
    }
    
    func reloadData() {
        self.items.removeAll()
        
        if let object = orderObj {
            
            //Accepted Orders
            for item in (object.orderLine?.enumerated())! {
                let product = Product()
                product.menuName = item.element.menuName
                product.itemPrice = item.element.unitPrice
                product.orderStatus = item.element.orderLineStatusDescription
                product.orderQuantity = Int64(item.element.quantity!)
                product.isEditable = false
                self.isTopUpOrder = false
                self.items.append(product)
            }
            
            //Initiated Orders
            for item in (object.orderLineCanCancel?.enumerated())! {
                let product = Product()
                product.menuName = item.element.menuName
                product.itemPrice = item.element.unitPrice
                product.orderStatus = item.element.orderLineStatusDescription
                product.orderQuantity = Int64(item.element.quantity!)
                product.isEditable = false
                self.isTopUpOrder = false
                
                self.items.append(product)
            }
            
        }
        
        getdbOrders()
        tblView.reloadData()
    }
    
    func getdbOrders() {
        let items = dbManager.getProdcutWithOrderId(orderId: Int64(orderObj!.orderId!), resturantId: resturantID)
        
        self.total = 0
        
        if items.count == 0 {
            totalItems = self.tblView.numberOfRows(inSection: 0)
            subTotal = subTotalFromService
        }
        
        for item in items {
            let total = Double(item.orderQuantity!) * item.itemPrice!
            self.total = total + self.total
           // lblTotal.text = "$\(self.total.rounded(toPlaces: 2))"
            print("Total-After: $\(self.total.rounded(toPlaces: 2))")
            
            totalItems = self.tblView.numberOfRows(inSection: 0)
            subTotal = "\(String(format: "%.2f", (self.total + Double (subTotalFromService)!)))"
        }
        
        for pdt in items.enumerated() {
            pdt.element.isEditable = true
            self.isTopUpOrder = true
            self.items.append(pdt.element)
            isProductAdded = true
        }
        print(items.count)
    }
    
    func getUpdatedItemsCount() {
        let items = dbManager.getProdcutWithOrderId(orderId: Int64(orderObj!.orderId!), resturantId: resturantID)
        
        self.total = 0
        
        for item in items {
            let total = Double(item.orderQuantity!) * item.itemPrice!
            self.total = total + self.total
           // lblTotal.text = "$\(self.total.rounded(toPlaces: 2))"
            print("Total-After: $\(self.total.rounded(toPlaces: 2))")
            
            totalItems = self.tblView.numberOfRows(inSection: 0)
            //subTotal = "\(self.total.rounded(toPlaces: 2) + Double (subTotalFromService)!)"
            subTotal = "\(String(format: "%.2f", (self.total + Double (subTotalFromService)!)))"
        }
    }
    
    func apiCancelOrder() {
        APIClient.CancelOrderRequest(orderId: "\(String(describing: orderObj!.orderId!))") { (response, error, message) in
            
            if response != nil {
                
                if(response?.status == "True")
                {
                    //showToast(viewControl: self, titleMsg: "", msgTitle: "Order has been Cancelled successfully.")
                    
                    if let user = getUser() {
                        print("RewardPoints: \(response!.rewardPoints)")
                        user.rewardPoints = Int((response!.rewardPoints)!)
                        setUser(userObj: user)
                    }
                
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                }
                else
                {
                    let cancelMenu = UIAlertController(title: "", message: response!.message!, preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    cancelMenu.addAction(cancelAction)
                    
                    self.present(cancelMenu, animated: true, completion: nil)
                }
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)

            }
            
            
        }
    }
    
    func showDialog()
    {
        let optionMenu = UIAlertController(title: "", message: "Order has been Cancelled successfully", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.navigationController?.popViewController(animated: true)
            
        })
        
        optionMenu.addAction(okAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func gotoResturantScreen() {
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kResturantControllerID) as! ResturantVC
        vc.resturantId = resturantID
        vc.isTopUp = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func cancelOrder() {
        let cancelMenu = UIAlertController(title: "Are you sure you want to cancel this order?", message: "", preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.apiCancelOrder()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        cancelMenu.addAction(cancelAction)
        cancelMenu.addAction(YesAction)
        self.present(cancelMenu, animated: true, completion: nil)
    }

    func apiUpdateOrder() {
        
        let arrProducts = dbManager.getProdcutWithOrderId(orderId: Int64(orderObj!.orderId!), resturantId: resturantID)
        
        if arrProducts.count > 0 {
            
            var arrDict = [[String: Any]]()
            var JSONText = ""
            let resturantId = "\(arrProducts[0].restaurantId!)"
            
            
            for product in arrProducts {
                let dicts = ["MenuId" : product.menuId!,
                             "Quantity": product.orderQuantity!,
                             "UnitPrice": product.itemPrice!,
                             "MenuOptionIds": product.menuOptionId!] as [String : Any]
                arrDict.append(dicts)
            }
            
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: arrDict, options: .prettyPrinted)
                // here "jsonData" is the dictionary encoded in JSON data
                
                JSONText = String(data: jsonData, encoding: .utf8)!
                
            } catch {
                print(error.localizedDescription)
            }
            
            let finalreDeemDiscount = abs(Double (self.redeemDiscount)!)//abs(Double (self.redeemDiscountFromService)! - Double (self.redeemDiscount)!)
            print("Final-Redeem-Discount: \(finalreDeemDiscount)")
            
            print(JSONText)
            APIClient.UpdateOrderRequest(jsonData: JSONText, businessId: resturantId, orderId: "\(orderObj!.orderId!)", Instructions: orderInstructions, DiscountAmount: String (finalreDeemDiscount), DiscountPoint: self.redeemPoints, pointId: self.pointID, isAvailDiscount: self.isavaildiscount) { (response, error, message) in
                
                if response != nil {
                    self.gotoOrderCompleteVC(orderNumber: response?.orderId! ?? "0")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        let arrProducts = self.dbManager.getProdcutWithOrderId(orderId: Int64(self.orderObj!.orderId!), resturantId: self.resturantID)
                        for item in arrProducts.enumerated() {
                            self.dbManager.deleteProduct(product: item.element)
                        }
                        //self.navigationController?.popViewController(animated: true)
                    })
                }
                
                if error != nil {
                    print(error.debugDescription)
                }
                
                if message != nil {
                    showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                }
                
                
                
            }
        }
    }
    
    func gotoOrderCompleteVC(orderNumber : String) {
        
        let storyboard = UIStoryboard(name: kCartStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kOrderCompleteControllerID) as! OrderCompleteVC
        vc.orderNo = orderNumber
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- Method of Instructions View
    
    func addViewWithAnimation() {
        self.tblView.isUserInteractionEnabled = false
        
        //Animation
        instructView.alpha = 0
        txtInstructions.alpha = 0
        btnDone.alpha = 0
        lblAlergies.alpha = 0
        
        //self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromTop, animations: {
            //Animation
            self.instructView.isHidden = false
            self.instructView.alpha = 1
            self.txtInstructions.alpha = 1
            self.btnDone.alpha = 1
            self.lblAlergies.alpha = 1
            
            //self.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        }, completion: {(finished) in
            //self.currentTabBar?.setBar(hidden: true, animated: true)
            //self.navigationController?.isNavigationBarHidden = true
        })
    }
    
    func removeViewWithAnimation() {
        self.tblView.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromBottom, animations: {
            self.instructView.alpha = 0
            self.txtInstructions.alpha = 0
            self.btnDone.alpha = 0
            self.lblAlergies.alpha = 0
            
        }, completion: { (finished) in
            
        })
    }
    
    func isValid() -> Bool {
        var returnValue = true
        
        if txtInstructions.text!.isEmpty {
            //message = "Please enter delivery address."
            returnValue = false
        }
        
        if !returnValue {
            showToast(animated: true, viewControl: self, titleMsg: "", msgTitle: "Please enter instructions.")
        }
        
        return returnValue
    }
    
    func doneTapped(Instructions: String) {
        self.orderInstructions = Instructions
        
        showOrderUpdateAlert()
    }
    
    func gotoUpdateItemListScreen(product: Product) {
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kItemCartControllerID) as! ItemCartVC
        vc.product = product
        vc.editingMode = true
        vc.isTopUp = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AcceptedScreenVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.items.count
        
        switch section {
        case 0:
            return self.items.count
        case 1:
            return 1
        default :
            return self.items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable)
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: reuseIdentifierTable)
        }
        
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! NotEditableOrderCell
            
            let model = self.items[indexPath.row]
            
            cell.lbl_itemName.text = model.menuName!
            cell.lbl_itemStatus.text = model.orderStatus
            if model.orderStatus == "Initiated" {
                cell.lbl_itemStatus.backgroundColor = .black
            }else{
                cell.lbl_itemStatus.backgroundColor = kThemeDarkGreen
            }
            
            
            //cell.lbl_totalPriceItem.text = "$\(model.itemPrice!)"
            cell.lbl_quantityItem.text = "\(model.orderQuantity!)"
            cell.lbl_singleItemPrice.text = "$\(model.itemPrice!)"
            
            
            //Total Calculation
            let total = Double(model.orderQuantity!) * model.itemPrice!
            print("Total::: $\(total.rounded(toPlaces: 2))")
            cell.lbl_totalPriceItem.text = "$\(total.rounded(toPlaces: 2))"
            
            
            cell.btn_minusItem.tag = indexPath.row
            cell.btn_plusItem.tag = indexPath.row
            cell.btn_deleteItem.tag = indexPath.row
            cell.delegate = self
            
            if model.isEditable! {
                cell.lbl_itemStatus.isHidden = true
                cell.btn_deleteItem.isHidden = false
                cell.btn_plusItem.isHidden = false
                cell.btn_minusItem.isHidden = false
                
                
            }else{
                cell.lbl_itemStatus.isHidden = false
                cell.btn_deleteItem.isHidden = true
                cell.btn_plusItem.isHidden = true
                cell.btn_minusItem.isHidden = true
                
            }
            
            cell.selectionStyle = .none
            
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTableTwo, for: indexPath as IndexPath) as! NotEditableOrderCellTwo
            
            cell.viewParent.layer.borderWidth = 0.5
            cell.viewParent.layer.cornerRadius = 5
            cell.viewParent.layer.borderColor = UIColorFromRGB(rgbValue: 0xacacac).cgColor
            
            cell.lblItems.text = String (self.tblView.numberOfRows(inSection: 0))
            //let sTotal = Double (subTotal)
            cell.lblSubtotal.text = "$ \(subTotal)"//("$ \(String(format: "%.2f", sTotal!))")
            
            let redeemDic = Double(redeemDiscountFromService)! + Double (redeemDiscount)!//String(format: "%.2f", currentRatio)
            print("Redeem-Discount: \(redeemDic)")
            if redeemDic > 0 {
                cell.lblDiscount.text = String ("($ \(String(format: "%.2f", redeemDic)))")
            }
            else {
                cell.lblDiscount.text = String ("$ \(String(format: "%.2f", redeemDic))")
            }
            //cell.lblDiscount.text = String ("$ \(redeemDiscount)")
            
            print("IsTopUp: \(self.isTopUpOrder)")
            if self.isTopUpOrder == true {
                cell.btnRedeemPoints.isEnabled = true
            }
            else {
                cell.btnRedeemPoints.isEnabled = false
            }
            
            let totalPrice = Double (subTotal)! - Double (redeemDic)
            self.lbl_TotalPrice.text = "$ \(String(format: "%.2f", totalPrice))"//String ((Double(Double (subTotal)! - Double (redeemDic))).rounded(toPlaces: 2))
            
            cell.btnRedeemPoints.addTarget(self, action: #selector(gotoRedeemPointScreen), for: .touchUpInside)
            return cell
            
        default:
            break
            
        }
        
        return cell1!
        
    }
    
    @objc func gotoRedeemPointScreen() {
        
        let vc = UIStoryboard.init(name: kCartStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRedeemPointsControllerID) as? RedeemPointsVC
        print("Total to Redeem: \(String (Double (subTotal)! - Double (subTotalFromService)!))")
        vc!.totalCount = String (Double (subTotal)! - Double (subTotalFromService)!)
        vc!.fromScreen = "UpdateOrder"
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 8
        case 1:
            return 0
            
        default:
            print("Default")
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        
        switch section {
        case 0:
            let headerView = UIView()
            headerView.backgroundColor = UIColorFromRGB(rgbValue: 0xf3f3f3)
            return headerView
            
        case 1:
            return headerView
            
        default:
            print("Default")
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if UIDevice.current.userInterfaceIdiom == .pad { return 98 } else { return 78 }
        } else {
            return 140
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            let selectedProduct = self.items[indexPath.row]
            if selectedProduct.isEditable! {
                gotoUpdateItemListScreen(product: selectedProduct)
            }
        case 1:
            print("Section-2")
            
        default:
            print("Default")
        }
        
    }
    
}

extension AcceptedScreenVC: EditableCellDelegate {
    func deleteTapped(index: Int) {
        let model = self.items[index]
        isProductAdded = false
        //deleteOrder(product: model)
        self.dbManager.deleteProduct(product: model)
        self.redeemDiscount = "0.0"
        self.reloadData()
        
    }
    
    func increamentTapped(index: Int) {
        let model = self.items[index]
        model.orderQuantity = model.orderQuantity! + 1
        dbManager.updateProduct(product: model)
        self.redeemDiscount = "0.0"
        tblView.reloadData()
        getUpdatedItemsCount()
        
    }
    
    func decrementTapped(index: Int) {
        let model = self.items[index]
        
        if model.orderQuantity! <= 1 {
            showAlert(title: "Quantity", message: "Item quantity cannot go below 1", viewController: self)
            return
        }
        model.orderQuantity = model.orderQuantity! - 1
        dbManager.updateProduct(product: model)
        self.redeemDiscount = "0.0"
        tblView.reloadData()
        getUpdatedItemsCount()
    }
    
    func deleteOrder(product: Product) {
        let cancelMenu = UIAlertController(title: "Are you sure you want to delete this item?", message: "", preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.dbManager.deleteProduct(product: product)
            self.reloadData()
        })
        
        let cancelAction = UIAlertAction(title: "No", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        cancelMenu.addAction(cancelAction)
        cancelMenu.addAction(YesAction)
        self.present(cancelMenu, animated: true, completion: nil)
    }
    
}

extension AcceptedScreenVC: OrderViewDelegate {
    func fieldBecomeDeactive() {
        
    }
    
    func fieldBecomeActive() {
        
    }
    
    func showValidation() {
        
    }
    
    func dismissView() {
        orderTypeView?.removeViewWithAnimation()
    }
    
    func doneTapped(deliveryAddress: String, Instructions: String, orderType: String) {
        print("\(Instructions)")
        orderInstructions = Instructions
        showOrderUpdateAlert()
    }
    
    func showOrderUpdateAlert() {
        let placeMenu = UIAlertController(title: "Confirm your order now?", message: "", preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.apiUpdateOrder()
            
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        placeMenu.addAction(cancel)
        placeMenu.addAction(YesAction)
        
        self.present(placeMenu, animated: true, completion: nil)
    }
    
}

extension AcceptedScreenVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        animateUp()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        animateDown()
    }
    
    func animateUp() {
        UIView.animate(withDuration: 0.3, animations: {
            self.bottomConstraint.constant = 245
            self.view.layoutIfNeeded()
            
        }) { (completed) in
            
        }
    }
    
    func animateDown() {
        UIView.animate(withDuration: 0.3, animations: {
            self.bottomConstraint.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (completed) in
            
        }
    }
}
