//
//  CompletedCellOne.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/25/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class CompletedCellOne: UITableViewCell {
    @IBOutlet weak var lblResturantName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgResturant: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
