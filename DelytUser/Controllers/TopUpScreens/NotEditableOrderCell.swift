//
//  NotEditableOrderCell.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 11/22/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol EditableCellDelegate {
    func deleteTapped(index:Int)
    func increamentTapped(index:Int)
    func decrementTapped(index:Int)
}
class NotEditableOrderCell: UITableViewCell {
    var delegate: EditableCellDelegate?

    
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var lbl_itemStatus: UILabel!
    @IBOutlet weak var lbl_quantityItem: UILabel!
    @IBOutlet weak var lbl_singleItemPrice: UILabel!
    @IBOutlet weak var lbl_totalPriceItem: UILabel!
    @IBOutlet weak var lbl_restaurantName: UILabel!
    @IBOutlet weak var btn_deleteItem: UIButton!
    @IBOutlet weak var btn_plusItem: UIButton!
    @IBOutlet weak var btn_minusItem: UIButton!
    
    @IBAction func deleteTapped(_ sender: Any) {
        
        let button = sender as! UIButton
        self.delegate?.deleteTapped(index: button.tag)
    }
    
    @IBAction func plusTapped(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.increamentTapped(index: button.tag)
    }
    
    @IBAction func minusTapped(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.decrementTapped(index: button.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
       super.layoutSubviews()

       //Your separatorLineHeight with scalefactor
       let separatorLineHeight: CGFloat = 15/UIScreen.main.scale

       let separator = UIView()

       separator.frame = CGRect(x: self.frame.origin.x,
                                y: self.frame.size.height - separatorLineHeight,
                            width: self.frame.size.width,
                           height: separatorLineHeight)

       separator.backgroundColor = UIColorFromRGB(rgbValue: 0xf3f3f3)

       self.addSubview(separator)
    }

}
