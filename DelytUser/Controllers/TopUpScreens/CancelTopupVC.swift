//
//  CancelTopupVC.swift
//  DelytUser
//
//  Created by Muhammad Umar on 14/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class CancelTopupVC: UIViewController {

    @IBOutlet weak var lblResturantName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtInstruction: UITextView!
    
    var items = [Product]()
    let reuseIdentifierTable = "NotEditableOrderCell"
    
    var restName = ""
    var status = ""
    var date = ""
    var imgUrl = ""
    var txtInst = ""
    var orderNo = ""
    var topUpCode = ""
    
    @IBAction func cancelTapped(_ sender: Any) {
        
        
        let cancelMenu = UIAlertController(title: "Are you sure you want to cancel?", message: "", preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "Yes", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.cancelTopupOrderApi()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        cancelMenu.addAction(cancel)
        cancelMenu.addAction(YesAction)
        
        self.present(cancelMenu, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        populateData()
    }
    
    func populateData() {
        self.title = "Order # \(orderNo) \(topUpCode)"
        lblResturantName.text = restName
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
        print("CurrentOrderDate: \(date)")
        let convertedDate = dateFormatter.string(from: parseDateToDateFormat(date))
        lblDate.text = getFormattedDateForTipScreen(strDate: date, currentFomat: "yyyy-MM-dd'T'HH:mm:ss.SSS", expectedFromat: "MMM dd yyyy   hh:mm aaa")//date//convertedDate
        lblStatus.text = status
        
        imgView.af_setImage(withURL: URL(string: imgUrl)!, placeholderImage: UIImage(named: "icon_noimage")!)
        
        txtInstruction.text = txtInst
    }
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    func cancelTopupOrderApi() {
        
        APIClient.CancelTopupOrderRequest(orderId: "\(String(describing: orderNo))", topupCode: topUpCode) { (response, error, message) in
            
            if response != nil {
                
                if(response?.status == "True") {
                    
                    if let user = getUser() {
                        print("RewardPoints: \(response!.rewardPoints)")
                        user.rewardPoints = Int((response!.rewardPoints)!)
                        setUser(userObj: user)
                    }
                    
                    showToast(viewControl: self, titleMsg: "", msgTitle: (response?.message!)!)
                    DispatchQueue.main.asyncAfter(deadline: .now() +  2.0, execute: {
                        self.popBack(3)
                    })
                    
                }
                else
                {
                    let cancelMenu = UIAlertController(title: "", message: response!.message!, preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "OK", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    cancelMenu.addAction(cancelAction)
                    
                    self.present(cancelMenu, animated: true, completion: nil)
                }
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)

            }
            
            
        }

    }
    
    

}

extension CancelTopupVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! NotEditableOrderCell
        
        let model = self.items[indexPath.row]
        
        cell.lbl_itemName.text = model.menuName!
        //cell.lbl_itemStatus.text = model.orderLineStatusDescription
        //cell.lbl_totalPriceItem.text = "$\(model.itemPrice!)"
        cell.lbl_quantityItem.text = "\(model.orderQuantity!)"
        cell.lbl_singleItemPrice.text = "$\(model.itemPrice!)"
        
        //Total Calculation
        let total = Double(model.orderQuantity!) * model.itemPrice!
        print("$\(total.rounded(toPlaces: 2))")
        cell.lbl_totalPriceItem.text = "$\(total.rounded(toPlaces: 2))"
       
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}
