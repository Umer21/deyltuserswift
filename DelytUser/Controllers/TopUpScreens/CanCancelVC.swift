//
//  CanCancelVC.swift
//  DelytUser
//
//  Created by Muhammad Umar on 12/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class CanCancelVC: UIViewController {
    var orderObj: HistoryObj?

    var selectedOrderObj: HistoryObj?
    var selectedProduct = Product()
    
    @IBOutlet weak var lblResturant: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgView: UIImageView!
    
    var items = [Product]()
    var editableItems = [Product]()
    var uniqueProducts = [Product]()
    
    var selectedUrl = ""
    var selectedOrderId = ""
    var selectedInst = ""
    var selectedTopupCode = ""
    var currentOrderDate = ""
    
    let reuseIdentifierTable = "canCancelCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        populateData()
    }
    
    func populateData() {
        if let object = orderObj {
            
            self.title = "Order # \(object.orderId!)"
            
            lblResturant.text = object.businessName!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
            //let convertedDate = dateFormatter.string(from: parseDateToDateFormat(object.orderedOn!))
            //lblDate.text = convertedDate
            lblDate.text = "\(formatDate(date: object.orderedOn!)) \(formateTime(date: object.orderedOn!))"
            
            lblStatus.text = object.orderStatusDetail!
            
            imgView.af_setImage(withURL: URL(string: object.businessLogoUrl!)!, placeholderImage: UIImage(named: "icon_noimage")!)
            selectedUrl = object.businessLogoUrl!
            selectedOrderId = "\(object.orderId!)"
            
            
            self.items.removeAll()
            
            self.selectedOrderObj = self.orderObj
            var confirmOrders = [OrderLine]()
            
            for (index, item) in (object.orderLine?.enumerated())! {
                
                let product = Product()
                product.menuName = item.menuName
                product.itemPrice = item.unitPrice
                product.orderStatus = item.orderLineStatusDescription
                product.orderQuantity = Int64(item.quantity!)
                product.orderDate = item.createdOn!
                product.orderId = Int64(item.orderId!)
                product.topupCode = item.topUpCode!
                product.isEditable = false
                
                if index == 0 {
                    self.editableItems.append(product)
                }
               
                confirmOrders.append(item)
                self.items.append(product)
            }
            
            self.selectedOrderObj?.orderLine = confirmOrders
            
            
            //get all cancelable distinct items
            
            let distictItems = object.orderLineCanCancel?.distinct { $0.topUpCode }
            
            
            //if new placed order come in
            for item in (distictItems?.enumerated())! {//object.orderLineCanCancel?
                
                let product = Product()
                product.menuName = item.element.menuName
                product.itemPrice = item.element.unitPrice
                product.orderStatus = item.element.orderLineStatusDescription
                product.orderQuantity = Int64(item.element.quantity!)
                product.orderDate = item.element.createdOn!
                product.orderId = Int64(item.element.orderId!)
                product.topupCode = item.element.topUpCode!
                product.isEditable = true
                
                if item.element.instructions != nil {
                    selectedInst = item.element.instructions!
                }
                
                //order initiated not added in list only accepted orders
                /*
                if item.element.orderLineStatusDescription == "Initiated" {
                    return
                }*/
                
                self.editableItems.append(product)
                
            }
            
            //self.items = object.orderLineCancelled!
            tblView.reloadData()
        }
       
    }
    
    func gotoAcceptedScreen() {
        let storyboard = UIStoryboard(name: kTopUpStoryboard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kAcceptedControllerID) as! AcceptedScreenVC
        vc.orderObj = selectedOrderObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoCancelTopupScreen() {
        let storyboard = UIStoryboard(name: kHomeStoryborad, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: kTopupCancelScreenControllerID) as! CancelTopupVC
        vc.restName = lblResturant.text!
        vc.status = lblStatus.text!
        vc.date = self.currentOrderDate//lblDate.text!
        vc.imgUrl = selectedUrl
        vc.txtInst = selectedInst
        vc.orderNo = selectedOrderId
        vc.items = self.uniqueProducts
        vc.topUpCode = selectedTopupCode
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension CanCancelVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.editableItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifierTable, for: indexPath as IndexPath) as! CanCancelCell
        
        let model = self.editableItems[indexPath.row]
        
        cell.lbl_orderTopUpName.text = "Order # \(String(describing: model.orderId!)) \(String(describing: model.topupCode!))"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd yyyy   hh:mm aaa"
        //let convertedDate = dateFormatter.string(from: parseDateToDateFormat(model.orderDate!))
        cell.lbl_orderDate.text = "\(formatDate(date: model.orderDate!)) \(formateTime(date: model.orderDate!))"//convertedDate
        self.currentOrderDate = model.orderDate!
        print("self.currentOrderDate: \(self.currentOrderDate)")
        
        cell.lbl_orderStatus.text = model.orderStatus!
        
        if model.orderStatus == "Initiated" {
            cell.lbl_orderStatus.backgroundColor = .black
        }else{
            cell.lbl_orderStatus.backgroundColor = kThemeDarkGreen
        }
       
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
            gotoAcceptedScreen()
        }else{
            let model = self.editableItems[indexPath.row]
            self.selectedProduct = model
            self.selectedTopupCode = model.topupCode!
            self.uniqueProducts.removeAll()
            
            var orderInstructions = ""
            
            for item in (orderObj?.orderLineCanCancel?.enumerated())! {//all topup items
                
                //get all same topupcode items
                if item.element.topUpCode == model.topupCode {
                    
                    let product = Product()
                    product.menuName = item.element.menuName
                    product.itemPrice = item.element.unitPrice
                    product.orderStatus = item.element.orderLineStatusDescription
                    product.orderQuantity = Int64(item.element.quantity!)
                    product.orderDate = item.element.createdOn!
                    product.orderId = Int64(item.element.orderId!)
                    product.topupCode = item.element.topUpCode!
                    product.isEditable = true
                    
                    //str.appending("\n\(String(describing: item.element.instructions!))")
                    orderInstructions.append("\(String(describing: item.element.instructions ?? ""))\n")
                    self.uniqueProducts.append(product)
                }
                
                selectedInst = orderInstructions
                print("\(selectedInst)")
            }
            
            gotoCancelTopupScreen()
        }
    }
    
}
