//
//  NotEditableOrderCellTwo.swift
//  DelytUser
//
//  Created by Inabia1 on 03/09/2021.
//  Copyright © 2021 Umar Farooq. All rights reserved.
//

import UIKit

class NotEditableOrderCellTwo: UITableViewCell {

    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var btnRedeemPoints: UIButton!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
