//
//  ContactUsVC.swift
//  DelytUser
//
//  Created by Muhammad Umar Farooq on 12/17/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC: UIViewController, MFMailComposeViewControllerDelegate {
    
    var items: UserAppSetting?
    var email: String?
    var phone: String?

    
    @IBAction func messageTapped(_ sender: Any) {
        
        UINavigationBar.appearance().barTintColor = .white
        
        //Its email----
        let em = email
        
        let mailComposeViewController = configureMailController(email: em!)
        if MFMailComposeViewController.canSendMail() {
            //mailComposeViewController.navigationController?.navigationBar.tintColor = UIColor.white
            //mailComposeViewController.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.gray]
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showMailError()
        }
        
    }
    
    @IBAction func phoneTapped(_ sender: Any) {
        
        //print(phone)
        let filteredNumber = removeSpecialCharsFromString(phone!)
        //print(filteredNumber)
        
        
        callNumber(phoneNumber: filteredNumber)
        
    }
    
    func removeSpecialCharsFromString(_ str: String) -> String {
        struct Constants {
            static let validChars = Set("1234567890-")
        }
        return String(str.filter { Constants.validChars.contains($0) })
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        apiGetBusinessReviews()
    }
    
    @objc func crossButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    

    func setNavBar() {
        
        let crossButton = UIBarButtonItem(image: UIImage(named: "icon_rating_cross"), style: .plain, target: self, action: #selector(crossButtonTapped))
        self.navigationItem.leftBarButtonItems = [crossButton]
        
    }
    
    func apiGetBusinessReviews() {
        APIClient.GetUserSettingsInformation()
        { (response, error, message) in
            if response != nil {
                
                self.items = (response?.userAppSetting)!
                
                if let em = self.items?.contactUsEmail
                {
                    self.email = em
                }
                
                if let ph = self.items?.contactUsPhone
                {
                    self.phone = ph
                }
                
                
            }
            
            if error != nil {
                print(error.debugDescription)
            }
            
            if message != nil {
                //showToast(viewControl: self, titleMsg: "Error", msgTitle: message!)
                
                let optionMenu = UIAlertController(title: "", message: "No Review(s) available.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                optionMenu.addAction(okAction)
                
                self.present(optionMenu, animated: true, completion: nil)
            }
        }
    }
    

   func callNumber(phoneNumber:String) {

       if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

           let application:UIApplication = UIApplication.shared
           if (application.canOpenURL(phoneCallURL)) {
               if #available(iOS 10.0, *) {
                   application.open(phoneCallURL, options: [:], completionHandler: nil)
               } else {
                   // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)

               }
           }
       }
   }
    
    func configureMailController(email: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([email])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showMailError() {
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device could not send email", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendMailErrorAlert.addAction(dismiss)
        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        UINavigationBar.appearance().barTintColor = kMerunColor
        UINavigationBar.appearance().tintColor = .white
        controller.dismiss(animated: true, completion: nil)
    }

}
